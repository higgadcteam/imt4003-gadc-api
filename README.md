# README #

This Java application allows users to gather data for multiple Google Analytics accounts and merge them. 

### Building ###

The project can be built by using the gradle wrapper, for example

```
#!bash

gradlew build
```

Import the project in your favorite Java IDE to work on the source files (or a text editor for that matter).

### Configuration ###

In order to run the project, make sure that the following environment variables are set for the application:

* GA_API_SECRET=<Google Analytics API secret value>
* GA_API_CLIENT_ID=<Google Analytics API client ID>
* GA_API_REDIRECTION_URL=<Redirection value for the URL in the browser when a Google Analytics user is added to the system>
* GA_API_LOGIN_REDIRECTION_URL=<Redirection value for the URL in the browser when singing in>

The Google Analytics API keys has to be obtained from Google.

The application can be deployed on a Glassfish 4 server, but in order to enable Swagger, the latest versions of Jackson should be deployed on the application server. Swagger can then be accessed from http://localhost:8080/<context path>/v1/swagger.json