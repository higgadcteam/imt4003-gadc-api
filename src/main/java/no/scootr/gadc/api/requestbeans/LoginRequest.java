package no.scootr.gadc.api.requestbeans;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean for parameters sent when users are requesting a login.
 */
@XmlRootElement
@ApiModel(value = "Login Request", description = "Request for signing into the system.")
public class LoginRequest {
    @XmlElement
    @ApiModelProperty("Authorization token from Google. See https://developers.google.com/identity/protocols/OpenIDConnect for more info.")
    private String token;

    public String getToken() {
        return token;
    }
}
