package no.scootr.gadc.api.requestbeans;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Request class used to register or update a Metrics Entity.
 */
@XmlRootElement
@ApiModel(value = "Metrics request", description = "Used for registering or updating a metrics entity.")
public class MetricsRequest {

    @XmlElement
    @ApiModelProperty("Name of the metric")
    private String metricsName;

    @XmlElement
    @ApiModelProperty("Value of the metric")
    private String metricsValue;

    @XmlElement
    @ApiModelProperty("Status of the metric indicating whether it is active or not in the system")
    private boolean status;

    public MetricsRequest() {
    }

    public String getMetricsName() {
        return metricsName;
    }

    public void setMetricsName(String metricsName) {
        this.metricsName = metricsName;
    }

    public String getMetricsValue() {
        return metricsValue;
    }

    public void setMetricsValue(String metricsValue) {
        this.metricsValue = metricsValue;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
