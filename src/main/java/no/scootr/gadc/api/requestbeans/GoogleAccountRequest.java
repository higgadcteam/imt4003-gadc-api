package no.scootr.gadc.api.requestbeans;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This request class is used to create or update a Google Account object.
 */
@XmlRootElement
@ApiModel(value = "Google Account request", description = "Used to create or update a Google account.")
public class GoogleAccountRequest {

    @XmlElement
    @ApiModelProperty("Email of the Google account")
    private String email;

    @XmlElement
    @ApiModelProperty("Authorization token used to get access to the Google account. See https://developers.google.com/identity/protocols/OpenIDConnect for more info.")
    private String token;

    @XmlElement
    @ApiModelProperty("Authorization refresh token towards Google. See https://developers.google.com/identity/protocols/OpenIDConnect for more info.")
    private String refreshToken;

    @XmlElement
    @ApiModelProperty("How long the authorization token is valid. See https://developers.google.com/identity/protocols/OpenIDConnect for more info.")
    private int tokenTimeToLive;

    @XmlElement
    @ApiModelProperty("Timestamp from when the token became valid. See https://developers.google.com/identity/protocols/OpenIDConnect for more info.")
    private long tokenStartTime;

    @XmlElement
    @ApiModelProperty("Status for indicating if the Google account should be active or not in the system.")
    private boolean active;

    @XmlElement
    @ApiModelProperty("ID of the user who owns the Google account in the system. (ID of the user that makes the request)")
    private int userId;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public int getTokenTimeToLive() {
        return tokenTimeToLive;
    }

    public void setTokenTimeToLive(int tokenTimeToLive) {
        this.tokenTimeToLive = tokenTimeToLive;
    }

    public long getTokenStartTime() {
        return tokenStartTime;
    }

    public void setTokenStartTime(long tokenStartTime) {
        this.tokenStartTime = tokenStartTime;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
