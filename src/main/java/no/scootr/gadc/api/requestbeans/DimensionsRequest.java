package no.scootr.gadc.api.requestbeans;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Request Class that is used to register or update a Dimensions entity.
 */
@XmlRootElement
@ApiModel(value = "Dimensions Request", description = "Request for adding a new web property")
public class DimensionsRequest {

    @XmlElement
    @ApiModelProperty("Name of the dimension")
    private String dimensionName;

    @XmlElement
    @ApiModelProperty("Value of the dimension")
    private String dimensionValue;

    @XmlElement
    @ApiModelProperty("Status of the web property indicating if it is disabled or not in the system")
    private boolean status;

    public DimensionsRequest() {
    }

    public String getDimensionName() {
        return dimensionName;
    }

    public void setDimensionName(String dimensionName) {
        this.dimensionName = dimensionName;
    }

    public String getDimensionValue() {
        return dimensionValue;
    }

    public void setDimensionValue(String dimensionValue) {
        this.dimensionValue = dimensionValue;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
