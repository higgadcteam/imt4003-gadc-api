package no.scootr.gadc.api.requestbeans;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean parameters that are sent when registering or updating a web property object.
 */
@XmlRootElement
@ApiModel(value = "Web Property request", description = "Request object for updating or registering a web property.")
public class WebPropertyRequest {

    @XmlElement
    @ApiModelProperty("Unique ID of the web property from Google.")
    private String id;
    @XmlElement
    @ApiModelProperty("Name of the web property.")
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
