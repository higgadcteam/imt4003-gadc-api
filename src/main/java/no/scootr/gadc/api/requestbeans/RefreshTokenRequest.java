package no.scootr.gadc.api.requestbeans;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bean for parameters sent when users are requesting regeneration of authorization token by providing the refresh token.
 */
@XmlRootElement
@ApiModel(value = "Refresh Token Request", description = "Request for regenerating the authorization token of the user.")
public class RefreshTokenRequest {
    @XmlElement
    @ApiModelProperty("Refresh token generated from the API.")
    private String refreshToken;

    public String getRefreshToken() {
        return refreshToken;
    }
}
