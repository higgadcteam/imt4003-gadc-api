package no.scootr.gadc.api.requestbeans;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Request Beans that is used to categorize a web property. This request will contain the ID of the category that is later
 * used to be assigned to the web property.
 */
@XmlRootElement
@ApiModel(value = "WebPropertyCategory Request", description = "The ID of the category that the web property will be assigned to.")
public class CategorizeWebPropertyRequest {

    @XmlElement
    @ApiModelProperty("ID of a category")
    private int categoryID;

    public CategorizeWebPropertyRequest() {
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }
}
