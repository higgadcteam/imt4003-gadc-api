package no.scootr.gadc.api.requestbeans;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *  Request Beans that is used to tag a web property. This request will contain the ID of the tag that is later
 * used to be assigned to the web property.
 */
@XmlRootElement
@ApiModel(value = "Tag request", description = "Used for tagging a web property.")
public class TagRequest {
    @XmlElement
    @ApiModelProperty(value = "Name of the tag", example = "fashion")
    private String tagName;

    public TagRequest() {
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
