package no.scootr.gadc.api.requestbeans;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This request class is used to create or update a google analytics account by only providing the id, name and owner of the entity
 * which is the Google Account email.
 */
@XmlRootElement
@ApiModel(value = "Google Analytics account request",
        description = "used to create or update a google analytics account by only providing the id, name and owner of " +
                "the entity which is the Google Account email.")
public class GAAccountRequest {

    @XmlElement
    @ApiModelProperty("Name of the Google Analytics account")
    private String name;

    @XmlElement
    @ApiModelProperty("ID of the Google Account that owns the Google Analytics account")
    private Integer owner;

    @XmlElement
    @ApiModelProperty("ID of the Google Analytics account")
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOwner() {
        return owner;
    }

    public void setOwner(Integer owner) {
        this.owner = owner;
    }
}
