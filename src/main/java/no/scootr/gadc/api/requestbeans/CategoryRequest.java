package no.scootr.gadc.api.requestbeans;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * CategoryRequest is used when creating a category in the API. It only has an attribute which is the Category Name, because the
 * id of the category is a auto increment generated value.
 */
@XmlRootElement
@ApiModel(value = "Category Request", description = "Describes information about a category without its ID.")
public class CategoryRequest {

    @XmlElement
    @ApiModelProperty("Name of the category")
    private String name;

    public CategoryRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
