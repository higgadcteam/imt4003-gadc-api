package no.scootr.gadc.api.exceptions.registrationexceptions;

/**
 * This is a custom Exception that will be thrown each time something goes wrong in the api. It will take a message which
 * then will be displayed to the user.
 */
public class CustomizedMessageException extends Exception {

    public CustomizedMessageException(String message) {
        super(message);
    }

    public CustomizedMessageException (final Throwable cause, String message) {
        super(message, cause);
    }
}
