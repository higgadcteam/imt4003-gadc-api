package no.scootr.gadc.api.services;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.Profile;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * This service is used to query data for a web-property profile entity and mainly for CRUD operations on that entity.
 * Created by Brikendi on 11/24/2015.
 */
@Stateless
public class ProfileService {
    @PersistenceContext
    private EntityManager em;

    /**
     * This method will register a Profile into the DB.
     * @param profile the Profile entity that has to be persisted.
     * @throws CustomizedMessageException
     */
    public void addProfile(final Profile profile) throws  CustomizedMessageException {
        try {
            em.persist(profile);
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * This method modifies an existing profile in the DB.
     * @param profile the Profile entity that has to be edited.
     * @return the edited/merged profile entity.
     * @throws CustomizedMessageException
     */
    public Profile editProfile(final Profile profile) throws  CustomizedMessageException {
        try {
            return em.merge(profile);
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * This method deletes an exiting Profile associated with a web property.
     * @param profile the profile entity that has to be deleted.
     * @return the deleted profile entity.
     * @throws CustomizedMessageException
     */
    public Profile deleteProfile(final Profile profile) throws CustomizedMessageException {
        try {
            em.remove(em.contains(profile) ? profile : em.merge(profile));
            return profile;
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * This method will query a profile by its unique ID attribute
     * @param profileID the ID of the profile to be queried.
     * @return the found Profile entity.
     */
    public Profile getProfileByID(final long profileID) {
        return em.find(Profile.class, profileID);
    }
}
