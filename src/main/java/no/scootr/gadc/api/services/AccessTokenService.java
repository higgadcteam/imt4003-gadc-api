package no.scootr.gadc.api.services;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.GoogleAccount;
import no.scootr.gadc.api.model.GoogleAccountAuthToken;
import no.scootr.gadc.api.utillity.ApiCall;
import no.scootr.gadc.api.utillity.Util;

import javax.ejb.Stateless;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
/**
 * This service has been generally created to access the refreshAccessToken method from multiple classes, as it is a
 * method that is used by many resources.
 */
@Stateless
public class AccessTokenService {
    @PersistenceContext
    private EntityManager em;

    /**
     * This method will updated the access token of the Google Account object, by making an API call to google providing
     * the refresh token which is then used to get e new valid access token from Google API.
     * @param googleAccount To be updated with the new access token
     * @param em The entity manager created manually.
     * @throws CustomizedMessageException
     */
    public void refreshAccessToken(final GoogleAccount googleAccount, final EntityManager em) throws CustomizedMessageException {
        try {
            /**
             * Specify the grant type to be retrieved from the API.
             */
            final String grantType = "refresh_token";

            /**
             * Format the query parameters and their corresponding values.
             */
            final String body = String.format (
                    "refresh_token=%s&client_id=%s&client_secret=%s&grant_type=%s",
                    googleAccount.getOAuthToken().getRefreshToken(), Util.GA_API_CLIENT_ID, Util.GA_API_SECRET, grantType);

            /**
             * Create the ApiCall and retrieve the access token from OAUTH2 and store it in a JsonObject.
             */
            final ApiCall accessTokeCall = new ApiCall("https://www.googleapis.com/oauth2/v3/token", "POST");
            accessTokeCall.setContentType("application/x-www-form-urlencoded");
            accessTokeCall.writeString(body);
            final JsonObject accessToken = accessTokeCall.readJsonResponse();

            final GoogleAccountAuthToken token = googleAccount.getOAuthToken();
            token.setTokenValue(accessToken.getString("access_token"));
            token.setTimeToLive(accessToken.getInt("expires_in"));
            token.setCreated(System.currentTimeMillis() / 1000L);

            em.merge(token);
        } catch (Exception e) {
            throw new CustomizedMessageException(e, "Could't update the new access token!");
        }
    }

    /**
     * This method will updated the access token of the Google Account object, by making an API call to google providing
     * the refresh token which is then used to get e new valid access token from Google API.
     * @param googleAccount To be updated with the new access token.
     * @throws CustomizedMessageException
     */
    public void refreshAccessToken(final GoogleAccount googleAccount) throws CustomizedMessageException {
        refreshAccessToken(googleAccount, em);
    }
}
