package no.scootr.gadc.api.services;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.Tag;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.utillity.JPAHelper;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * This service is used to query data for a tag entity and mainly for CRUD operations on that entity.
 */
@Stateless
public class TagService {
    @PersistenceContext
    private EntityManager em;


    /**
     * Method to find all the registered tags in the DB.
     * @return The list of all tags registered in the DB.
     */
    public List<Tag> findAll() {
        return em.createQuery("SELECT t FROM Tag t", Tag.class).getResultList();
    }

    /**
     * Method to find Tag object by ID.
     * @param tagID ID of the tag that is used to find the entity in the DB.
     * @return The found tag entity.
     */
    public Tag getTagByID(final int tagID) {
        return em.find(Tag.class, tagID);
    }

    /**
     * This method adds a tag entity into the DB.
     * @param tag Tag object that has to be registered in the DB.
     * @throws CustomizedMessageException
     */
    public void addTag(final Tag tag) throws CustomizedMessageException {
        try {
            em.persist(tag);
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * This method tries to find a tag by its tag name specified in the parameter attribute.
     * @param tagName Is the name of the tag that is used to find the entity in the DB.
     * @return The found tag entity.
     */
    public Tag getTagByName (final String tagName) {
        return JPAHelper.getFirsOrNull(em.createQuery("SELECT t FROM Tag t WHERE t.name = :tagName", Tag.class)
                .setParameter("tagName", tagName.toLowerCase())
                .setMaxResults(1));
    }

    /**
     * This method checks if the web property provided in the parameter exists in the list of web properties
     * of the specified tag. I.E it checks the relationship between tag and web properties (on the tag Part).
     * @param tag Tag object.
     * @param webProperty The web property object.
     * @return A boolean value which is true if the tag has a relation with the web property or false otherwise.
     */
    public boolean containsWebProperty(final Tag tag, final WebProperty webProperty) {
        final List<WebProperty> webProperties = tag.getWebProperties();

        if (webProperties.isEmpty()) {
            return false;
        }

        for (WebProperty webProperty1: webProperties) {
            if (webProperty1.getId().trim().equals(webProperty.getId().trim())) {
                return true;
            }
        }

        return false;
    }


    /**
     * This method will edit the tag entity and its attributes in the DB.
     * @param tag Is the tag object that is used to be modified.
     * @throws CustomizedMessageException
     */
    public void edit(final Tag tag) throws CustomizedMessageException {
        try {
            em.merge(tag);
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * This method will delete the given webProperty from the list of web properties of the specified tag.
     * @param tag Is the Tag object that the web property object will be deleted from.
     * @param webProperty Is the web property object that will be deleted from the web property list of the tag.
     */
    public void deleteWebPropertyFromTag(final Tag tag, final WebProperty webProperty) {
        final List<WebProperty> webProperties = tag.getWebProperties();
        for (int i = 0; i < webProperties.size(); i++) {
            if (webProperties.get(i).getId().trim().equals(webProperty.getId().trim())) {
                webProperties.remove(i);
                return;
            }
        }
    }
}
