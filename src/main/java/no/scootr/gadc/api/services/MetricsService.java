package no.scootr.gadc.api.services;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.Metrics;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.utillity.JPAHelper;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * This service class is used to query Metrics data and also mainly for CRUD operation on the metrics entity.
 */
@Stateless
public class MetricsService {
    @PersistenceContext
    private EntityManager em;


    /**
     * This method finds all the metrics that a specific user has registered.
     * @param user the user entity that the dimensions have to be queried from
     * @return a list of dimensions that are associated with the user.
     */
    public List<Metrics> findAllUserMetrics(final User user) {
        return em.createQuery("SELECT m FROM Metrics m WHERE m.status = true AND m.owner = :owner", Metrics.class)
                .setParameter("owner", user)
                .getResultList();
    }

    /**
     * This method adds a metrics entity into the DB.
     * @param metrics Metrics entity that will be added into the DB.
     * @throws CustomizedMessageException
     */
    public void add(final Metrics metrics) throws CustomizedMessageException {
        try {
            em.persist(metrics);
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * This method updates the attributes of a metrics entity.
     * @param metrics Metrics entity that has to be modified.
     * @throws CustomizedMessageException
     */
    public void edit(final Metrics metrics) throws CustomizedMessageException {
        try {
            em.merge(metrics);
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }


    /**
     * This method queries all the active metrics in the DB, and returns a list of found entries.
     * @return The list of all active metrics found in the database.
     */
    public List<Metrics> getAllActiveMetrics() {
        return em.createQuery("SELECT m FROM Metrics m WHERE m.status = true", Metrics.class)
                .getResultList();
    }

    /**
     * This method finds a Metrics object by its MetricsValue attribute.
     * @param metricsValue Metrics value as a string that is used to find the metrics entity.
     * @return The found metrics entity.
     */
    public Metrics findByMetricsValue(final String metricsValue) {
        return JPAHelper.getFirsOrNull(em.createQuery("SELECT m FROM Metrics m WHERE m.metricsValue = :metricsValue", Metrics.class)
                .setParameter("metricsValue", metricsValue)
                .setMaxResults(1));
    }

    /**
     * Finds the Metrics object by ID.
     * @param metricsID ID of the metrics that is used to find the metrics entity from the DB.
     * @return The found metrics entity.
     */
    public Metrics findByID(final int metricsID) {
        return em.find(Metrics.class, metricsID);
    }

    /**
     * Deletes a Metrics entity from the DB.
     * @param metrics Metrics entity that has to be deleted from the DB.
     * @return The deleted metrics entity.
     * @throws CustomizedMessageException
     */
    public Metrics deleteMetrics(final Metrics metrics) throws CustomizedMessageException {
        try {
            em.remove(em.contains(metrics) ? metrics : em.merge(metrics));
            return metrics;
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * This method will find a metrics object for a specific metricsValue and a specific owner.
     * @param metricsValue the String value to be queried.
     * @param user the owner of the metrics entity.
     * @return the found Metrics entity.
     */
    public Metrics findByMetricsValueAndOwner (final String metricsValue, final User user) {
        return JPAHelper.getFirsOrNull(em.createQuery("SELECT m FROM Metrics m WHERE m.metricsValue = :metricsValue AND m.owner = :owner", Metrics.class)
                .setParameter("metricsValue", metricsValue)
                .setParameter("owner", user));
    }
}
