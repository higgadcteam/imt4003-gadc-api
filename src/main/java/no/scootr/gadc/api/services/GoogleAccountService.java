package no.scootr.gadc.api.services;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.GoogleAccount;
import no.scootr.gadc.api.model.GoogleAccountAuthToken;
import no.scootr.gadc.api.model.OAuthToken;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.requestbeans.GoogleAccountRequest;
import no.scootr.gadc.api.utillity.JPAHelper;
import no.scootr.gadc.api.utillity.Util;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This service class is used to query Google Account data and mainly for CRUD operation on those objects.
 */
@Stateless
public class GoogleAccountService {
    @PersistenceContext
    private EntityManager em;
    private final Pattern pattern;

    /**
     * Initialize the email pattern in the constructor of the class.
     */
    public GoogleAccountService() {
        pattern = Pattern.compile(Util.EMAIL_PATTERN);
    }

    /**
     * Queries all the active google accounts from the database and returns a list of the results.
     * If there are no results available, an empty list is returned.
     * @param loggedUser Is the currently logged user into the system.
     * @return List<GoogleAccounts>
     */
    public List<GoogleAccount> getActiveGoogleAccounts(User loggedUser) {
        return em.createQuery("SELECT ga FROM GoogleAccount ga WHERE ga.active = true AND ga.owner.id = :loggedUserId", GoogleAccount.class)
                .setParameter("loggedUserId", loggedUser.getId())
                .getResultList();
    }

    /**
     * Queries all the Google accounts from the database and returns a list of the results.
     * If there are no results available, an empty list is returned.
     * @param loggedUser Is the currently logged in user into the system.
     * @return List<GoogleAccounts>
     */
    public List<GoogleAccount> getAllGoogleAccounts(User loggedUser) {
        return em.createQuery("SELECT ga FROM GoogleAccount ga WHERE ga.owner.id = :loggedUserId", GoogleAccount.class)
                .setParameter("loggedUserId", loggedUser.getId())
                .getResultList();
    }

    /**
     * Accepts a accountID and returns a GoogleAccount object found from the DB.
     * @param accountID ID of the Google Account object.
     * @return GoogleAccount
     */
    public GoogleAccount getGoogleAccount(final int accountID) {
        return em.find(GoogleAccount.class, accountID);
    }

    /**
     * Accepts a String e-mail, and queries the DB to find a GoogleAccount object with the specified e-mail
     * E-mail is unique for each GoogleAccount in the DB.
     * @param gaEmail Google Account email by which the google account entity is queried in the DB.
     * @return GoogleAccount
     */
    public GoogleAccount getGoogleAccount(final String gaEmail) {
        return JPAHelper.getFirsOrNull(
                em.createQuery("SELECT ga FROM GoogleAccount ga WHERE ga.email = :google_mail", GoogleAccount.class)
                        .setParameter("google_mail", gaEmail));
    }

    /**
     * Function adds a GoogleAccount object into the DB. A GoogleAccountRequest is provided as parameter, where it holds
     * all the data necessary to create a GA object.
     * @param googleAccountRequest Request object that contains the information of the google account object to be added.
     * @return GoogleAccount.
     * @throws CustomizedMessageException if the attributes of the GA object are incorrect.
     */
    public GoogleAccount addGoogleAccount(final GoogleAccountRequest googleAccountRequest) throws CustomizedMessageException {
        final Matcher matcher =  pattern.matcher(googleAccountRequest.getEmail());
        validateGoogleAccountAttributes(googleAccountRequest, matcher);


        final User user = em.find(User.class, googleAccountRequest.getUserId());
        if (user == null) {
            throw new CustomizedMessageException("Could not find the user who is registering this request in the Database");
        }

        /**
         * Checks if the new GA is already registered in the DB, by checking its e-mail address.
         */
        final GoogleAccount existingAccount = getGoogleAccount(googleAccountRequest.getEmail());

        if (existingAccount != null) {
            return updateGoogleAccount(googleAccountRequest, user, existingAccount);
        }

        return registerGoogleAccount(googleAccountRequest, user);
    }

    /**
     * Refactored method that registeres the google account from the request.
     * @param googleAccountRequest the google account request object that holds the information to register the account
     * @param user the user that is registering the google account
     * @return the newly registered google account
     */
    private GoogleAccount registerGoogleAccount(GoogleAccountRequest googleAccountRequest, User user) {
        final GoogleAccount newGoogleAccount = new GoogleAccount(
                googleAccountRequest.getEmail(),
                googleAccountRequest.isActive(),
                user);

        em.persist(newGoogleAccount);

        final GoogleAccountAuthToken authToken =
                new GoogleAccountAuthToken(OAuthToken.BEARER,
                        googleAccountRequest.getToken(),
                        googleAccountRequest.getRefreshToken(),
                        googleAccountRequest.getTokenTimeToLive(),
                        googleAccountRequest.getTokenStartTime(),
                        newGoogleAccount);

        em.persist(authToken);
        newGoogleAccount.setOAuthToken(authToken);
        return em.merge(newGoogleAccount);
    }

    /**
     * Refactored method that updates an existing google account object.
     * @param googleAccountRequest the google account request object that has the information of the google account.
     * @param user the user that is updating the google account.
     * @param existingAccount the google account to be updated.
     * @return the modified google account.
     */
    private GoogleAccount updateGoogleAccount(GoogleAccountRequest googleAccountRequest, User user, GoogleAccount existingAccount) {
        if (existingAccount.getOAuthToken() != null) {
            em.remove(existingAccount.getOAuthToken());
        }

        final GoogleAccountAuthToken authToken =
                new GoogleAccountAuthToken(OAuthToken.BEARER,
                        googleAccountRequest.getToken(),
                        googleAccountRequest.getRefreshToken(),
                        googleAccountRequest.getTokenTimeToLive(),
                        googleAccountRequest.getTokenStartTime(),
                        existingAccount);
        existingAccount.setOAuthToken(authToken);
        existingAccount.setOwner(user);

        existingAccount.setActive(true);

        em.persist(authToken);
        em.merge(existingAccount);
        return  existingAccount;
    }


    /**
     * This method will validate the attributes of the google account entity.
     * @param googleAccountRequest the google account request object that holds the information to store a google account
     * @param matcher is a email pattern to validate the email of the google account.
     * @throws CustomizedMessageException
     */
    private void validateGoogleAccountAttributes(GoogleAccountRequest googleAccountRequest, Matcher matcher) throws CustomizedMessageException {
        /**
         * Checks e-mail pattern for correctness.
         */
        if (!matcher.matches()) {
            throw new CustomizedMessageException("E-mail form is incorrect. Please provide a valid e-mail!");
        }

        /**
         * Checks whether the token has a value.
         */
        if (googleAccountRequest.getToken() != null && googleAccountRequest.getToken().isEmpty()) {
            throw new CustomizedMessageException("Google Account Token is empty");
        }

        /**
         * Checks if the User registering the object is initialized.
         */
        if (googleAccountRequest.getUserId() <= 0) {
            throw new CustomizedMessageException("User ID cannot be empty!");
        }
    }

    /**
     * Deletes an existing google account entity from the DB.
     * @param googleAccount Google Account object to be deleted.
     * @return The deleted Google Account entity.
     * @throws CustomizedMessageException
     */
    public GoogleAccount deleteGoogleAccount(final GoogleAccount googleAccount) throws CustomizedMessageException {
        try {
            googleAccount.getGaAccounts().clear();
            em.remove(em.contains(googleAccount) ? googleAccount : em.merge(googleAccount));
            return googleAccount;
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * This method will updated the access token of the Google Account object.
     * @param googleAccount Google Account object to be updated.
     * @param accessToken The new access token to be assigned to the object.
     * @throws CustomizedMessageException When the merge transaction fails to commit the changes.
     */
    public void updateAccessToken(GoogleAccount googleAccount, final GoogleAccountAuthToken accessToken) throws CustomizedMessageException {
        try {
            em.remove(googleAccount.getOAuthToken());
            em.persist(accessToken);
            googleAccount.setOAuthToken(accessToken);
            em.merge(googleAccount);
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * This method is used to change the status of a google account entity.
     * @param googleAccount Entity itself that has to be updated.
     * @param status The status to be assigned to the google account entity.
     * @throws CustomizedMessageException
     */
    public void changeGoogleAccountStatus(final GoogleAccount googleAccount, final boolean status) throws CustomizedMessageException {
        try {
            googleAccount.setActive(status);
            em.merge(googleAccount);
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }


}
