package no.scootr.gadc.api.services;

import no.scootr.gadc.api.model.GoogleAccount;
import no.scootr.gadc.api.utillity.JPAHelper;
import no.scootr.gadc.api.model.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * This service is used to query data for User entity and mainly for CRUD operations on that entity.
 */
@Stateless
public class UserService {

    @PersistenceContext
    private EntityManager em;

    /**
     * Gets or register a user in the system.
     * @param email Email identifying the user.
     * @return If a user exists with the specified email exist, the user is returned. Otherwise, a new user is made.
     */
    public User getOrRegisterUser(final String email) {
        final User registeredUser = getUser(email);
        if (registeredUser == null) {
            final User user = new User(email);
            em.persist(user);

            return user;
        } else {
            return registeredUser;
        }
    }

    /**
     * Gets user by ID.
     * @param id Unique ID for the user.
     * @return The User with the specified ID. {@code null} if not found.
     */
    public User getUserById(final int id) {
        return em.find(User.class, id);
    }

    /**
     * Gets a user by email address.
     * @param email Email address for the user.
     * @return The user with the specified email, {@code null} if not found.
     */
    public User getUser(final String email) {
        final CriteriaBuilder builder = em.getCriteriaBuilder();
        final CriteriaQuery<User> criteria = builder.createQuery(User.class);
        final Root<User> userRoot = criteria.from(User.class);

        criteria.select(userRoot);
        criteria.where(builder.equal(userRoot.get("email"), email));

        return JPAHelper.getFirsOrNull(em.createQuery(criteria));
    }

    /**
     * Checks if a {@code GoogleAccount} belongs to a user.
     * @param user A {@code User}.
     * @param googleAccount A {@code GoogleAccount}.
     * @return {@code true} If the {@code GoogleAccount} belongs to the {@code User}, otherwise false.
     */
    public boolean checkRelationBetweenUserAndGoogleAccount(final User user, final GoogleAccount googleAccount) {
        List<GoogleAccount> googleAccountList = user.getAccounts();

        for (GoogleAccount googleAccountItem : googleAccountList) {
            if (googleAccount != null && googleAccountItem.getId() == googleAccount.getId()) {
                return true;
            }
        }

        return false;
    }
}
