package no.scootr.gadc.api.services;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.Category;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.utillity.JPAHelper;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * This service class is used to query category data and mainly for CRUD operation on category objects.
 */
@Stateless
public class CategoryService {
    @PersistenceContext
    private EntityManager em;

    /**
     * This method will return all the categories found in the database as a Array list.
     * @return The list of all categories found in the DB.
     */
    public List<Category> getCategories() {
        return em.createQuery("SELECT c FROM Category c", Category.class).getResultList();
    }

    /**
     * This method will return all the categories that belong to a specific user in the database as a Array list.
     * @param user is the owner of the category that will be queried.
     * @return The list of all categories found in the DB.
     */
    public List<Category> getCategories(User user) {
        return em.createQuery("SELECT c FROM Category c WHERE c.owner = :owner", Category.class)
                .setParameter("owner", user)
                .getResultList();
    }


    /**
     * This method finds a category using its primary key ID. Null is returned if the Category is not found.
     * @param id ID of the category to be queried.
     * @return The category found from the DB, or a null object if the category with that id does not exist.
     */
    public Category getCategoryById(final int id) {
        return em.find(Category.class, id);
    }

    /**
     * THis method finds a category entity using the categoryName attribute.
     * @param name Name of the category with which the category is queried.
     * @return The category object found.
     */
    public Category getCategory(final String name) {
        final CriteriaBuilder builder = em.getCriteriaBuilder();
        final CriteriaQuery<Category> criteria = builder.createQuery(Category.class);
        final Root<Category> categoryRoot = criteria.from(Category.class);

        criteria.select(categoryRoot);
        criteria.where(builder.equal(categoryRoot.get("name"), name));

        return JPAHelper.getFirsOrNull(em.createQuery(criteria));
    }


    /**
     * This method registers a new category in the DB.
     * @param category Category object that has to be stored in the DB.
     * @throws CustomizedMessageException
     */
    public void add(final Category category) throws CustomizedMessageException {
        try {
            em.persist(category);
        } catch (EJBTransactionRolledbackException e) {
            throw new CustomizedMessageException(e, e.getMessage());
        }
    }

    /**
     * Updates a stored category entry in the database.
     * @param category Category data to update.
     * @return The updated category.
     */
    public Category update(final Category category) {
        return em.merge(category);
    }

    /**
     * Method to edit/modify an existing Category and its attributes.
     * @param category Category object that has to be modified.
     */
    public void edit(final Category category) {
        em.merge(category);
    }

    /**
     * This method will try to delete an existing category entity from the DB.
     * @param category Category object that has to be deleted from the DB.
     * @return The deleted category object.
     * @throws CustomizedMessageException
     */
    public Category deleteCategory (final Category category) throws CustomizedMessageException {
        try {
            em.remove(em.contains(category) ? category : em.merge(category));
            return category;
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * This method checks if the Category provided in the parameter is assigned to the provided web property. It is used
     * to make sure that the same category is not assigned more than once to a web property.
     * @param category Category object that has to be checked for having a web property.
     * @param webProperty Web property object that has to be checked on the relationship with the category.
     * @return A boolean if the given web property exists in the list of web properties of the specified category.
     */
    public boolean containsWebProperty(final Category category, final WebProperty webProperty) {
        final List<WebProperty> webProperties = category.getWebProperties();
        if (webProperties.isEmpty()) {
            return false;
        }

        for (WebProperty wp: webProperties) {
            if (wp.getId().trim().equals(webProperty.getId().trim())) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method deletes the specified Web Property object from the list of webProperties of the specified category.
     * @param category Category object that the web property has to be deleted from.
     * @param webProperty Web property that has to be deleted from the list of web properties of the given category.
     */
    public void deleteWebPropertyFromCategory(final Category category, final WebProperty webProperty) {
        List<WebProperty> webProperties = category.getWebProperties();
        for (int i = 0; i < webProperties.size(); i++) {
            if (webProperties.get(i).getId().trim().equals(webProperty.getId().trim())) {
                webProperties.remove(i);
                return;
            }
        }
    }
}