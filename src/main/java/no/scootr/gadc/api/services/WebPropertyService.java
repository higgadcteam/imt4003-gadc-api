package no.scootr.gadc.api.services;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.Category;
import no.scootr.gadc.api.model.GAAccount;
import no.scootr.gadc.api.model.Tag;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.requestbeans.WebPropertyRequest;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * This service is used to query data for a Web Property entity and mainly for CRUD operations on that entity.
 */
@Stateless
public class WebPropertyService {
    @PersistenceContext
    private EntityManager em;

    /**
     * Finds a Web Property (Profile) object given the ID.
     * @param id Is the String value that is used to find the web property entity in the DB.
     * @return The found web property entity.
     */
    public WebProperty getWebPropertyById (final String id) {
        return em.find(WebProperty.class, id);
    }

    /**
     * Adds a Web Property in the DB. Takes as parameters a WebPropertyRequest object which contains the data to register
     * a web property and a GAAccount object to associate it with.
     * @param webPropertyRequest The request object that contains the information needed to add a web property in the DB.
     * @param gaAccount Is the google analytics account object that is used to be assigned as the owner of the web property.
     * @return The registered/stored web property entity.
     * @throws CustomizedMessageException
     */
    public WebProperty addWebProperty (final WebPropertyRequest webPropertyRequest, final GAAccount gaAccount) throws CustomizedMessageException {

        /**
         * Checks whether the name of the web property(Profile) has a value.
         */
        if (webPropertyRequest.getName() != null && webPropertyRequest.getName().isEmpty()) {
            throw new CustomizedMessageException("Name of the Web Property (Profile) must not be empty");
        }

        /**
         * Checks if the Google Analytics Account object has been initialized.
         */
        if (gaAccount == null) {
            throw new CustomizedMessageException("Could not find the GoogleAnalytics Account with the provided ID");
        }

        try {
            final WebProperty webproperty = new WebProperty(webPropertyRequest.getId(), webPropertyRequest.getName(), gaAccount);
            em.persist(webproperty);

            return webproperty;
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * Method to edit/modify an existing Web Property and its attributes.
     * @param webProperty Web property entity that will be edited/modified.
     * @throws CustomizedMessageException
     */
    public void edit(final WebProperty webProperty) throws CustomizedMessageException {
        try {
            em.merge(webProperty);
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * Accepts an ID and queries the database to find all the web properties from a specific Google Analytics account.
     */
    public List<WebProperty> getWebProperties(final long gaId) {
        return em.createQuery("SELECT wp FROM WebProperty wp WHERE wp.gaAccount.id = :gaid", WebProperty.class)
                .setParameter("gaid", gaId)
                .getResultList();
    }

    /**
     * Deletes an existing web property profile entity from the database.
     * @param webProperty Web property entity that will be deleted from the DB.
     * @return The deleted web property entity.
     * @throws CustomizedMessageException
     */
    public WebProperty deleteWebProperty(final WebProperty webProperty) throws CustomizedMessageException {
        try {
            em.remove(em.contains(webProperty) ? webProperty : em.merge(webProperty));
            return webProperty;
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }


    /**
     * This method will add a Web Property if it doesn't exist in the DB, otherwise the object will be updated/merged.
     * @param webProperty Web property entity to be added or updated in the DB.
     * @throws CustomizedMessageException
     */
    public void addOrUpdateWebProperty(final WebProperty webProperty) throws CustomizedMessageException {
        try {
            if (getWebPropertyById(webProperty.getId()) == null) {
                em.persist(webProperty);
            } else {
                em.merge(webProperty);
            }
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * This method checks if the specified category exists in the list of categories of the web property.
     * @param category Category entity that is used to check the relation with the web property.
     * @param webProperty Web property entity that is checked if it contains the given category.
     * @return A boolean value of true if the relation exists between category and web property, and false otherwise.
     */
    public boolean containsCategory(final Category category, final WebProperty webProperty) {
        List<Category> categories = webProperty.getCategories();

        if (categories.isEmpty()) {
            return false;
        }

        for (Category cat: categories) {
            if (cat.getId() == category.getId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method deletes the specified category object from the list of categories of the specified web property
     * @param category Category object that has to be deleted from the relation with the web property.
     * @param webProperty The web property object that will be updated.
     */
    public void deleteCategoryFromWebProperty(final Category category, final WebProperty webProperty) {
        final List<Category> categories = webProperty.getCategories();
        for (int i = 0; i < categories.size(); i++) {
            if (categories.get(i).getId() == category.getId()) {
                categories.remove(i);
                return;
            }
        }
    }

    /**
     * This method checks if the tag provided in the parameter exists in the list of tags
     * of the specified web property. I.E it checks the relationship between tag and web properties (on the Web Property Part).
     * @param tag Tag object that is used to check its relation with the web property.
     * @param webProperty Web property object that is checked if it contains the given tag.
     * @return A boolean value of true if the relation exists between tag and web property, and false if otherwise.
     */
    public boolean containsTag(final Tag tag, final WebProperty webProperty) {
        final List<Tag> tags = webProperty.getTags();

        if (tags.isEmpty()) {
            return false;
        }

        for (Tag webPropertyTag : tags) {
            if (webPropertyTag.getId() == tag.getId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method deletes a tag object from the web property.
     * @param tag Tag to be deleted from the relationship.
     * @param webProperty Web property object to be updated.
     */
    public void deleteTagFromWebProperty(final Tag tag, final WebProperty webProperty) {
        final List<Tag> tags = webProperty.getTags();
        final Tag tagToRemove = tags
                .stream()
                .filter(t -> t.getId() == tag.getId())
                .findFirst()
                .orElse(null);

        if (tagToRemove != null) {
            tags.remove(tagToRemove);
        }
    }
}
