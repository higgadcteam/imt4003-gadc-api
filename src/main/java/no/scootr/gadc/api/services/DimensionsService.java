package no.scootr.gadc.api.services;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.Dimensions;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.utillity.JPAHelper;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * This service class is used to query dimensions data and CRUD operation of the dimensions object.
 */
@Stateless
public class DimensionsService {

    @PersistenceContext
    private EntityManager em;

    /**
     * This method finds all the dimension that a specific user has registered.
     * @param user the user entity that the dimensions have to be queried from
     * @return a list of dimensions that are associated with the user.
     */
    public List<Dimensions> findAllUserDimension(final User user) {
        return em.createQuery("SELECT d FROM Dimensions d WHERE d.status = true AND d.owner = :owner", Dimensions.class)
                .setParameter("owner", user)
                .getResultList();
    }
    /**
     * This method adds a Dimensions entity into the DB.
     * @param dimensions Dimension entity that has to be stored into the DB.
     * @throws CustomizedMessageException
     */
    public void add(final Dimensions dimensions) throws CustomizedMessageException {
        try {
            em.persist(dimensions);
        } catch (Exception ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * This method updates the attributes of a Dimensions entity.
     * @param dimensions Dimensions entity that has to be modified.
     * @throws CustomizedMessageException
     */
    public void edit(final Dimensions dimensions) throws CustomizedMessageException {
        try {
            em.merge(dimensions);
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * This method queries all the active Dimensions in the DB, and returns a list of found entries.
     * @return The list of all active dimensions found in the DB.
     */
    public List<Dimensions> getAllActiveDimensions () {
        return em.createQuery("SELECT d FROM Dimensions d WHERE d.status = true", Dimensions.class)
                .getResultList();
    }

    /**
     * This method finds a Dimensions object by its DimensionValue attribute.
     * @param dimensionValue Is used to find a dimension entity from the DB. It is a unique attribute and therefor can
     * be used to query dimensions.
     * @return The found Dimension entity.
     */
    public Dimensions findByDimensionValue (final String dimensionValue) {
        return JPAHelper.getFirsOrNull(em.createQuery("SELECT d FROM Dimensions d WHERE d.dimensionValue = :dimensionValue", Dimensions.class)
                .setParameter("dimensionValue", dimensionValue));
    }

    /**
     * Finds the Dimensions object by ID.
     * @param dimensionID ID of the dimension entity that is queried in the DB.
     * @return The found Dimension object.
     */
    public Dimensions findByID (final int dimensionID) {
        return em.find(Dimensions.class, dimensionID);
    }

    /**
     * Deletes a Dimension entity from the DB.
     * @param dimension Is the dimension entity that has to be deleted from the DB.
     * @return The deleted dimension entity.
     * @throws CustomizedMessageException
     */
    public Dimensions deleteDimension (final Dimensions dimension) throws CustomizedMessageException {
        try {
            em.remove(em.contains(dimension) ? dimension : em.merge(dimension));
            return dimension;
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }


    /**
     * This method will find a dimension object for a specific dimensionValue and a specific owner.
     * @param dimensionValue the String value to be queried.
     * @param user the owner of the dimension entity.
     * @return the found dimension entity.
     */
    public Dimensions findByDimensionValueAndOwner (final String dimensionValue, final User user) {
        return JPAHelper.getFirsOrNull(em.createQuery("SELECT d FROM Dimensions d WHERE d.dimensionValue = :dimensionValue AND d.owner = :owner", Dimensions.class)
                .setParameter("dimensionValue", dimensionValue)
                .setParameter("owner", user));
    }
}
