package no.scootr.gadc.api.services;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.GAAccount;
import no.scootr.gadc.api.model.GoogleAccount;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.requestbeans.GAAccountAndWebPropertyRequest;
import no.scootr.gadc.api.requestbeans.GAAccountRequest;
import no.scootr.gadc.api.requestbeans.WebPropertyRequest;

import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * This service class is used to query data for Google Analytics Account and mainly for CRUD operations.
 */
@Stateless
public class GAAccountService {
    @PersistenceContext
    EntityManager em;

    @Inject
    WebPropertyService wpService;

    /**
     * Finds a Google Analytics Account given the ID.
     * @param id ID of the Google Analytics Account.
     * @return The GAAccount entity found in the DB with the specified ID.
     */
    public GAAccount findGAAccount(long id) {
        return em.find(GAAccount.class, id);
    }

    /**
     * Queries all the google analytics accounts from the database and returns a list of results.
     * If there are no results available, a NoResultException is fired returning a empty list.
     * @param loggedUser Is the currently logged in User into the system.
     * @return List<GAAccount>
     */
    public List<GAAccount> getAllGAAccounts(final User loggedUser) {
        return em.createQuery("SELECT ga FROM GAAccount ga WHERE ga.owner.owner.id = :loggedUserID", GAAccount.class)
                .setParameter("loggedUserID", loggedUser.getId())
                .getResultList();
    }

    /**
     * This method is used to query all the google analytics account that have the owner specified as a parameter of this
     * method.
     * @param googleAccount The google account object.
     * @return The list of all GAAccounts registered that belong to the specified google account entity.
     */
    public List<GAAccount> getAccountsByOwner(final GoogleAccount googleAccount) {
        return em.createQuery("SELECT ga FROM GAAccount ga WHERE ga.owner = :owner", GAAccount.class)
                .setParameter("owner", googleAccount)
                .getResultList();
    }

    /**
     * Adds a Google Analytics Account into the DB.
     * @param gaAccountRequest The request object that contains the data to add a google analytics object.
     * @return The GAAccount entity that has been stored into the database.
     * @throws CustomizedMessageException
     */
    public GAAccount addGAAccount(final GAAccountRequest gaAccountRequest) throws CustomizedMessageException {
        checkNameAndOwner(gaAccountRequest.getName(), gaAccountRequest.getOwner());

        final GoogleAccount googleAccount = em.find(GoogleAccount.class, gaAccountRequest.getOwner());
        if (googleAccount == null) {
            throw new CustomizedMessageException("Could not find the Google Account with the provided ID");
        }

        try {
            final GAAccount gaAccount = new GAAccount(gaAccountRequest.getName(), googleAccount, gaAccountRequest.getId());
            return em.merge(gaAccount);
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * Checks whether the name and the owner (Google Account) are initialized. If not a CustomizedMessageException is thrown.
     * @param name Name attribute of the Google Analytics account entity.
     * @param owner Owner attribute (Google Account entity) of the Google Analytics Account entity.
     * @throws CustomizedMessageException
     */
    private void checkNameAndOwner(final String name, final Integer owner) throws CustomizedMessageException {
        /**
         * Checks whether the name of the GA Account has a value.
         */
        if (name != null && name.isEmpty()) {
            throw new CustomizedMessageException("Name of the GA Account must not be empty");
        }


        /**
         * Checks if the Google Account (Owner) is initialized.
         */
        if (owner == null || owner <= 0) {
            throw new CustomizedMessageException("Google Account (Owner) property is not provided or is not a valid ID!");
        }
    }

    /**
     * Adds a Google Analytics Account together with its web properties. The parameter is of type GAAccountAndWPRequest
     * which has the name and owner for the GAAccount object, and a list of WebPropertyRequest beans which contain
     * the data to register an individual Web Property.
     * @param gaAccountAndWPRequest is the request object of the GAAccount that has the info to add a GAAccount object
     * into the DB together with its web properties as a list.
     * @return The registered/stored google analytics account entity.
     * @throws CustomizedMessageException
     */
    public GAAccount addGAAccountWithWPs(final GAAccountAndWebPropertyRequest gaAccountAndWPRequest) throws CustomizedMessageException {
        checkNameAndOwner(gaAccountAndWPRequest.getName(), gaAccountAndWPRequest.getOwner());

        GoogleAccount googleAccount = em.find(GoogleAccount.class, gaAccountAndWPRequest.getOwner());
        if (googleAccount == null) {
            throw new CustomizedMessageException("Could not find the Google Account with the provided ID");
        }

        final GAAccount gaAccount = new GAAccount(gaAccountAndWPRequest.getName(), googleAccount, gaAccountAndWPRequest.getId());
        em.persist(gaAccount);

        if (!gaAccountAndWPRequest.getWebPropertyRequestList().isEmpty()) {
            for (WebPropertyRequest w: gaAccountAndWPRequest.getWebPropertyRequestList()) {
                wpService.addWebProperty(w, gaAccount);
            }
        }

        return gaAccount;
    }

    /**
     * Retrieve a Google Analytics Account object from the DB with a specific ID.
     * @param id ID of the google analytics account entity.
     * @return The found google analytics account entity.
     */
    public GAAccount getGAAccountByID(final long id) {
        return em.find(GAAccount.class, id);
    }

    /**
     * Deletes an existing Google Analytics Account from the DB.
     * @param gaAccount The object to be deleted from the DB.
     * @return The deleted object is returned back from the method.
     * @throws CustomizedMessageException
     */
    public GAAccount deleteGAAccount(final GAAccount gaAccount) throws CustomizedMessageException {
        try {
            em.remove(em.contains(gaAccount) ? gaAccount : em.merge(gaAccount));
            return gaAccount;
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }

    /**
     * This method will add a Google Analytics Account if it doesn't exist in the DB, otherwise the object will be
     * updated/merged.
     * @param gaAccount Google Analytics Account entity to be added or updated in the DB.
     * @throws CustomizedMessageException
     */
    public void addOrUpdateGAAccount(final GAAccount gaAccount) throws CustomizedMessageException {
        try {
            if (getGAAccountByID(gaAccount.getId()) == null) {
                em.persist(gaAccount);
            } else {
                em.merge(gaAccount);
            }
        } catch (EJBTransactionRolledbackException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }
}
