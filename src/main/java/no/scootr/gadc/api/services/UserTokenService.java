package no.scootr.gadc.api.services;

import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.model.UserAuthToken;
import no.scootr.gadc.api.utillity.Util;

import javax.annotation.Nullable;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Service for persistence related to user tokens.
 */
@Stateless
public class UserTokenService {
    @PersistenceContext
    private EntityManager em;

    /**
     * This method will remove a UserAuthToken entity from the DB.
     * @param token The UserAuthToken entity to be removed.
     */
    public void removeToken(final UserAuthToken token) {
        if (!em.contains(token)) {
            em.remove(em.merge(token));
        } else {
            em.remove(token);
        }
    }

    /**
     * This method returns the UserAuthToken object from a specific user. It also checks if the token is valid and has not
     * expired yet.
     * @param token The string value of the token provided in the header parameter of the request that has to be checked.
     * @return The UserAuthToken enetity found.
     */
    public UserAuthToken getUserAuthToken(@Nullable final String token) {
        if (Util.isNullOrEmpty(token)) {
            return null;
        }

        final String tokenType = token.substring(0, token.indexOf(" "));
        final String tokenId = token.substring(token.indexOf(" ") + 1, token.indexOf("-"));
        final String tokenValue = token.substring(token.indexOf("-") + 1);

        final UserAuthToken authToken = em.find(UserAuthToken.class, Integer.valueOf(tokenId));

        if (authToken != null && authToken.getRandomTokenValue().equals(tokenValue) && authToken.getTokenType().equals(tokenType)) {
            return authToken;
        } else {
            return null;
        }
    }

    /**
     * This method will create a new User Authentication Token object and will also delete all the other tokens that
     * have been already expired.
     * @param user The user entity for the authentication token to be registered for.
     * @return The registered UserAuthToken entity.
     */
    public UserAuthToken createToken(final User user) {
        List<UserAuthToken> userAuthTokenList = user.getAccessTokens();

        for (UserAuthToken token : userAuthTokenList) {
            if (token.isExpired()) {
                em.remove(em.contains(token) ? token : em.merge(token));
            }
        }
        final UserAuthToken token = new UserAuthToken(user);
        em.persist(token);
        return token;
    }


    /**
     * This method will query a refresh token if it exists in the DB or not.
     * @param refreshToken The refresh token value that has to be queried.
     * @return The user entity associated with the provided refresh token.
     */
    public User findRefreshToken(final String refreshToken) {
        final TypedQuery<UserAuthToken> findRefreshToken = em.createQuery("SELECT uat FROM UserAuthToken uat WHERE uat.refreshToken = :refreshToken", UserAuthToken.class)
                .setParameter("refreshToken", refreshToken)
                .setMaxResults(1);

        List<UserAuthToken> userAuthTokenList = findRefreshToken.getResultList();
        if (userAuthTokenList.isEmpty()) {
            return null;
        }

        /**
         * Return the first value of the array since it will always contain only one.
         */
        return userAuthTokenList.get(0).getUser();
    }
}
