package no.scootr.gadc.api.utillity.json;

import no.scootr.gadc.api.utillity.Util;

import javax.annotation.Nullable;
import javax.ejb.Stateless;
import javax.json.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Class for merging JSON objects.
 */
@Stateless
public class JsonMerger {
    /**
     * Merges an array of JSON objects.
     * @param objects Objects to merge.
     * @return A merged JSON object.
     */
    public JsonObject mergeObjects(@Nullable final JsonObject[] objects) {
        if (objects == null) {
            return null;
        }

        final Map<String, JsonValue> mergedObject = new HashMap<>();

        for (JsonObject object : objects) {
            for (Map.Entry<String, JsonValue> entry : object.entrySet()) {

                final String key = entry.getKey();
                final JsonValue value = entry.getValue();

                if (!mergedObject.containsKey(key)) {
                    mergedObject.put(key, value);
                } else {
                    JsonValue existingValue = mergedObject.get(key);
                    mergedObject.remove(key);
                    mergedObject.put(key, merge(existingValue, value));
                }
            }
        }

        final JsonObjectBuilder builder = Json.createObjectBuilder();

        for (Map.Entry<String, JsonValue> valueSet : mergedObject.entrySet()) {
            builder.add(valueSet.getKey(), valueSet.getValue());
        }

        return builder.build();
    }

    /**
     * Merges two {@code JsonValue} instances by combining arrays, merging objects,
     * adding numbers and making an array out of values.
     * @param a First object to merge.
     * @param b Other object to merge.
     * @return A merged value of a and b.
     */
    public JsonValue merge(final JsonValue a, final JsonValue b) {
        if (a.getValueType() == JsonValue.ValueType.NUMBER && Util.isValidDouble(b.toString().replace("\"", ""))) {
            return toJsonNumber(Double.valueOf(a.toString().replace("\"", "")) + Double.valueOf(b.toString().replace("\"", "")));
        } else if (b.getValueType() == JsonValue.ValueType.NUMBER && Util.isValidDouble(a.toString().replace("\"", ""))) {
            return toJsonNumber(Double.valueOf(b.toString().replace("\"", "")) + Double.valueOf(a.toString().replace("\"", "")));
        }

        if (b.getValueType().equals(a.getValueType())) {
            if (a.getValueType() == JsonValue.ValueType.ARRAY) {
                return mergeJsonArray((JsonArray) a, (JsonArray) b);
            } else if (a.getValueType() == JsonValue.ValueType.OBJECT) {
                return mergeJsonObject((JsonObject) a, (JsonObject) b);
            } else if (a.getValueType() == JsonValue.ValueType.NUMBER) {
                return mergeJsonNumber((JsonNumber) a, (JsonNumber) b);
            } else if (Util.isValidDouble(a.toString().replace("\"", ""))
                    && Util.isValidDouble(b.toString().replace("\"", ""))) {
                return toJsonNumber(Double.valueOf(a.toString().replace("\"", "")) + Double.valueOf(b.toString().replace("\"", "")));
            } else if (a.getValueType() != JsonValue.ValueType.NULL) {
                // String, integer or boolean
                return combineOrAdd(a, b);
            } else {
                // Null objects are just merged to null.
                return JsonValue.NULL;
            }
        } else {
            return combineOrAdd(a, b);
        }
    }

    /**
     * Merges a {@code JsonValue} by summing together two values.
     * @param objectA First value to merge.
     * @param objectB Second value to merge.
     * @return Sum of the two objects as a {@code JsonValue}.
     */
    public JsonNumber mergeJsonNumber(final JsonNumber objectA, final JsonNumber objectB) {
        return toJsonNumber(objectA.doubleValue() + objectB.doubleValue());
    }

    private JsonNumber toJsonNumber(final double value) {
        // Necessary as it is not possible to create a JsonValue by hand.
        return Json.createObjectBuilder()
                .add("value", value)
                .build()
                .getJsonNumber("value");
    }

    /**
     * Merges two JsonObjects by merging together their values individually.
     * @param objectA First object to merge.
     * @param objectB Other object to merge.
     * @return A merged JsonValue of the two objects that is the merged object of the two.
     */
    public JsonValue mergeJsonObject(JsonObject objectA, JsonObject objectB) {
        final JsonObjectBuilder objectBuilder = Json.createObjectBuilder();

        for (Map.Entry<String, JsonValue> entry : objectA.entrySet()) {
            if (objectB.containsKey(entry.getKey())) {
                // The same value exists in B, merge them together.
                final JsonValue value = merge(objectA.get(entry.getKey()), entry.getValue());
                final String key = entry.getKey();

                objectBuilder.add(key, value);
            } else {
                // This value does not exist in B.
                objectBuilder.add(entry.getKey(), entry.getValue());
            }
        }

        return objectBuilder.build();
    }

    /**
     * Merges an array JsonValues together.
     * @param first First value to merge.
     * @param second Second value to merge.
     * @return A merged JsonArray of all the values.
     */
    public JsonArray mergeJsonArray(final JsonArray first, final JsonArray second) {
        final JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

        int arrayAi = 0;
        int arrayBi = 0;

        // Merge the two arrays first until they are out of bounds.
        for (; arrayAi < first.size() && arrayAi < second.size(); arrayAi++, arrayBi++) {
            arrayBuilder.add(merge(first.get(arrayAi), second.get(arrayBi)));
        }

        // Merge the extra values
        for (; arrayAi < first.size(); arrayAi++) {
            arrayBuilder.add(first.get(arrayAi));
        }

        for (; arrayBi < second.size(); arrayBi++) {
            arrayBuilder.add(second.get(arrayBi));
        }

        return arrayBuilder.build();
    }

    /**
     * Creates an array out of a set of {@code JsonValue}.
     * @param values All the {@code JsonValue} to create an array out of.
     * @return A {@code JsonArray} of all the arguments to the function.
     */
    public JsonArray makeArray(final JsonValue... values) {
        final JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

        for (JsonValue value : values) {
            arrayBuilder.add(value);
        }

        return arrayBuilder.build();
    }

    /**
     * Creates an array out of two values.
     * @param a First value to make an array out of.
     * @param b Second value to make an array out of.
     * @return An array of a and b.
     */
    public JsonArray combineOrAdd(final JsonValue a, final JsonValue b) {
        final JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

        if (a.getValueType() == JsonValue.ValueType.ARRAY) {
            arrayToList((JsonArray) a).forEach(arrayBuilder::add);
        } else {
            arrayBuilder.add(a);
        }

        if (b.getValueType() == JsonValue.ValueType.ARRAY) {
            arrayToList((JsonArray) b).forEach(arrayBuilder::add);
        } else {
            arrayBuilder.add(b);
        }

        return arrayBuilder.build();
    }

    private List<JsonValue> arrayToList(final JsonArray array) {
        final List<JsonValue> values = new LinkedList<>();

        for (JsonValue value : array) {
            if (value.getValueType() == JsonValue.ValueType.ARRAY) {
                values.addAll(arrayToList((JsonArray) value));
            } else {
                values.add(value);
            }
        }

        return values;
    }
}
