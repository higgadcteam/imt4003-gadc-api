package no.scootr.gadc.api.utillity.lazyloading;

/**
 * Generic lazy loading specifying the methods each lazy loader has to implement.
 * The lazy loaders are used when obtaining attributes from an entity that is lazily loaded.
 */
public interface GenericLazyLoader<T> {
    /**
     * Makes sure that the context object is loaded
     * @param proxy Object to load lazily.
     * @return The lazily loaded object.
     */
    T assertLoaded(T proxy);
}
