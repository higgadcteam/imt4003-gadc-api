package no.scootr.gadc.api.utillity.lazyloading;

import org.hibernate.Hibernate;
import org.hibernate.Session;

import javax.persistence.*;

/**
 * Lazy loader that updates and fetches the data from the database.
 */
public class LazyLoader<T> implements GenericLazyLoader<T> {

    /**
     * Associates the context object that has the proxy object being subject to lazy loading with a session.
     * @param context Entity that has the a reference to the attribute being subject to lazy loading.
     */
    public LazyLoader(Object context) {
        final EntityManager em = Persistence.createEntityManagerFactory("mainPu").createEntityManager();
        final Session session = (Session) em.getDelegate();

        try {
            session.update(context);
        } catch (Exception e) {
            session.close();
            throw e;
        }
    }

    /**
     * Lazily loads the object.
     * @param context Object to lazy load.
     * @return The object that was lazily loaded.
     */
    @Override
    public T assertLoaded(final T context) {
        Hibernate.initialize(context);
        return context;
    }
}
