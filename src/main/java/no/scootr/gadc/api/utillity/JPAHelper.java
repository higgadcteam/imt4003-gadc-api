package no.scootr.gadc.api.utillity;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Helper class to get cleaner code with JPA.
 */
public class JPAHelper {

    private JPAHelper() {
    }

    /**
     * Finds the first result in a query.
     * @param query Query to get the result for.
     * @param <T> What type is being queried from the database.
     * @return The object being queried, {@code null} if not found.
     */
    public static <T> T getFirsOrNull(final TypedQuery<T> query) {
        final List<T> results = query.getResultList();

        return !results.isEmpty() ? results.get(0) : null;
    }
}
