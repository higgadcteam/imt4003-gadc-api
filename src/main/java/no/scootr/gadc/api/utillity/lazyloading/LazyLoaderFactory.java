package no.scootr.gadc.api.utillity.lazyloading;

import org.hibernate.Hibernate;

/**
 * Factory class for lazy loading.
 */
public class LazyLoaderFactory {

    private LazyLoaderFactory() {
    }

    /**
     * Creates a new lazy loader for a member. If the object is already loaded, a lazy loader that returns the object
     * is returned. If not, a lazy loader that will load the instance is returned.
     * @param owner The entity that owns the object.
     * @param member Member of the entity that will be lazy loaded.
     * @param <T> Type of the object being lazy loaded.
     * @return A lazy loader depending on if the member/attribute is already loaded.
     */
    public static <T> GenericLazyLoader<T> using(Object owner, T member) {
        if (Hibernate.isInitialized(member)) {
            return new IgnoringLoader<>();
        } else {
            return new LazyLoader<>(owner);
        }
    }
}
