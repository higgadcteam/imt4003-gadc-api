package no.scootr.gadc.api.utillity;


import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Helper class for sending HTTP calls to a REST-full web service.
 * This code is based on http://stackoverflow.com/a/27246948/1924825
 */
public class ApiCall {
    private HttpURLConnection conn;

    /**
     * Prepares a new API call to a HTTP server..
     * @param url The URL of the HTTP resource.
     * @param method The HTTP method, eg. GET, POST or DELETE.
     * @throws IOException Invalid URL.
     */
    public ApiCall(String url, String method) throws IOException {
        this.conn = (HttpURLConnection) new URL(url).openConnection();
        this.conn.setDoOutput(true);
        this.conn.setRequestMethod(method);
        this.conn.setRequestProperty("Content-Type", "application/json");
    }

    /**
     * Prepares a new API call to a HTTP server.
     * @param url URL of the HTTP resource.
     * @param accessToken The Authorization access token.
     * @param method The HTTP method, eg. GET, POST or DELETE.
     * @throws IOException
     */
    public ApiCall(String url, String accessToken, String method) throws IOException {
        this(url, method);
        setHeader("Authorization", accessToken);
    }

    /**
     * Sets the content type for the HTTP request.
     * @param type The content type of the request, eg. application/json.
     */
    public void setContentType(String type) {
        setHeader("Content-Type", type);
    }

    /**
     * Adds a new header value to the HTTP request.
     * @param key The key of the HTTP header.
     * @param value Actual value of the HTTP header.
     */
    public void setHeader(String key, String value) {
        this.conn.setRequestProperty(key, value);
    }

    /**
     * Writes a text message to the HTTP resource.
     * @param string The string that will be written to the API.
     * @throws IOException Network issues. The website may be down.
     */
    public void writeString(String string) throws IOException {
        conn.getOutputStream().write(string.getBytes());
        conn.getOutputStream().flush();
    }

    /**
     * Gets the HTTP response code of the request.
     * @return An integer that represents the HTTP result, eg. 404 for HTTP 404 NOT FOUND.
     * @throws IOException Unable to access the network, the website may be down.
     */
    public int getResponse() throws IOException {
        return conn.getResponseCode();
    }

    /**
     * Reads the response as a JSON object.
     * @return JSON response.
     * @throws IOException Communication issues.
     */
    public JsonObject readJsonResponse() throws IOException {
        try (final JsonReader parser = Json.createReader(new InputStreamReader(conn.getInputStream(), "UTF-8"))) {
            return parser.readObject();
        }
    }
}
