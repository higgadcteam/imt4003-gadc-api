package no.scootr.gadc.api.utillity;

/**
 * Utility class.
 */
public class Util {
    public static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final String GA_API_SECRET = System.getenv("GA_API_SECRET");
    public static final String GA_API_CLIENT_ID = System.getenv("GA_API_CLIENT_ID");

    private Util() {
    }

    /**
     * Checks if a string is null or empty.
     * @param string String to check if is null or empty.
     * @return {@code true} if null or empty, otherwise {@code false}.
     */
    public static boolean isNullOrEmpty(final String string) {
        return string == null || string.length() == 0;
    }

    /**
     * Checks if a value is a valid {@code double}.
     * @param value Value to check if is true.
     * @return {@code true} if valid, otherwise {@code false}.
     */
    public static boolean isValidDouble(final String value) {
        // TODO: Refactor this by using a scanner  .

        if (isNullOrEmpty(value)) {
            return false;
        }

        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
