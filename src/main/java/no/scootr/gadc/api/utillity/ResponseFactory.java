package no.scootr.gadc.api.utillity;

import no.scootr.gadc.api.model.ErrorMessage;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 * Creates exceptions and responses.
 */
public class ResponseFactory {
    private ResponseFactory() {
    }

    /**
     * Creates a new {@code WebApplicationException} with a response entity describing the cause.
     * @param message Message of the exception.
     * @param responseCode The response code for the response to the client.
     * @param cause Exception that caused the exception.
     * @return A new {@code WebApplicationException}
     */
    public static WebApplicationException buildError(final String message, final Response.Status responseCode, final Throwable cause) {
        final ErrorMessage error = new ErrorMessage(message);
        final Response response = Response.status(responseCode)
                .entity(error)
                .build();

        return new WebApplicationException(cause, response);
    }

    /**
     * Creates a new {@code WebApplicationException} with a message and a response code
     * @param message Message of the exception.
     * @param responseCode The response code for the response to the client.
     * @return A new {@code WebApplicationException}
     */
    public static WebApplicationException buildError(final String message, final Response.Status responseCode) {
        return buildError(message, responseCode, null);
    }

    /**
     * Creates a new {@code WebApplicationException} with a message and a response code
     * @param message Message of the exception.
     * @return A new {@code WebApplicationException}
     */
    public static WebApplicationException buildError(final String message) {
        return buildError(message, Response.Status.INTERNAL_SERVER_ERROR, null);
    }

    /**
     * Creates a new {@code Response} with a response code and an entity.
     * @param responseCode HTTP Response status.
     * @param entity The object for the response that will be serialized.
     * @return A new {@code Response}.
     */
    public static Response create(final Response.Status responseCode, final Object entity) {
        return Response.status(responseCode)
                .entity(entity)
                .build();
    }

    /**
     * Creates a new {@code Response} with a response code and an entity.
     * @param entity The object for the response that will be serialized.
     * @return A new {@code Response}.
     */
    public static Response create(final Object entity) {
        return create(Response.Status.OK, entity);
    }
}
