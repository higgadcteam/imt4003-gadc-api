package no.scootr.gadc.api.utillity.lazyloading;

/**
 * Lazy loader that does not do anything as the proxy object is already loaded.
 */
public class IgnoringLoader<T> implements GenericLazyLoader<T> {
    @Override
    public T assertLoaded(T proxy) {
        return proxy;
    }
}
