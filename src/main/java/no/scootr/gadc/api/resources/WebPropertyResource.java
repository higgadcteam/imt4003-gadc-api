package no.scootr.gadc.api.resources;

import io.swagger.annotations.*;
import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.*;
import no.scootr.gadc.api.requestbeans.WebPropertyRequest;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.security.filters.*;
import no.scootr.gadc.api.services.GAAccountService;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.services.WebPropertyService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.LinkedList;
import java.util.List;

/**
 * Resource that handles retrieving web properties, adding ones, categorizing and tagging.
 */
@Path("management")
@Api(value = "Web Properties", description = "Retrieving web properties, adding, categorizing and tagging",
        authorizations = {@Authorization(OAuthToken.DEFAULT_AUTH_SCHEME)})
public class WebPropertyResource {

    @Inject
    WebPropertyService wpService;
    @Inject
    GAAccountService gaaService;
    @Inject
    UserTokenService userTokenService;
    @Context
    SecurityContext securityContext;

    /**
     * This method takes the ID of Google Analytics account and then returns all the web properties associated with that
     * google analytic account.
     * @param gaAccountId Google analytics account id.
     * @return WebProperties
     */
    @GET
    @Path("accounts/{accountId}/webproperties")
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(GAAccountFilter.class)
    @ApiOperation(value = "Get web properties for an account", response = WebProperty.class, responseContainer = "List")
    @ApiResponses({
            @ApiResponse(code = 404, message = "Google Analytics account with the specified ID was not found", response = ErrorMessage.class, responseContainer = "List"),
            @ApiResponse(code = 200, message = "Found the web properties", response = WebProperty.class),
    })
    public List<WebProperty> getWebPropertiesForAccount(
            @ApiParam(value = "Google Analytics account ID", required = true) @PathParam("accountId") final int gaAccountId) {
        final GAAccount gaAccount = gaaService.findGAAccount(gaAccountId);
        if (gaAccount == null) {
            throw ResponseFactory.buildError("Couldn't find the Google Analytics Account with the specified ID: " + gaAccountId, Response.Status.NOT_FOUND);
        }

        return gaAccount.getWebProperties();
    }

    @GET
    @Path("webproperties")
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(AuthorizationFilter.class)
    @ApiOperation(value = "Get all web properties", response = WebProperty.class, responseContainer = "List")
    @ApiResponses({
            @ApiResponse(code = 404, message = "Google Analytics account with the specified ID was not found", response = ErrorMessage.class, responseContainer = "List"),
            @ApiResponse(code = 200, message = "Found the web properties", response = WebProperty.class),
    })
    public List<WebProperty> getWebProperties() {
        final User currentUser = userTokenService.getUserAuthToken(securityContext.getUserPrincipal().getName()).getUser();

        final List<WebProperty> webProperties = new LinkedList<>();

        for (GoogleAccount googleAccount : currentUser.getAccounts()) {
            for (GAAccount gaAccount : googleAccount.getGaAccounts()) {
                webProperties.addAll(gaAccount.getWebProperties());
            }
        }

        return webProperties;
    }


    /**
     * Method takes as parameter a WebPropertyRequest with all the necessary attributes to store a Web Property(Profile).
     * The services method addWebProperty adds the object into the DB. If the transaction fails, a CustomizedMessageException
     * will be thrown and the corresponding message will be returned as a WebApplicationException in the Http Response.
     */
    @POST
    @Path("accounts/{accountId}/webproperties")
    @AccessConstraint(GAAccountFilter.class)
    @ApiOperation(value = "Add a web property", response = WebProperty.class)
    @ApiResponses({@ApiResponse(code = 201, message = "Web property created", response = WebProperty.class)})
    public Response addWebProperty(
            @ApiParam(value = "Google Analytics account ID", required = true) @PathParam("accountId") long accountId,
            @ApiParam(value = "Information about the web property to add", required = true) final WebPropertyRequest webPropertyRequest) {
        final GAAccount gaAccount = gaaService.findGAAccount(accountId);

        try {
            return ResponseFactory.create(Response.Status.CREATED, wpService.addWebProperty(webPropertyRequest, gaAccount));
        } catch (CustomizedMessageException e) {
            throw new WebApplicationException(e);
        }
    }

    /**
     * Deletes a Web Property with the specified ID from a Google Analytics Account.
     * @param id ID of the Web property object to be deleted.
     * @return The deleted web property from the DB as a json response entity.
     */
    @DELETE
    @Path("accounts/{accountId}/webproperties/{webPropertyId}")
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(GAAccountFilter.class)
    @ApiOperation(value = "Remove a web property", response = WebProperty.class)
    @ApiResponses({
            @ApiResponse(code = 404, message = "A web property with the specified ID was not found", response = ErrorMessage.class),
            @ApiResponse(code = 200, message = "Found the web properties", response = WebProperty.class),
    })
    public Response deleteWebProperty (@PathParam("webPropertyId") final String id) {
        final WebProperty webProperty = wpService.getWebPropertyById(id);

        if (webProperty != null) {
            try {
                return ResponseFactory.create(wpService.deleteWebProperty(webProperty));
            } catch (CustomizedMessageException cme) {
                throw new WebApplicationException(cme);
            }
        } else {
            throw ResponseFactory.buildError("WebProperty not found", Response.Status.NOT_FOUND);
        }
    }

    /**
     * This method returns a CategoryWebPropertyResource object which contains GET,POST and DELETE methods for
     * categorizing a web property.
     * @return CategoryWebPropertyResource
     */
    @Path("accounts/{accountId}/webproperties/{webPropertyID}/category")
    @ApiOperation(value = "Category SubResource", response = TagWebPropertyResource.class)
    public Class<CategoryWebPropertyResource> getCategoryWebPropertyResource () {
        return CategoryWebPropertyResource.class;
    }

    /**
     * This method returns a TagWebPropertyResource object which contains GET,POST and DELETE methods for
     * tagging a web property.
     * @return TagWebPropertyResource
     */
    @Path("accounts/{accountId}/webproperties/{webPropertyID}/tag")
    @AccessConstraint(GAAccountFilter.class)
    @ApiOperation(value = "Tag SubResource", response = TagWebPropertyResource.class)
    public Class<TagWebPropertyResource> getTagWebPropertyResource () {
        return TagWebPropertyResource.class;
    }

    @Path("/webproperties/{webPropertyID}/profiles")
    @AccessConstraint(WebPropertyFilter.class)
    @ApiOperation(value = "Profiles SubResource", response = TagWebPropertyResource.class)
    public Class<ProfileResource> getWebPropertyProfileResource () {
        return ProfileResource.class;
    }
}

