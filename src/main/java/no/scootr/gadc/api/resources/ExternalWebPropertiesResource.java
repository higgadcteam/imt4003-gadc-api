package no.scootr.gadc.api.resources;

import io.swagger.annotations.*;
import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.*;
import no.scootr.gadc.api.google.GoogleWebPropertyService;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.security.filters.GAAccountFilter;
import no.scootr.gadc.api.services.GAAccountService;
import no.scootr.gadc.api.services.ProfileService;
import no.scootr.gadc.api.services.WebPropertyService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.json.*;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * Core reporting API for Web Properties.
 */
@Path("reporting/gaa/{googleAnalyticsAccountID}")
@Api(value = "Web Properties Core Reporting API",
        description = "Resource for managing data on records with data from Google for Google Analytics Accounts",
        authorizations = {@Authorization(OAuthToken.DEFAULT_AUTH_SCHEME)})
public class ExternalWebPropertiesResource {
    public static final String ITEMS_FIELD = "items";

    @Inject
    GAAccountService gaAccountService;
    @Inject
    WebPropertyService webPropertyService;
    @Inject
    GoogleWebPropertyService googleWebPropertyService;
    @Inject
    ProfileService profileService;

    /**
     * This method retrieves all the Web Properties associated with the given Google Analytics Account.
     * @param googleAnalyticsAccountID The id of the Google Analytics Account.
     * @return The list of profiles formatted as a json response.
     */
    @GET
    @Path("/webproperties")
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(GAAccountFilter.class)
    @ApiOperation(value = "Get web properties", response = JsonObject.class, notes = "See https://developers.google.com/analytics/devguides/config/mgmt/v3/mgmtReference/#Web Properties for more information")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found web properties"),
            @ApiResponse(code = 404, message = "A Google Analytics Account with this ID does not exist", response = ErrorMessage.class),
            @ApiResponse(code = 503, message = "Unable to communicate with Google services", response = ErrorMessage.class)})
    public Response getProfilesOfGAAccount(
            @ApiParam(value = "Google Analytics Account ID", required = true) @PathParam("googleAnalyticsAccountID") final long googleAnalyticsAccountID) {
        final GAAccount gaAccount = checkGAAccount(googleAnalyticsAccountID);
        try {
            final JsonObject webProperties = googleWebPropertyService.apiCallGetWebProperties(gaAccount, googleAnalyticsAccountID);
            return ResponseFactory.create(webProperties.getJsonArray(ITEMS_FIELD));
        } catch (CustomizedMessageException ex) {
            throw ResponseFactory.buildError(ex.getMessage(), Response.Status.INTERNAL_SERVER_ERROR, ex);
        }

    }

    /**
     * This method is used to update the web properties of a google analytics account. By update, it is meant to get
     * all the web properties from Google API and store them in the local DB.
     * @param googleAnalyticsAccountID The id of the Google Analytics Account that the web properties will be update to.
     * @return The Google Analytics Account object together with the list of web properties associated with it.
     */
    @GET
    @Path("/updateGAAccounts")
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(GAAccountFilter.class)
    @ApiOperation(value = "Update web properties", response = WebProperty.class, responseContainer = "List")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successfully updated the web properties", response = WebProperty.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "A Google Analytics Account with this ID does not exist", response = ErrorMessage.class),
            @ApiResponse(code = 503, message = "Unable to communicate with Google services", response = ErrorMessage.class)})
    public Response updateWebProperties(
            @ApiParam(value = "Google Analytics Account ID", required = true) @PathParam("googleAnalyticsAccountID") final long googleAnalyticsAccountID) {
        final GAAccount gaAccount = checkGAAccount(googleAnalyticsAccountID);

        try {
            final JsonObject webProperties = googleWebPropertyService.apiCallGetWebProperties(gaAccount, googleAnalyticsAccountID);

            /**
             * Get all the WebProperties from the DB for this Google Analytics Account.
             */
            final List<WebProperty> webPropertiesDB = webPropertyService.getWebProperties(googleAnalyticsAccountID);

            final List<WebProperty> webPropertiesAPI = new ArrayList<>();
            final List<String> webPropertiesKeysAPI = new ArrayList<>();

            for (int i = 0; i < webProperties.getJsonArray(ITEMS_FIELD).size(); i++) {
                final WebProperty webProperty = new WebProperty();
                final JsonObject jObj = webProperties.getJsonArray(ITEMS_FIELD).getJsonObject(i);
                webProperty.setId(jObj.getString("id"));
                webProperty.setName(jObj.getString("name"));
                webProperty.setWebsiteURL(jObj.getString("websiteUrl"));
                webProperty.setGaAccount(gaAccount);
                webPropertiesAPI.add(webProperty);
                webPropertiesKeysAPI.add(jObj.getString("id"));
                webPropertyService.addOrUpdateWebProperty(webProperty);
            }

            /**
             * Delete Google Analytics Account from DB that are not available from the Google API anymore.
             */
            for (WebProperty dbAPI : webPropertiesDB) {
                if (!webPropertiesKeysAPI.contains(dbAPI.getId())) {
                    webPropertyService.deleteWebProperty(dbAPI);
                }
            }

            /**
             * Return the list of Web Properties retrieved from the API call.
             */
            final GenericEntity<List<WebProperty>> genericWebPropertyListEntity = new GenericEntity<List<WebProperty>>(webPropertiesAPI) {
            };
            return ResponseFactory.create(genericWebPropertyListEntity);
        } catch (CustomizedMessageException ex) {
            throw ResponseFactory.buildError(ex.getMessage(), Response.Status.INTERNAL_SERVER_ERROR, ex);
        }
    }

    /**
     * This method is used to get the list of profiles/views of a web property (Simplified).
     * @param googleAnalyticsAccountID The ID of the Google Analytics Account.
     * @param webPropertyID The id of the web property.
     * @return The views/profiles of the specified Google Analytics Account entity as a json response entity.
     */
    @GET
    @Path("/webproperty/{webPropertyID}/views")
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(GAAccountFilter.class)
    @ApiOperation(value = "Get views", response = JsonArray.class, notes = "See [ref] for more information")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successfully fetched the views"),
            @ApiResponse(code = 404, message = "A Google Analytics Account with this ID does not exist", response = ErrorMessage.class),
            @ApiResponse(code = 503, message = "Unable to communicate with Google services", response = ErrorMessage.class)})
    public Response getWebPropertyViews(
            @ApiParam(value = "Google Analytics Account ID", required = true) @PathParam("googleAnalyticsAccountID") final long googleAnalyticsAccountID,
            @ApiParam(value = "Web Property ID", required = true) @PathParam("webPropertyID") final String webPropertyID) {
        final GAAccount gaAccount = checkGAAccount(googleAnalyticsAccountID);

        try {
            final JsonObject views = googleWebPropertyService.apiCallGetViewsOfWebProperty(webPropertyID, gaAccount);

            WebProperty webProperty = webPropertyService.getWebPropertyById(webPropertyID);
            if (webProperty == null) {
                throw ResponseFactory.buildError("Unable to find the Web Property with the specified ID: " + webPropertyID, Response.Status.NOT_FOUND);
            }

            JsonArray viewList = views.getJsonArray(ITEMS_FIELD);
            JsonObject viewElement;
            final JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

            for (int i = 0; i < viewList.size(); i++) {
                viewElement = viewList.getJsonObject(i);
                final JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
                objectBuilder.add("ProfileID", viewElement.getString("id"));
                objectBuilder.add("Profile Name", viewElement.getString("name"));
                arrayBuilder.add(objectBuilder);
                final Profile profile = new Profile(Long.valueOf(viewElement.getString("id")), viewElement.getString("name"));
                profile.setWebProperty(webProperty);
                if (profileService.getProfileByID(profile.getId()) == null) {
                    profileService.addProfile(profile);
                } else {
                    profileService.editProfile(profile);
                }
            }

            return ResponseFactory.create(arrayBuilder.build());
        }  catch (CustomizedMessageException cme) {
            throw new WebApplicationException(cme);
        }
    }

    /**
     * This method will check if the Google Analytics Account exists. If not, an Exception with a custom
     * message will be thrown.
     * @param googleAnalyticsAccountID The Google Analytics Account.
     * @return The GAAccount.
     */
    private GAAccount checkGAAccount(final long googleAnalyticsAccountID) {
        final GAAccount gaAccount = gaAccountService.getGAAccountByID(googleAnalyticsAccountID);
        if (gaAccount == null) {
            throw ResponseFactory.buildError("Google Analytics Account does not exist", Response.Status.NOT_FOUND);
        }

        return gaAccount;
    }
}
