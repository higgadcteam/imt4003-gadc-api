package no.scootr.gadc.api.resources;

import io.swagger.annotations.*;
import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.ErrorMessage;
import no.scootr.gadc.api.model.Profile;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.security.filters.WebPropertyFilter;
import no.scootr.gadc.api.services.ProfileService;
import no.scootr.gadc.api.services.WebPropertyService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Sub-Resource that handles the requests for adding, modifying and deleting a Profile of a Web Property.
 * Created by Brikendi on 11/24/2015.
 */
@Path("/profiles")
@Api(hidden = true)
public class ProfileResource {

    @Inject
    ProfileService profileService;
    @Inject
    WebPropertyService webPropertyService;

    /**
     * This method will retrieve all the registered Profiles of the specified Web Property.
     * @param webPropertyID the ID of the web property entity that the profiles will be retrieved from.
     * @return the list of the found profiles.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(WebPropertyFilter.class)
    @ApiOperation(value = "Get Profiles", response = Profile.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Found the Profiles of the Web Property", response = Profile.class, responseContainer = "List")})
    public Response getProfilesOfWebProperty(
            @ApiParam(value = "Web property ID", required = true) @PathParam("webPropertyID") final String webPropertyID) {
        WebProperty webProperty = webPropertyService.getWebPropertyById(webPropertyID);
        return ResponseFactory.create(webProperty.getProfiles());
    }


    /**
     * This method will find a Profile entity with the specified ID.
     * @param profileID the id value of the Profile to be queried.
     * @return the found Profile object.
     */
    @GET
    @Path("/{profileID}")
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(WebPropertyFilter.class)
    @ApiOperation(value = "Get Profile by ID", response = Profile.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found the Profile", response = Profile.class),
            @ApiResponse(code = 404, message = "Profile with the specified ID does not exist", response = ErrorMessage.class)})
    public Response getProfileByID(
            @ApiParam(value = "Profile ID", required = true) @PathParam("profileID") final int profileID) {
        Profile profile = profileService.getProfileByID(profileID);

        if (profile == null) {
            throw ResponseFactory.buildError("Unable to find Profile object with ID: " + profileID, Response.Status.NOT_FOUND);
        }

        return ResponseFactory.create(profile);
    }


    /**
     * This method will delete a Profile associated with a web property from the DB.
     * @param profileID the id of the profile object to be deleted.
     * @return the deleted Profile object.
     */
    @DELETE
    @Path("/{profileID}")
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(WebPropertyFilter.class)
    @ApiOperation(value = "Delete Profile", response = Profile.class, notes = "Does not delete the profile of the web property from Google, only the data stored on records in this API.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Profile removed", response = Profile.class),
            @ApiResponse(code = 404, message = "No Profile object with this ID exists", response = ErrorMessage.class)})
    public Response deleteProfileFromWebProperty(
            @ApiParam(value = "Profile ID", required = true) @PathParam("profileID") final int profileID) {
        Profile profile = profileService.getProfileByID(profileID);

        if (profile == null) {
            throw ResponseFactory.buildError("Unable to find Profile object with ID: " + profileID, Response.Status.NOT_FOUND);
        }
        try {
            return ResponseFactory.create(profileService.deleteProfile(profile));
        } catch (CustomizedMessageException ex) {
            throw new WebApplicationException(ex.getMessage(), ex);
        }
    }
}
