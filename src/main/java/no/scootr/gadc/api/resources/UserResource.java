package no.scootr.gadc.api.resources;

import io.swagger.annotations.*;
import no.scootr.gadc.api.google.GoogleService;
import no.scootr.gadc.api.model.OAuthToken;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.model.UserAuthToken;
import no.scootr.gadc.api.requestbeans.LoginRequest;
import no.scootr.gadc.api.requestbeans.RefreshTokenRequest;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.security.filters.AuthorizationFilter;
import no.scootr.gadc.api.services.UserService;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.io.IOException;

/**
 * Resource for users.
 */
@Path("users")
@Api(value = "Users", description = "Resource for user and token management",
        authorizations = {@Authorization(OAuthToken.DEFAULT_AUTH_SCHEME)})
public class UserResource {

    @Inject
    UserService userService;

    @Inject
    GoogleService googleService;

    @Inject
    UserTokenService userTokenService;

    @Context
    SecurityContext securityContext;

    /**
     * This method is used to retrieve the information for a specific user.
     * @param id ID of the User object.
     * @return The user entity as a json response.
     */
    @GET
    @Path("{id}")
    @AccessConstraint(AuthorizationFilter.class)
    @ApiOperation(value = "Get user", response = User.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found the user"),
            @ApiResponse(code = 404, message = "User not found")})
    public Response getUserInfo(@ApiParam("User ID") @PathParam("id") final int id) {
        final User user = userService.getUserById(id);

        if (user != null) {
            return ResponseFactory.create(user);
        } else {
            throw ResponseFactory.buildError("User not found", Response.Status.NOT_FOUND);
        }
    }


    /**
     * This method is used to retrieve the user requesting the data.
     * @return User information for the current user requesting the data.
     */
    @GET
    @Path("me")
    @AccessConstraint(AuthorizationFilter.class)
    @ApiOperation(value = "Get user", response = User.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found the user")})
    public Response getMyUserInfo() {
        final UserAuthToken token = userTokenService.getUserAuthToken(securityContext.getUserPrincipal().getName());
        return ResponseFactory.create(token.getUser());
    }

    /**
     * This method is used to authenticate a registered user.
     * @param loginRequest Request object that contains the data for logging in.
     * @return The logged in user as a json response entity.
     */
    @Path("token")
    @POST
    @ApiOperation(value = "Create new auth token", response = UserAuthToken.class)
    @ApiResponses({
            @ApiResponse(code = 202, message = "Provided token was valid"),
            @ApiResponse(code = 503, message = "Unable to communicate with Google services or invalid token")
    })
    public Response login(
            @ApiParam(value = "Google auth token") final LoginRequest loginRequest) {
        try {
            final OAuthToken token = googleService.getOauthToken(loginRequest.getToken(), System.getenv("GA_API_LOGIN_REDIRECTION_URL"));
            final JsonObject accountInformation = googleService.getUserInfo(token.getTokenValue());

            final User user = userService.getOrRegisterUser(accountInformation.getString("email"));

            return ResponseFactory.create(Response.Status.ACCEPTED, userTokenService.createToken(user));
        } catch (IOException e) {
            throw ResponseFactory.buildError("Unable to contact Google services", Response.Status.SERVICE_UNAVAILABLE, e);
        }
    }

    /**
     * Invalidates the current token the user uses to access this service.
     * @return A HTTP 200 OK response.
     */
    @Path("token")
    @DELETE
    @AccessConstraint(AuthorizationFilter.class)
    @ApiOperation(value = "Invalidate auth token")
    @ApiResponses({@ApiResponse(code = 200, message = "Provided token was valid", response = AuthorizationFilter.class)})
    public Response invalidateToken() {
        final UserAuthToken token = userTokenService.getUserAuthToken(securityContext.getUserPrincipal().getName());
        userTokenService.removeToken(token);

        return Response.status(Response.Status.OK).build();
    }

    @Path("/refreshtoken")
    @POST
    @Produces("application/json")
    @ApiOperation(value = "Create new auth token using the refresh token", response = UserAuthToken.class)
    @ApiResponses({
            @ApiResponse(code = 202, message = "Provided token was valid"),
            @ApiResponse(code = 400, message = "Provided refresh token does not match with the refresh token from the DB.")
    })
    public Response getUserRefreshToken(
            @ApiParam(value = "User refresh token")final RefreshTokenRequest refreshTokenRequest) {

        User user = userTokenService.findRefreshToken(refreshTokenRequest.getRefreshToken());

        if (user == null) {
            throw ResponseFactory.buildError("The provided refresh token does not match with the one registered in the DB", Response.Status.BAD_REQUEST);
        }

        return ResponseFactory.create(Response.Status.ACCEPTED, userTokenService.createToken(user));
    }

    // TODO: Remove this method as it is for debugging purposes only
    @Path("token")
    @GET
    @AccessConstraint(AuthorizationFilter.class)
    public Response getToken() {
        final UserAuthToken token = userTokenService.getUserAuthToken(securityContext.getUserPrincipal().getName());

        return ResponseFactory.create(token);
    }
}