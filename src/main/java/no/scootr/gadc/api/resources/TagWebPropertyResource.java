package no.scootr.gadc.api.resources;

import io.swagger.annotations.*;
import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.ErrorMessage;
import no.scootr.gadc.api.model.Tag;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.requestbeans.TagRequest;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.security.filters.GAAccountFilter;
import no.scootr.gadc.api.services.TagService;
import no.scootr.gadc.api.services.WebPropertyService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Sub-resource for tagging web properties.
 */
@Path("/")
@Api(hidden = true)
public class TagWebPropertyResource {

    @Inject
    WebPropertyService wpService;
    @Inject
    TagService tagService;

    /**
     * This method will tag a web property.
     * @param webPropertyID ID of the Web Property object.
     * @param tagRequest Tag Request object that holds the tag Name to be assigned to the web property.
     * @return The tagged web property as a json response entity.
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(GAAccountFilter.class)
    @ApiOperation(value = "Tag a web property", response = WebProperty.class)
    @ApiResponses({
            @ApiResponse(code = 201, message = "Web property tagged", response = WebProperty.class),
            @ApiResponse(code = 404, message = "No web property with the specified ID exists", response = ErrorMessage.class)})
    public Response tagWebProperty(
            @ApiParam(value = "Web property ID", required = true) @PathParam("webPropertyID") final String webPropertyID,
            @ApiParam(value = "Data about the new tag", required = true) final TagRequest tagRequest) {
        try {
            final WebProperty webProperty = getWebProperty(webPropertyID);

            Tag tag = tagService.getTagByName(tagRequest.getTagName());
            if (tag == null) {
                tag = new Tag();
                tag.setName(tagRequest.getTagName());
                tagService.addTag(tag);
            }

            /**
             * Assign web property to the tag.
             */
            if (!tagService.containsWebProperty(tag, webProperty)) {
                tag.getWebProperties().add(webProperty);
                tagService.edit(tag);
            }

            /**
             * Assign tag to web property.
             */
            if (!wpService.containsTag(tag, webProperty)) {
                webProperty.getTags().add(tag);
                wpService.edit(webProperty);
            }

            return ResponseFactory.create(Response.Status.CREATED, webProperty);
        } catch (CustomizedMessageException cme) {
            throw new WebApplicationException(cme);
        }
    }

    /**
     * This method will will delete the relationship between a tag and a web property.
     * @param webPropertyID ID of the Web Property object to be updated.
     * @param tagID ID of the Tag object that will be deleted from the web property.
     * @return The updated web property as a json response entity.
     */
    @DELETE
    @Path("/{tagID}")
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(GAAccountFilter.class)
    @ApiOperation(value = "Remove a tag from a web property", response = WebProperty.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Tag successfully removed from the web property", response = WebProperty.class),
            @ApiResponse(code = 404, message = "No web property with the specified ID exists", response = ErrorMessage.class)})
    public Response deleteTagFromWebProperty(
            @ApiParam(value = "Web property ID", required = true) @PathParam("webPropertyID") final String webPropertyID,
            @ApiParam(value = "Tag ID") @PathParam("tagID") final int tagID) {
        try {
            final WebProperty webProperty = getWebProperty(webPropertyID);

            Tag tag = tagService.getTagByID(tagID);
            if (tag == null) {
                throw ResponseFactory.buildError("Tag Not Found", Response.Status.NOT_FOUND);
            }

            /**
             * Check if the tag has the specified web property in his webProperties list. If yes, it will be deleted
             * from the list (and also from the table in the DB).
             */
            if (tagService.containsWebProperty(tag, webProperty)) {
                tagService.deleteWebPropertyFromTag(tag, webProperty);
                tagService.edit(tag);
            }

            /**
             * Check if the web property has the specified tag in his tag list. If yes, it will be deleted
             * from the list (and also from the table in the DB).
             */
            if (wpService.containsTag(tag, webProperty)) {
                wpService.deleteTagFromWebProperty(tag, webProperty);
                wpService.edit(webProperty);
            }

            return ResponseFactory.create(webProperty);
        } catch (CustomizedMessageException cme) {
            throw new WebApplicationException(cme);
        }
    }

    /**
     * This method checks if a Web Property object exists. If not an Exception with a custom error message is
     * thrown.
     * @param webPropertyID Web property object to be checked.
     */
    private WebProperty getWebProperty(final String webPropertyID) {
        final WebProperty webProperty = wpService.getWebPropertyById(webPropertyID);
        if (webProperty == null) {
            throw ResponseFactory.buildError("WebProperty not found", Response.Status.NOT_FOUND);
        }

        return webProperty;
    }
}
