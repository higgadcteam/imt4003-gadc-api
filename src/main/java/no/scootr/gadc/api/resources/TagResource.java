package no.scootr.gadc.api.resources;

import io.swagger.annotations.*;
import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.ErrorMessage;
import no.scootr.gadc.api.model.OAuthToken;
import no.scootr.gadc.api.model.Tag;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.requestbeans.TagRequest;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.security.filters.AuthorizationFilter;
import no.scootr.gadc.api.services.TagService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Tag Resource is used to get the list of tags, register or update a tag, delete a tag, and get the web properties of a tag.
 */

@Path("tags")
@Api(value = "Tags", description = "Manage tags", authorizations = {@Authorization(OAuthToken.DEFAULT_AUTH_SCHEME)})
@AccessConstraint(AuthorizationFilter.class)
public class TagResource {

    @Inject
    TagService tagService;

    /**
     * This method find all the tags registered in the DB and returns a json list as a response.
     * @return The list of all tags found in the DB.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get tags", response = Tag.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Found the tags")})
    public List<Tag> getAllTags() {
        return tagService.findAll();
    }

    /**
     * This method finds a tag entity using the tag ID and it returns the result as a json response.
     * @param tagID ID of Tag object.
     * @return The founded Tag formatted as a json response entity.
     */
    @GET
    @Path("/{tagID}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get tag", response = Tag.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found the tag", response = Tag.class),
            @ApiResponse(code = 404, message = "No tag with the specified ID exists", response = ErrorMessage.class)})
    public Response findTagByID(@ApiParam(value = "Tag ID", required = true) @PathParam("tagID") final int tagID) {
        final Tag tag = tagService.getTagByID(tagID);

        if (tag == null) {
            throw ResponseFactory.buildError("Could not find Tag with the specified ID: " + tagID, Response.Status.NOT_FOUND);
        }
        return ResponseFactory.create(tag);
    }

    /**
     * This method retrieves all the web properties that have the tag provided as a parameter.
     * @param tagName The tag name.
     * @return The list of all web properties that have the specified tag.
     */
    @GET
    @Path("{tagName}/webProperties")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get web properties for a tag", response = WebProperty.class, responseContainer = "List")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found the web properties", response = WebProperty.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "No tag with the specified ID exists", response = ErrorMessage.class)})
    public List<WebProperty> getWebPropertiesByTag(
            @ApiParam(value = "Tag name", required = true) @PathParam("tagName") final String tagName) {
        final Tag tag = tagService.getTagByName(tagName);

        if (tag == null) {
            throw ResponseFactory.buildError("Could not find Tag with the specified tag: " + tagName, Response.Status.NOT_FOUND);
        }

        return tag.getWebProperties();
    }


    /**
     * This method registers a new tag in the DB.
     * @param tagRequest Tag request object that holds the tag name to be registered.
     * @return The registered/stored tag object into the DB as a json response entity.
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Register tag", response = Tag.class)
    @ApiResponses({@ApiResponse(code = 201, message = "Created the tag", response = Tag.class)})
    public Response registerTag(@ApiParam(value = "Data for the new tag", required = true) final TagRequest tagRequest) {
        /**
         * Get Tag object.
         */
        Tag tag = tagService.getTagByName(tagRequest.getTagName());

        try {
            if (tag == null) {
                tag = new Tag();
                tag.setName(tagRequest.getTagName());
                tagService.addTag(tag);
            }

            return ResponseFactory.create(Response.Status.CREATED, tag);
        } catch (CustomizedMessageException cme ) {
            throw new WebApplicationException(cme);
        }
    }
}
