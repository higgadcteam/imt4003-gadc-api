package no.scootr.gadc.api.resources;

import io.swagger.annotations.*;
import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.Category;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.requestbeans.CategorizeWebPropertyRequest;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.security.filters.CategoryFilter;
import no.scootr.gadc.api.security.filters.GAAccountFilter;
import no.scootr.gadc.api.services.CategoryService;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.services.WebPropertyService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Sub-resource class that is used to categorize and delete a category from a web property.
 */
@Path("/category")
@Api(hidden = true)
public class CategoryWebPropertyResource {
    @Inject
    WebPropertyService wpService;
    @Inject
    CategoryService categoryService;
    @Inject
    UserTokenService userTokenService;

    @Context
    SecurityContext securityContext;

    /**
     * This method categorizes a web property.
     * @param webPropertyID The ID of the web property entity that is used to find the object from the DB.
     * @param categorizeWebPropertyRequest Contains the category id that has to be assigned to the web property.
     * @return The categorized Web Property as a json response entity.
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(GAAccountFilter.class)
    @ApiOperation(value = "Categorize a web property", response = WebProperty.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Successfully categorized the web property", response = WebProperty.class)})
    public Response categorizeWebProperty(
            @ApiParam(value = "Web property ID", required = true) @PathParam("webPropertyID") final String webPropertyID,
            @ApiParam(value = "Categorization request", required = true) final CategorizeWebPropertyRequest categorizeWebPropertyRequest) {
        try {
            /**
             * Get the web property object.
             */
            final WebProperty webProperty = wpService.getWebPropertyById(webPropertyID);
            checkWebProperty(webProperty);

            /**
             * Get category Object.
             */
            final Category category = categoryService.getCategoryById(categorizeWebPropertyRequest.getCategoryID());
            checkCategory(category);

            /**
             * Check if the user has permission to access the specified category that the web property will be categorized
             * with.
             */
            final User loggedUser = userTokenService.getUserAuthToken(securityContext.getUserPrincipal().getName()).getUser();

            if (category.getOwner() == null || category.getOwner().getId() != loggedUser.getId()) {
                throw ResponseFactory.buildError("403 Forbidden! Seems that you do not have permission to access this resource.", Response.Status.FORBIDDEN);
            }

            /**
             * Assign web property to the category.
             */
            if (!categoryService.containsWebProperty(category, webProperty)) {
                category.getWebProperties().add(webProperty);
                categoryService.edit(category);
            }

            /**
             * Assign category to web property.
             */
            if (!wpService.containsCategory(category, webProperty)) {
                webProperty.getCategories().add(category);
                wpService.edit(webProperty);
            }

            return ResponseFactory.create(webProperty);
        } catch (CustomizedMessageException cme) {
            throw new WebApplicationException(cme);
        }
    }


    /**
     * This method will delete an assigned category to the web property.
     * @param webPropertyID The id of the web property object that the category has to be deleted from.
     * @param categoryID The id of the category object that has to be deleted from the category list of the given web property object.
     * @return The updated Web Property as a json response entity.
     */
    @DELETE
    @Path("/{categoryID}")
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint({CategoryFilter.class, GAAccountFilter.class})
    @ApiOperation(value = "Remove a category from a web property", response = WebProperty.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Removed the category from the web property", response = WebProperty.class)})
    public Response deleteCategoryFromWebProperty(
            @ApiParam(value = "Web property ID", required = true) @PathParam("webPropertyID") final String webPropertyID,
            @ApiParam(value = "Category ID", required = true) @PathParam("categoryID") final int categoryID) {
        try {
            /**
             * Get the web property object.
             */
            WebProperty webProperty = wpService.getWebPropertyById(webPropertyID);
            checkWebProperty(webProperty);

            /**
             * Get category Object.
             */
            Category category = categoryService.getCategoryById(categoryID);
            checkCategory(category);

            /**
             * Check if the category has the specified web property in his webProperties list. If yes, it will be deleted
             * from the list (and also from the table in the DB).
             */
            if (categoryService.containsWebProperty(category, webProperty)) {
                categoryService.deleteWebPropertyFromCategory(category, webProperty);
                categoryService.edit(category);
            }

            /**
             * Check if the web property has the specified category in his categories list. If yes, it will be deleted
             * from the list (and also from the table in the DB).
             */
            if (wpService.containsCategory(category, webProperty)) {
                wpService.deleteCategoryFromWebProperty(category, webProperty);
                wpService.edit(webProperty);
            }

            return ResponseFactory.create(webProperty);
        } catch (CustomizedMessageException cme) {
            throw new WebApplicationException(cme);
        }
    }

    /**
     * This method checks if a Category object has been initialized. If not a Exception with a custom error message is
     * thrown.
     * @param category The category object that has to be checked if it is initialized or not.
     */
    private void checkCategory(Category category) {
        if (category == null) {
            throw ResponseFactory.buildError("Category not found", Response.Status.NOT_FOUND);
        }
    }

    /**
     * This method checks if a Web Property object has been initialized. If not a Exception with a custom error message is
     * thrown.
     * @param webProperty The web property object that has to be checked if it is initialized or not.
     */
    private void checkWebProperty(WebProperty webProperty) {
        if (webProperty == null) {
            throw ResponseFactory.buildError("WebProperty not found", Response.Status.NOT_FOUND);
        }
    }
}
