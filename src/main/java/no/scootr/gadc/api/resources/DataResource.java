package no.scootr.gadc.api.resources;

import io.swagger.annotations.Api;
import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.google.GoogleAnalyticsService;
import no.scootr.gadc.api.model.*;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.security.filters.AuthorizationFilter;
import io.swagger.annotations.*;
import no.scootr.gadc.api.services.ProfileService;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.utillity.json.JsonMerger;

import javax.inject.Inject;
import javax.json.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Core reporting API
 */
@Path("data/ga")
@Api(value = "Core Reporting API", description = "Fetches and aggregates data for several different Google Analytics view",
        authorizations = {@Authorization(OAuthToken.DEFAULT_AUTH_SCHEME)})
public class DataResource {

    @Inject
    ProfileService profileService;

    @Inject
    UserTokenService userTokenService;

    @Inject
    GoogleAnalyticsService googleAnalyticsService;

    @Inject
    JsonMerger jsonMerger;

    @Context
    SecurityContext securityContext;


    @GET
    @ApiOperation(value = "Get Google Analytics data",
            response = JsonObject.class,
            notes = "Will merge the JSON response from the specified IDs. For further information regarding the response see https://developers.google.com/analytics/devguides/reporting/core/v3/reference")
    @ApiResponses({@ApiResponse(code = 200, message = "Received data and merged it from Google")})
    @AccessConstraint(AuthorizationFilter.class)
    public JsonObject getAnalyticsData(@Context final UriInfo ui,
            @ApiParam(value = "IDs of profiles", required = true) @QueryParam("ids") final List<String> ids) {
        final MultivaluedMap<String, String> queryParameters = ui.getQueryParameters();

        final List<JsonObject> apiResponses = new LinkedList<>();
        final User user = userTokenService.getUserAuthToken(securityContext.getUserPrincipal().getName()).getUser();

        for (String profileId : ids) {
            final Profile profile = profileService.getProfileByID(Integer.valueOf(profileId.substring(profileId.indexOf(":") + 1)));
            if (profile.getWebProperty().getGaAccount().getOwner().getOwner().getId() != user.getId()) {
                throw new WebApplicationException("Forbidden access to profile with ID " + profileId, Response.Status.FORBIDDEN);
            }


            final StringBuilder resourceParameters = new StringBuilder();
            for (String key : queryParameters.keySet()) {
                queryParameters
                        .get(key)
                        .stream()
                        .filter(value -> !"ids".equals(key))
                        .forEach(value -> resourceParameters
                                .append(key)
                                .append("=")
                                .append(value)
                                .append("&"));
            }

            resourceParameters
                    .append("ids=")
                    .append(profileId);


            try {
                apiResponses.add(googleAnalyticsService.getAnalyticsData(resourceParameters.toString(), profile));
            } catch (CustomizedMessageException e) {
                throw new WebApplicationException(e);
            }
        }

        final JsonObject[] responseObjects = apiResponses.toArray(new JsonObject[apiResponses.size()]);
        return jsonMerger.mergeObjects(responseObjects);
    }
}
