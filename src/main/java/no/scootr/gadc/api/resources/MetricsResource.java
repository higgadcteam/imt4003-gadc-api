package no.scootr.gadc.api.resources;

import io.swagger.annotations.*;
import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.ErrorMessage;
import no.scootr.gadc.api.model.Metrics;
import no.scootr.gadc.api.model.OAuthToken;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.requestbeans.MetricsRequest;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.security.filters.AuthorizationFilter;
import no.scootr.gadc.api.security.filters.MetricsFilter;
import no.scootr.gadc.api.services.MetricsService;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.List;

/**
 * Resource that handles the requests for adding, modifying and deleting a metrics.
 */
@Path("management/metrics")
@Api(value = "Metrics",
        description = "Resource for adding, modifying and deleting a metric",
        authorizations = {@Authorization(OAuthToken.DEFAULT_AUTH_SCHEME)})
public class MetricsResource {

    @Inject
    MetricsService metricsService;
    @Inject
    UserTokenService userTokenService;
    @Context
    SecurityContext securityContext;

    /**
     * This method retrieves all the registered metrics in the database and returns a list of the found entities.
     * @return The list of Metrics registered in the DB.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(AuthorizationFilter.class)
    @ApiOperation(value = "Get metrics", response = Metrics.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Found the metrics", response = Metrics.class, responseContainer = "List")})
    public List<Metrics> getAllAvailableMetrics() {
        final User loggedUser = userTokenService.getUserAuthToken(securityContext.getUserPrincipal().getName()).getUser();

        return metricsService.findAllUserMetrics(loggedUser);
    }

    /**
     * This method returns a single Metrics entity as a json response to the server. It takes the metrics id as a
     * parameter and uses it to find the object in the DB.
     * @param metricsID The ID of the metrics which is used to find a metrics entity in the DB.
     * @return The metrics object as a json response.
     */
    @GET
    @Path("/{metricsID}")
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(MetricsFilter.class)
    @ApiOperation(value = "Get metric", response = Metrics.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found the metrics", response = Metrics.class),
            @ApiResponse(code = 404, message = "Metrics does not exist", response = ErrorMessage.class)})
    public Response getSingleMetrics(
            @ApiParam(value = "Metrics ID", required = true) @PathParam("metricsID") final int metricsID) {
        final Metrics metrics = metricsService.findByID(metricsID);

        if (metrics == null) {
            throw ResponseFactory.buildError("Metrics not found", Response.Status.NOT_FOUND);
        }

        return ResponseFactory.create(metrics);
    }

    /**
     * This method is used to register or update a metrics. It takes as parameter a MetricsRequest object which
     * holds the attributes and their corresponding values that are required to register a metrics in the DB.
     * @param metricsRequest The request object that contains the information needed to store a metrics entity in the DB.
     * @return The registered or updated Metrics Object.
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(AuthorizationFilter.class)
    @ApiOperation(value = "Add new metrics", response = Metrics.class)
    @ApiResponses({
            @ApiResponse(code = 400, message = "Empty metrics name and/or value", response = ErrorMessage.class),
            @ApiResponse(code = 201, message = "Created metrics", response = Metrics.class)})
    public Response addNewMetrics(
            @ApiParam(value = "Information about the metrics to add", required = true) final MetricsRequest metricsRequest) {

        final User loggedUser = userTokenService.getUserAuthToken(securityContext.getUserPrincipal().getName()).getUser();
        /**
         * Validate Metrics Attributes.
         */
        validateMetricsAttributes(metricsRequest);

        /**
         * Register Metrics into the DB if it doesn't already exist.
         */
        try {
            Metrics metrics = metricsService.findByMetricsValueAndOwner(metricsRequest.getMetricsValue().toLowerCase(), loggedUser);

            if (metrics != null && metrics.getOwner().getId() == loggedUser.getId()) {
                metrics.setMetricsName(metricsRequest.getMetricsName());
                metrics.setMetricsValue(metricsRequest.getMetricsValue().toLowerCase());
                metrics.setStatus(metricsRequest.getStatus());
                metricsService.edit(metrics);

                return ResponseFactory.create(metrics);
            } else {
                metrics = new Metrics(metricsRequest.getMetricsName(), metricsRequest.getMetricsValue().toLowerCase(), metricsRequest.getStatus());
                metrics.setOwner(loggedUser);
                metricsService.add(metrics);

                return ResponseFactory.create(Response.Status.CREATED, metrics);
            }

        } catch (CustomizedMessageException cme) {
            throw new WebApplicationException(cme);
        }
    }

    /**
     * This method will validate the metrics attribute coming from the request.
     * @param metricsRequest the dimension request object that holds the information for a metrix entity.
     */
    private void validateMetricsAttributes(@ApiParam(value = "Information about the metrics to add", required = true) MetricsRequest metricsRequest) {
        if (metricsRequest.getMetricsName() == null || "".equals(metricsRequest.getMetricsName())) {
            throw ResponseFactory.buildError("Metrics Name cannot be empty", Response.Status.BAD_REQUEST);
        }

        if (metricsRequest.getMetricsValue() == null || "".equals(metricsRequest.getMetricsValue())) {
            throw ResponseFactory.buildError("Metrics value is required and cannot be empty", Response.Status.BAD_REQUEST);
        }
    }

    /**
     * This method deletes a metrics from the DB. It takes as parameter the metrics ID for which it uses to find the
     * metrics object and then delete it.
     * @param metricsID The ID of the metrics that is used to find the metrics entity and then delete it from DB.
     * @return The deleted Metrics Object.
     */
    @DELETE
    @Path("/{metricsID}")
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(MetricsFilter.class)
    @ApiOperation(value = "Delete metric", response = Metrics.class)
    @ApiResponses({@ApiResponse(code = 404, message = "Metrics with the specified ID does not exist", response = ErrorMessage.class),
            @ApiResponse(code = 200, message = "Metrics was successfully deleted", response = Metrics.class)})
    public Response deleteMetric(
            @ApiParam(value = "Metrics ID", required = true) @PathParam("metricsID") final int metricsID) {
        final Metrics metrics = metricsService.findByID(metricsID);

        if (metrics == null) {
            throw ResponseFactory.buildError("Metrics not found", Response.Status.NOT_FOUND);
        }

        try {
            return ResponseFactory.create(metricsService.deleteMetrics(metrics));
        } catch (CustomizedMessageException cme) {
            throw new WebApplicationException(cme);
        }
    }
}
