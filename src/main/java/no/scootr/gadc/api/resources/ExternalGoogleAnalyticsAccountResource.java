package no.scootr.gadc.api.resources;

import io.swagger.annotations.*;
import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.ErrorMessage;
import no.scootr.gadc.api.google.GoogleAnalyticsService;
import no.scootr.gadc.api.model.GAAccount;
import no.scootr.gadc.api.model.GoogleAccount;
import no.scootr.gadc.api.model.OAuthToken;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.security.filters.AuthorizationFilter;
import no.scootr.gadc.api.security.filters.GoogleAccountFilter;
import no.scootr.gadc.api.services.GAAccountService;
import no.scootr.gadc.api.services.GoogleAccountService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * Core reporting API for Google Analytics Accounts.
 */
@Path("reporting/ga/{googleAccountID}")
@Api(value = "Google Analytics Accounts Core Reporting API",
        description = "Resource for managing data on records with data from Google for Google Analytics Accounts",
        authorizations = {@Authorization(OAuthToken.DEFAULT_AUTH_SCHEME)})
public class ExternalGoogleAnalyticsAccountResource {
    public static final String ITEMS_FIELD = "items";

    @Inject
    GoogleAccountService googleAccountService;
    @Inject
    GAAccountService gaAccountService;
    @Inject
    GoogleAnalyticsService googleAnalyticsService;

    /**
     * This method will retrieve all the Google Analytics Account from the Core Reporting API.
     * @param googleAccountID The ID integer value which is used to find the google account entity having that ID.
     * @return The found Google Account entity.
     */
    @GET
    @Path("/GAAccounts")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get Google Analytics accounts", response = JsonObject.class, notes = "See https://developers.google.com/analytics/devguides/config/mgmt/v3/mgmtRest for more info")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Got data from Google"),
            @ApiResponse(code = 503, message = "Unable to communicate with Google services", response = ErrorMessage.class)})
    @AccessConstraint(GoogleAccountFilter.class)
    public Response getGAAccounts(
            @ApiParam(value = "Google Account ID", required = true) @PathParam("googleAccountID") final int googleAccountID) {
        try {
            final GoogleAccount account = getAccount(googleAccountID);
            final JsonObject gaAccounts = googleAnalyticsService.apiCallGetGAAccounts(account);

            return ResponseFactory.create(gaAccounts.getJsonArray(ITEMS_FIELD));
        } catch (CustomizedMessageException ex) {
            throw ResponseFactory.buildError(ex.getMessage(), Response.Status.INTERNAL_SERVER_ERROR, ex);
        }
    }

    /**
     * This method will add, update and delete the Google Analytics Account from the Database based on the results
     * retrieved from the API call, that is the list of Google Analytics Accounts.
     * @param googleAccountID The ID integer vale that is used to find the google analytics account entity from DB to be updated.
     * @return The updated google analytics account entity.
     */
    @GET
    @Path("/updateGAAccounts")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Update Google Analytics account", response = GAAccount.class, responseContainer = "List", notes = "Updates information about a Google Analytics account towards Google")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Got data from Google and updated information on record", response = GAAccount.class, responseContainer = "List"),
            @ApiResponse(code = 503, message = "Unable to communicate with Google services", response = ErrorMessage.class)})
    @AccessConstraint(GoogleAccountFilter.class)
    public Response updateGoogleAnalyticsAccount(
            @ApiParam(value = "Google Account ID", required = true) @PathParam("googleAccountID") final Integer googleAccountID) {
        final GoogleAccount googleAccount = getAccount(googleAccountID);
        try {
            final JsonObject gaAccounts = googleAnalyticsService.apiCallGetGAAccounts(googleAccount);



            /**
             * Get all the Google Analytics Accounts from the DB for this Google Account.
             */
            final List<GAAccount> gaAccountsDB = gaAccountService.getAccountsByOwner(googleAccount);
            final List<GAAccount> gaAccountsAPI = new ArrayList<>();
            final List<Long> gaAccountKeysAPI = new ArrayList<>();

            GAAccount gaAccount;
            JsonObject jObj;

            for (int i = 0; i < gaAccounts.getJsonArray(ITEMS_FIELD).size(); i++) {
                gaAccount = new GAAccount();
                jObj = gaAccounts.getJsonArray(ITEMS_FIELD).getJsonObject(i);
                gaAccount.setId(Long.parseLong(jObj.getString("id")));
                gaAccount.setName(jObj.getString("name"));
                gaAccount.setOwner(googleAccount);
                gaAccountsAPI.add(gaAccount);
                gaAccountKeysAPI.add(Long.parseLong(jObj.getString("id")));
                gaAccountService.addOrUpdateGAAccount(gaAccount);
            }

            /**
             * Delete Google Analytics Accounts from DB that are not available from the Google API anymore.
             */
            for (GAAccount dbAPI : gaAccountsDB) {
                if (!gaAccountKeysAPI.contains(dbAPI.getId())) {
                    gaAccountService.deleteGAAccount(dbAPI);
                }
            }

            /**
             * Return the list of Google Analytics Accounts retrieved from the API call.
             */
            final GenericEntity<List<GAAccount>> genericGoogleAnalyticsAccountListEntity = new GenericEntity<List<GAAccount>>(gaAccountsAPI) {
            };

            return ResponseFactory.create(genericGoogleAnalyticsAccountListEntity);
        } catch (CustomizedMessageException ex) {
            throw ResponseFactory.buildError(ex.getMessage(), Response.Status.INTERNAL_SERVER_ERROR, ex);
        }
    }

    /**
     * This method will check if the Google Account object is initialized or not. An Exception will be thrown if the
     * object provided is null.
     * @param googleAccountID ID of the Google account to get.
     */
    @AccessConstraint(AuthorizationFilter.class)
    private GoogleAccount getAccount(final int googleAccountID) {
        final GoogleAccount googleAccount = googleAccountService.getGoogleAccount(googleAccountID);

        if (googleAccount == null) {
            throw ResponseFactory.buildError("Google Account does not exist", Response.Status.NOT_FOUND);
        }

        return googleAccount;
    }
}
