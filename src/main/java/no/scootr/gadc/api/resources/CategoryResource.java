package no.scootr.gadc.api.resources;

import io.swagger.annotations.*;
import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.*;
import no.scootr.gadc.api.requestbeans.CategoryRequest;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.security.filters.AuthorizationFilter;
import no.scootr.gadc.api.security.filters.CategoryFilter;
import no.scootr.gadc.api.services.CategoryService;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.List;

/**
 * Resource class that is used to manipulate with category object like adding, editing, deleting and listing categories.
 */

@Path("categories")
@Api(value = "Categories", description = "Manage registered categories in the system", authorizations = {@Authorization(OAuthToken.DEFAULT_AUTH_SCHEME)})
public class CategoryResource {

    @Inject
    CategoryService categoryService;
    @Inject
    UserTokenService userTokenService;
    @Context
    SecurityContext securityContext;

    /**
     * This method queries all the categories from the database and returns it as a json array.
     * @return The list of Categories received from the database.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get all categories")
    @AccessConstraint(AuthorizationFilter.class)
    public List<Category> getAllCategoriesList() {
        final User loggedUser = userTokenService.getUserAuthToken(securityContext.getUserPrincipal().getName()).getUser();
        return categoryService.getCategories(loggedUser);
    }

    /**
     * This method finds a category by ID and returns all its information back as a json object.
     * @param id The id of the category object that we want to get the information from.
     * @return The response containing the Category formatted as a json object.
     */
    @GET
    @Path("{categoryID}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get category", response = Category.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Found the category", response = Category.class),
            @ApiResponse(code = 404, message = "Category with the specified ID does not exist.", response = ErrorMessage.class)})
    @AccessConstraint(CategoryFilter.class)
    public Response getCategoryInfo(
            @ApiParam(value = "Category ID", required = true) @PathParam("categoryID") final int id) {
        final Category category = categoryService.getCategoryById(id);

        if (category != null) {
            return ResponseFactory.create(Response.Status.OK, category);
        } else {
            throw ResponseFactory.buildError("Category not found", Response.Status.NOT_FOUND);
        }
    }

    /**
     * This method registers a new category in the Database.
     * @param categoryRequest Is the request object that contains the information for a category to be stored.
     * @return The new registered Category.
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Add a new category", response = Category.class)
    @ApiResponses({@ApiResponse(code = 201, message = "The category was created", response = Category.class)})
    @AccessConstraint(AuthorizationFilter.class)
    public Response addCategory(
            @ApiParam(value = "Category request", required = true) final CategoryRequest categoryRequest) {
        Category category = categoryService.getCategory(categoryRequest.getName());
        final User loggedUser = userTokenService.getUserAuthToken(securityContext.getUserPrincipal().getName()).getUser();
        try {
            if (category == null) {
                category = new Category();
                category.setName(categoryRequest.getName());
                category.setOwner(loggedUser);
                categoryService.add(category);
            }
            return ResponseFactory.create(Response.Status.CREATED, category);
        } catch (CustomizedMessageException cme) {
            throw new WebApplicationException(cme);
        }
    }

    /**
     * This method allows modification of an existing category.
     * @param category The request object that contains the information for a category to be updated.
     * @return The updated category.
     */
    @POST
    @Path("/{categoryID}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Update category", response = Category.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The category was updated", response = Category.class)})
    @AccessConstraint(CategoryFilter.class)
    public Response modifyCategory(
            @ApiParam(value = "Category ID", required = true) @PathParam("categoryID") final int categoryId,
            @ApiParam(value = "Category request", required = true) final Category category) {
        final Category storedCategory = categoryService.getCategoryById(categoryId);
        storedCategory.setName(category.getName());

        return ResponseFactory.create(categoryService.update(storedCategory));
    }

    /**
     * This method will delete an existing category from the Database.
     * @param id The id of the category that has to be deleted.
     * @return The deleted Category as a json object.
     */
    @DELETE
    @Path("/{categoryID}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Delete a category", response = Category.class)
    @ApiResponses({
            @ApiResponse(code = 201, message = "Successfully deleted the Category", response = Category.class),
            @ApiResponse(code = 404, message = "The category with the specified ID does not exist", response = ErrorMessage.class)})
    @AccessConstraint(CategoryFilter.class)
    public Response deleteCategory(
            @ApiParam(value = "Category ID", required = true) @PathParam("categoryID") final int id) {
        final Category category = categoryService.getCategoryById(id);

        if (category != null) {
            try {
                return ResponseFactory.create(Response.Status.OK, categoryService.deleteCategory(category));
            } catch (CustomizedMessageException ex) {
                throw new WebApplicationException(ex);
            }
        } else {
            throw ResponseFactory.buildError("Category not found", Response.Status.NOT_FOUND);
        }
    }

    @GET
    @Path("{categoryID}/webProperties")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get a web properties under a category", response = WebProperty.class, responseContainer = "List")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found the web properties", response = WebProperty.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "The category with the specified ID does not exist", response = ErrorMessage.class)})
    @AccessConstraint(CategoryFilter.class)
    public Response getWebPropertiesByCategory(
            @ApiParam(value = "Category ID", required = true) @PathParam("categoryID") final int categoryID) {
        final Category category = categoryService.getCategoryById(categoryID);

        if (category == null) {
            throw ResponseFactory.buildError("Could not find Category with the specified ID: " + categoryID, Response.Status.NOT_FOUND);
        }

        return ResponseFactory.create(category.getWebProperties());
    }
}

