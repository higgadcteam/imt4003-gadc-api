package no.scootr.gadc.api.resources;

import io.swagger.annotations.*;
import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.google.GoogleService;
import no.scootr.gadc.api.model.GoogleAccount;
import no.scootr.gadc.api.model.OAuthToken;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.requestbeans.GoogleAccountRequest;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.security.filters.AuthorizationFilter;
import no.scootr.gadc.api.security.filters.GoogleAccountFilter;
import no.scootr.gadc.api.requestbeans.LoginRequest;
import no.scootr.gadc.api.services.GoogleAccountService;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.io.IOException;
import java.util.List;

/**
 * This resource will handle the requests from Frontend and will manipulate with the Google Account entity (Add, List, Remove, Receive etc).
 */

@Path("management/googleAccounts")
@Api(value = "Google Accounts",
        description = "Manages the Google Account registered for data acquisition",
        authorizations = {@Authorization(OAuthToken.DEFAULT_AUTH_SCHEME)})
public class GoogleAccountResource {
    @Inject
    GoogleAccountService gAccountService;

    @Inject
    GoogleService googleAnalyticsService;
    @Inject
    UserTokenService userTokenService;
    @Context
    SecurityContext securityContext;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get all Google accounts",
            notes = "Gets all the Google accounts registered in the system.",
            response = GoogleAccount.class, responseContainer = "List")
    @AccessConstraint(AuthorizationFilter.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Found the Google accounts", response = GoogleAccount.class, responseContainer = "List")})
    public List<GoogleAccount> getAllGAccountList () {
        final User loggedUser = userTokenService.getUserAuthToken(securityContext.getUserPrincipal().getName()).getUser();

        return gAccountService.getAllGoogleAccounts(loggedUser);
    }

    @GET
    @Path("active")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get all Google accounts that are active",
            notes = "Gets all the Google accounts registered in the system that are active",
            response = GoogleAccount.class, responseContainer = "List")
    @AccessConstraint(AuthorizationFilter.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Found the active Google accounts", response = GoogleAccount.class, responseContainer = "List")})
    public List<GoogleAccount> getActiveGAccounts() {
        final User loggedUser = userTokenService.getUserAuthToken(securityContext.getUserPrincipal().getName()).getUser();

        return gAccountService.getActiveGoogleAccounts(loggedUser);
    }

    /**
     * This function adds a new google account into the DB. Two Exceptions might be fired while
     * trying to add the account. RegistrationException in this case might be fired if the email is invalid,
     * and the CustomizedMessageException might be fired if email is empty, token is empty etc.
     */

    @POST
    @ApiOperation(value = "Add a new Google account",
            notes = "Registers a new Google account in the system",
            response = GoogleAccount.class)
    @AccessConstraint(AuthorizationFilter.class)
    @ApiResponses({@ApiResponse(code = 201, message = "Created the Google account", response = GoogleAccount.class)})
    public Response addNewGoogleAccount(
            @ApiParam(value = "Information about the new Google account", required = true) final GoogleAccountRequest gAccountRequest) {
        try {
            return ResponseFactory.create(Response.Status.CREATED, gAccountService.addGoogleAccount(gAccountRequest));
        } catch (CustomizedMessageException e) {
            throw new WebApplicationException(e.getMessage(), e);
        }
    }

    /**
     * Retrieves a google account based on the id parameter passed. If the google account with that id
     * is not found, a WebApplicationException is fired with the a specific error message.
     */
    @GET
    @Path("/{googleAccountID}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get Google account information",
            notes = "Gets information about a registered Google account",
            response = GoogleAccount.class)
    @AccessConstraint(GoogleAccountFilter.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Found the Google account", response = GoogleAccount.class)})
    public Response getGoogleAccount(
            @ApiParam(value = "Google account ID", required = true) @PathParam("googleAccountID") final int googleAccountID) {
        final GoogleAccount googleAccount = gAccountService.getGoogleAccount(googleAccountID);

        if (googleAccount != null) {
            return ResponseFactory.create(googleAccount);
        } else {
            throw ResponseFactory.buildError("Google Account not found", Response.Status.NOT_FOUND);
        }
    }

    /**
     * Deletes a google account with the specified ID from the DB. If the google account with that ID
     * is not found in the DB a WebApplicationException is fired with a 404 status code and error message.
     * If found, the gAccountService will try to delete it, if the delete fails a CustomizedMessageException is
     * fired with an error message which is passed to the WebApplicationException as a HTTP Response.
     */
    @DELETE
    @Path("/{googleAccountID}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Remove a registered Google account",
            notes = "Deletes an existing Google account together with its registered data",
            response = GoogleAccount.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Removed the Google account", response = GoogleAccount.class)})
    @AccessConstraint(GoogleAccountFilter.class)
    public Response deleteGoogleAccount(@PathParam("googleAccountID") final int googleAccountID) {
        final GoogleAccount googleAccount = gAccountService.getGoogleAccount(googleAccountID);

        if (googleAccount != null) {
            try {
                return ResponseFactory.create(gAccountService.deleteGoogleAccount(googleAccount));
            } catch (CustomizedMessageException cme) {
                throw new WebApplicationException(cme.getMessage(), cme);
            }
        } else {
            throw ResponseFactory.buildError("Google Account not found", Response.Status.NOT_FOUND);
        }
    }

    /**
     * This method registers a new Google Account to the database by providing only the authorization code retrieved
     * from google api as a header parameter.
     * @param request Contains the authorization code that has to be assigned to the google account entity.
     * @return The registered Google Account as a json response entity.
     */
    @POST
    @Path("/syncGoogleAccount")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Update Google accounts",
            notes = "Updates records for a Google account with the data provided by Google",
            response = GoogleAccount.class)
    @ApiResponses({@ApiResponse(code = 201, message = "Created the Google account", response = GoogleAccount.class)})
    @AccessConstraint(AuthorizationFilter.class)
    public Response registerAccountWithToken(
            @ApiParam("Authorization token") final LoginRequest request) {
        try {
            final OAuthToken token = googleAnalyticsService.getOauthToken(request.getToken(), System.getenv("GA_API_REDIRECTION_URL"));
            final JsonObject userInfo = googleAnalyticsService.getUserInfo(token.getTokenValue());
            final User loggedUser = userTokenService.getUserAuthToken(securityContext.getUserPrincipal().getName()).getUser();
            /**
             * Use the GoogleAccountRequest object to store the attributes of the Google Account and then call the
             * addNewGoogleAccount method to store the information in the DB.
             */
            final GoogleAccountRequest googleAccountRequest = new GoogleAccountRequest();
            googleAccountRequest.setActive(true);
            googleAccountRequest.setEmail(userInfo.getString("email"));
            googleAccountRequest.setToken(token.getTokenValue());
            googleAccountRequest.setRefreshToken(token.getRefreshToken());
            googleAccountRequest.setTokenTimeToLive((int) token.getTimeToLive());
            googleAccountRequest.setTokenStartTime(System.currentTimeMillis() / 1000);
            googleAccountRequest.setUserId(loggedUser.getId());

            return addNewGoogleAccount(googleAccountRequest);
        } catch (IOException ioe) {
            throw new WebApplicationException(ioe.getMessage(), ioe);
        }
    }
}
