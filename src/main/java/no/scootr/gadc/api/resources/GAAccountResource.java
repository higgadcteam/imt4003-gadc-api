package no.scootr.gadc.api.resources;

import io.swagger.annotations.*;
import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.ErrorMessage;
import no.scootr.gadc.api.model.GAAccount;
import no.scootr.gadc.api.model.OAuthToken;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.requestbeans.GAAccountAndWebPropertyRequest;
import no.scootr.gadc.api.requestbeans.GAAccountRequest;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.security.filters.AuthorizationFilter;
import no.scootr.gadc.api.security.filters.GAAccountFilter;
import no.scootr.gadc.api.services.GAAccountService;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.List;

/**
 * Resource class that is used to add, edit, delete and list google analytics accounts stored in the DB.
 * https://developers.google.com/analytics/devguides/config/mgmt/v3/mgmtReference/#Account Summaries
 * Or should this be set to accounts instead, as it is a simplified version of the official API?
 */
@Path("management/accountSummaries")
@Api(value = "Google Analytics Accounts",
        description = "Manage Google Analytics accounts",
        authorizations = {@Authorization(OAuthToken.DEFAULT_AUTH_SCHEME)})
public class GAAccountResource {
    @Inject
    GAAccountService gaAccountService;
    @Inject
    UserTokenService userTokenService;
    @Context
    SecurityContext securityContext;

    /**
     * This method is used to get the list of all registered Google Analytics Accounts from the DB.
     * @return The list of all Google Analytics Accounts registered in the DB.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(AuthorizationFilter.class)
    @ApiOperation(value = "Get Google Analytics accounts", response = GAAccount.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Found the Google Analytics accounts", response = GAAccount.class, responseContainer = "List")})
    public List<GAAccount> getAllGAAccountList() {
        final User loggedUser = userTokenService.getUserAuthToken(securityContext.getUserPrincipal().getName()).getUser();
        return gaAccountService.getAllGAAccounts(loggedUser);
    }

    /**
     * Returns a single Google Analytics Object from the DB by specifying the ID.
     */
    @GET
    @Path("/{gAAccountID}")
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(GAAccountFilter.class)
    @ApiOperation(value = "Get Google Analytics account", response = GAAccount.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Found the Google Analytics account", response = GAAccount.class),
            @ApiResponse(code = 404, message = "Google Analytics account with the specified ID does not exist", response = ErrorMessage.class)})
    public Response getGAAccountByID(
            @ApiParam(value = "Google Analytics Account ID", required = true) @PathParam("gAAccountID") final long gAAccountID) {
        final GAAccount gAAccount = gaAccountService.getGAAccountByID(gAAccountID);

        if (gAAccount != null) {
            return ResponseFactory.create(gAAccount);
        } else {
            throw ResponseFactory.buildError("Google Analytics account not found", Response.Status.NOT_FOUND);
        }
    }

    /**
     * Adds a Google Analytics Account in the DB.
     */
    @POST
    @AccessConstraint(AuthorizationFilter.class)
    @ApiOperation(value = "Add Google Analytics Account", response = GAAccount.class)
    @ApiResponses({@ApiResponse(code = 201, message = "Google Analytics account was added", response = GAAccount.class)})
    public Response addGAAccount(
            @ApiParam(value = "Google Analytics account to add", required = true) final GAAccountRequest gaAccountRequest) {
        try {
            return ResponseFactory.create(Response.Status.CREATED, gaAccountService.addGAAccount(gaAccountRequest));
        } catch (CustomizedMessageException e) {
            throw new WebApplicationException(e.getMessage(), e);
        }
    }

    /**
     * Adds a Google Analytics Account together with its corresponding web properties (Profile).
     */
    @POST
    @Path("/addGAAWithWebProperties")
    @AccessConstraint(AuthorizationFilter.class)
    @ApiOperation(value = "Add Google Analytics account with web properties", response = GAAccount.class)
    @ApiResponses({@ApiResponse(code = 201, message = "Google Analytics account was added", response = GAAccount.class)})
    public Response addGAAccountWithWPs(
            @ApiParam(value = "Information about the Google Analytics Account and the Web Properties")
            final GAAccountAndWebPropertyRequest gaAccountAndWebPropertyRequest) {
        try {
            return ResponseFactory.create(Response.Status.CREATED, gaAccountService.addGAAccountWithWPs(gaAccountAndWebPropertyRequest));
        } catch (CustomizedMessageException e) {
            throw new WebApplicationException(e.getMessage(), e);
        }
    }

    /**
     * This method deletes an existing Google Analytics Account from the DB.
     * @param gAAccountID The ID of the google analytics account.
     * @return The deleted object.
     */
    @DELETE
    @Path("/{gAAccountID}")
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(GAAccountFilter.class)
    @ApiOperation(value = "Delete Google Analytics account", response = GAAccount.class, notes = "Does not delete the account from Google, only the data stored on records in this API.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Account removed", response = GAAccount.class),
            @ApiResponse(code = 404, message = "No Google Analytics account with this ID exists", response = ErrorMessage.class)})
    public Response deleteGAAccount(
            @ApiParam(value = "Google Analytics Account ID", required = true) @PathParam("gAAccountID") final long gAAccountID) {
        final GAAccount gAAccount = gaAccountService.getGAAccountByID(gAAccountID);

        if (gAAccount != null) {
            try {
                return ResponseFactory.create(gaAccountService.deleteGAAccount(gAAccount));
            } catch (CustomizedMessageException cse) {
                throw new WebApplicationException(cse.getMessage(), cse);
            }
        } else {
            throw ResponseFactory.buildError("Google Analytics Account was not found", Response.Status.NOT_FOUND);
        }
    }
}
