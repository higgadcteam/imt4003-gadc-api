package no.scootr.gadc.api.resources;

import io.swagger.annotations.*;
import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.Dimensions;
import no.scootr.gadc.api.model.ErrorMessage;
import no.scootr.gadc.api.model.OAuthToken;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.requestbeans.DimensionsRequest;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.security.filters.AuthorizationFilter;
import no.scootr.gadc.api.security.filters.DimensionFilter;
import no.scootr.gadc.api.services.DimensionsService;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.List;

/**
 * Resource that handles the requests for adding, modifying and deleting a Dimensions.
 */
@Path("management/dimensions")
@Api(value = "Dimensions", description = "Resource for adding, modifying and deleting dimensions",
        authorizations = {@Authorization(OAuthToken.DEFAULT_AUTH_SCHEME)})
public class DimensionResource {

    @Inject
    DimensionsService dimensionsService;
    @Inject
    UserTokenService userTokenService;
    @Context
    SecurityContext securityContext;

    /**
     * This method retrieves all the registered dimensions in the database and returns a list of the found entities.
     * @return The list of Metrics registered in the DB.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(AuthorizationFilter.class)
    @ApiOperation(value = "Get all dimensions", response = Dimensions.class, responseContainer = "List")
    public List<Dimensions> getAllAvailableDimensions() {
        final User loggedUser = userTokenService.getUserAuthToken(securityContext.getUserPrincipal().getName()).getUser();
        return dimensionsService.findAllUserDimension(loggedUser);
    }

    /**
     * This method returns a single Dimensions entity as a json response to the server. It takes the dimensions id as a
     * parameter and uses it to find the object in the DB.
     * @param dimensionID The ID of the dimension entity that will be accessed from the DB.
     * @return The metrics object as a json response entity.
     */
    @GET
    @Path("/{dimensionID}")
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(DimensionFilter.class)
    @ApiOperation(value = "Get dimensions", response = Dimensions.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "The dimension was found", response = Dimensions.class),
            @ApiResponse(code = 404, message = "No dimension exists with the provided ID", response = ErrorMessage.class)})
    public Response getSingleDimension(
            @ApiParam(value = "Dimension ID", required = true) @PathParam("dimensionID") final int dimensionID) {
        final Dimensions dimension = dimensionsService.findByID(dimensionID);

        if (dimension == null) {
            throw ResponseFactory.buildError("Dimension not found", Response.Status.NOT_FOUND);
        }

        return ResponseFactory.create(dimension);
    }

    /**
     * This method is used to register or update a dimension. It takes as parameter a DimensionRequest object which
     * holds the attributes and their corresponding values that are required to register a dimension in the DB.
     * @param dimensionsRequest The request object of the dimension entity that has the information needed to store
     *a dimension entity into the DB.
     * @return The registered or updated Dimension as a json response entity.
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @AccessConstraint(AuthorizationFilter.class)
    @ApiOperation(value = "Create dimension", response = Dimensions.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "A dimension already exists and was updated", response = Dimensions.class),
            @ApiResponse(code = 202, message = "A new dimension has been created", response = Dimensions.class),
            @ApiResponse(code = 400, message = "Empty dimension name or dimension value", response = ErrorMessage.class)})
    public Response addNewDimension(
            @ApiParam(value = "Dimension request", required = true) final DimensionsRequest dimensionsRequest) {

        final User loggedUser = userTokenService.getUserAuthToken(securityContext.getUserPrincipal().getName()).getUser();

        /**
         * Validate Dimension Attributes.
         */
        validateDimensionsAttributes(dimensionsRequest);

        /**
         * Register Dimension into the DB if it doesn't already exist.
         */
        try {
            Dimensions dimension = dimensionsService.findByDimensionValueAndOwner(dimensionsRequest.getDimensionValue().toLowerCase(), loggedUser);

            if (dimension != null && dimension.getOwner().getId() == loggedUser.getId()) {
                dimension.setDimensionName(dimensionsRequest.getDimensionName());
                dimension.setDimensionValue(dimensionsRequest.getDimensionValue().toLowerCase());
                dimension.setStatus(dimensionsRequest.isStatus());
                dimensionsService.edit(dimension);

                return Response.status(Response.Status.OK)
                        .entity(dimension)
                        .build();
            } else {
                dimension = new Dimensions(dimensionsRequest.getDimensionName(), dimensionsRequest.getDimensionValue().toLowerCase(), dimensionsRequest.isStatus());
                dimension.setOwner(loggedUser);
                dimensionsService.add(dimension);
                return Response.status(Response.Status.CREATED)
                        .entity(dimension)
                        .build();
            }
        } catch (CustomizedMessageException cme) {
            throw new WebApplicationException(cme.getMessage(), cme);
        }
    }

    /**
     * This method will validate the dimensions attribute coming from the request.
     * @param dimensionsRequest the dimension request object that holds the information for a dimension entity.
     */
    private void validateDimensionsAttributes(@ApiParam(value = "Dimension request", required = true) DimensionsRequest dimensionsRequest) {
        if (dimensionsRequest.getDimensionName() == null || "".equals(dimensionsRequest.getDimensionName())) {
            throw ResponseFactory.buildError("Dimension Name cannot be empty", Response.Status.BAD_REQUEST);
        }

        if (dimensionsRequest.getDimensionValue() == null || "".equals(dimensionsRequest.getDimensionValue())) {
            throw ResponseFactory.buildError("Dimension value is required and cannot be empty", Response.Status.BAD_REQUEST);
        }
    }


    /**
     * This method deletes a dimension from the DB. It takes as parameter the metrics ID for which it uses to find the
     * dimension object and then delete it.
     * @param dimensionID The ID of the dimension entity that has to be deleted from the database.
     * @return THe deleted Metrics as a json response entity.
     */
    @DELETE
    @Path("/{dimensionID}")
    @AccessConstraint(DimensionFilter.class)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Delete dimension", response = Dimensions.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Dimension removed", response = Dimensions.class)})
    public Response deleteDimension(
            @ApiParam(value = "Dimension ID", required = true) @PathParam("dimensionID") final int dimensionID) {
        Dimensions dimension = dimensionsService.findByID(dimensionID);

        if (dimension == null) {
            throw ResponseFactory.buildError("Dimension not found", Response.Status.NOT_FOUND);
        }

        try {
            return ResponseFactory.create(dimensionsService.deleteDimension(dimension));
        } catch (CustomizedMessageException cme) {
            throw new WebApplicationException(cme.getMessage(), cme);
        }
    }
}
