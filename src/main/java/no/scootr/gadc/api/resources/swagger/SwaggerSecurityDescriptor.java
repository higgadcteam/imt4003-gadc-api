package no.scootr.gadc.api.resources.swagger;

import io.swagger.annotations.*;
import io.swagger.jaxrs.Reader;
import io.swagger.jaxrs.config.ReaderListener;
import io.swagger.models.Swagger;
import io.swagger.models.auth.OAuth2Definition;
import no.scootr.gadc.api.model.OAuthToken;

/**
 * Describes what security permissions this API can use.
 */
@SwaggerDefinition
public class SwaggerSecurityDescriptor implements ReaderListener {

    @Override
    public void beforeScan(Reader reader, Swagger swagger) {
        /**
         * This method is empty because is not used, and it has to be implemented from the interface ReaderLister.
         */
    }

    @Override
    public void afterScan(Reader reader, Swagger swagger) {
        final OAuth2Definition tokenScheme = new OAuth2Definition();
        tokenScheme.setFlow("implicit");
        tokenScheme.setTokenUrl("https://" + swagger.getHost() + "/v1/users/token");

        swagger.addSecurityDefinition(OAuthToken.DEFAULT_AUTH_SCHEME, tokenScheme);
    }
}
