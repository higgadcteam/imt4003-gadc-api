package no.scootr.gadc.api;

import io.swagger.jaxrs.config.BeanConfig;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Application configuration class for the API.
 * Also specifies the root path for each resource.
 */
@ApplicationPath("v1")
public class JAXRSConfiguration extends Application {
    public JAXRSConfiguration() {
        /**
         * GADC-110:
         * Temporarily fix for a security vulnerability involving the Apache Commons Library (ACL).
         * Swagger is the only dependency that requires ACL, this code can be removed once ACL and
         * Swagger publishes an update that mitigates this issue.
         *
         * Learn more about this vulnerability at https://www.kb.cert.org/vuls/id/576313
         */
        System.setProperty("org.apache.commons.collections.enableUnsafeSerialization", "false");

        final BeanConfig config = new BeanConfig();
        config.setTitle("GADC API");
        config.setVersion("0.0.1");
        config.setTitle("Google Analytics Data Acquisition API");
        config.setSchemes(new String[]{"http, https"});
        config.setHost("localhost:8000");
        config.setBasePath("/v1");
        config.setResourcePackage("no.scootr.gadc.api.resources");
        config.setScan(true);
    }
}
