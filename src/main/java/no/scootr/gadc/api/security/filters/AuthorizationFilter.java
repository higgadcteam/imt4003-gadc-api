package no.scootr.gadc.api.security.filters;

import no.scootr.gadc.api.model.UserAuthToken;
import no.scootr.gadc.api.security.AuthSecurityContext;
import no.scootr.gadc.api.security.RestrictionFilter;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Response;

/**
 * Filter requiring requests to have specified the 'secret' string in the Authorization header.
 */
public class AuthorizationFilter implements RestrictionFilter {

    @Inject
    private UserTokenService userTokenService;

    /**
     * Checks if the user provides a valid auth token.
     * @param containerRequestContext Request context.
     * @return {@code true} if valid, otherwise, {@code false}.
     */
    @Override
    public boolean authorize(ContainerRequestContext containerRequestContext) {
        final String authorizationHeader = containerRequestContext.getHeaderString("Authorization");

        final UserAuthToken userAuthToken = userTokenService.getUserAuthToken(authorizationHeader);
        if (userAuthToken == null) {
            return false;
        } else {
            if (userAuthToken.isExpired()) {
                throw ResponseFactory.buildError("403 Forbidden! Authorization Token has expired.", Response.Status.FORBIDDEN);
            }

            containerRequestContext.setSecurityContext(new AuthSecurityContext(authorizationHeader));
            return true;
        }
    }
}
