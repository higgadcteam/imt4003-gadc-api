package no.scootr.gadc.api.security.annotations;

import no.scootr.gadc.api.security.RestrictionFilter;

import javax.enterprise.util.Nonbinding;
import javax.interceptor.InterceptorBinding;
import java.lang.annotation.*;

/**
 * Indicates that a class or a method should be filtered using a specified {@code RestrictionFilter}
 * before any call to the method (or the class) is made.
 */
@Inherited
@InterceptorBinding
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface AccessConstraint {
    @Nonbinding
    Class<? extends RestrictionFilter>[] value();
}