package no.scootr.gadc.api.security.filters;

import no.scootr.gadc.api.model.GoogleAccount;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.security.RestrictionFilter;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.services.GoogleAccountService;
import no.scootr.gadc.api.services.UserService;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * Filter that checks if the Google account entity being accessed by the logged user has the rights to access it.
 * Created by Brikendi on 11/17/2015.
 */
@AccessConstraint(AuthorizationFilter.class)
public class GoogleAccountFilter implements RestrictionFilter {
    @Inject
    private UserTokenService userTokenService;
    @Inject
    private UserService userService;
    @Inject
    private GoogleAccountService googleAccountService;


    /**
     * Authorizes the access of the logged user to the google account entity.
     * @param containerRequestContext Request context.
     * @return a boolean value which is true when the user is associated with the google account. False if otherwise.
     */
    @Override
    public boolean authorize(ContainerRequestContext containerRequestContext) {
        final User loggedUser = userTokenService.getUserAuthToken(containerRequestContext.getSecurityContext().getUserPrincipal().getName()).getUser();
        final MultivaluedMap<String, String> maps = containerRequestContext.getUriInfo().getPathParameters();

        final int googleAccountID = Integer.valueOf(maps.getFirst("googleAccountID"));
        final GoogleAccount googleAccount = googleAccountService.getGoogleAccount(googleAccountID);

        if (googleAccount == null) {
            throw ResponseFactory.buildError("Google Account with ID: " + googleAccountID + " was not found in the system", Response.Status.NOT_FOUND);
        }

        return userService.checkRelationBetweenUserAndGoogleAccount(loggedUser, googleAccount);
    }
}
