package no.scootr.gadc.api.security.filters;

import no.scootr.gadc.api.model.GAAccount;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.security.RestrictionFilter;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.services.GAAccountService;
import no.scootr.gadc.api.services.UserService;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * Filter that checks if the google analytics account being accessed by the logged user has the the rights to access it.
 * Created by Brikendi on 11/17/2015.
 */
@AccessConstraint(AuthorizationFilter.class)
public class GAAccountFilter implements RestrictionFilter {
    private static final String[] GA_ACCOUNT_ID_DEFINITIONS = {"gAAccountID", "accountId", "googleAnalyticsAccountID"};

    @Inject
    private GAAccountService gaAccountService;
    @Inject
    private UserTokenService userTokenService;
    @Inject
    private UserService userService;

    /**
     * Authorizes the access of the logged user to the google analytics account entity.
     * @param containerRequestContext Request context.
     * @return a boolean value which is true when the user is associated with the google analytics account. False if otherwise.
     */
    @Override
    public boolean authorize(ContainerRequestContext containerRequestContext) {
        final User loggedUser = userTokenService.getUserAuthToken(containerRequestContext.getSecurityContext().getUserPrincipal().getName()).getUser();
        final MultivaluedMap<String, String> maps = containerRequestContext.getUriInfo().getPathParameters();

        long gAAccountID = -1;
        for (String gaAccountIdDef : GA_ACCOUNT_ID_DEFINITIONS) {
            if (maps.getFirst(gaAccountIdDef) != null && !"".equals(maps.getFirst(gaAccountIdDef))) {
                gAAccountID = Long.valueOf(maps.getFirst(gaAccountIdDef));
            }
        }

        final GAAccount gaAccount = gaAccountService.getGAAccountByID(gAAccountID);

        if (gaAccount == null || gaAccount.getOwner() == null) {
            throw ResponseFactory.buildError("Google Analytics Account with ID: " + gAAccountID + " was not found in the system", Response.Status.NOT_FOUND);
        }

        return userService.checkRelationBetweenUserAndGoogleAccount(loggedUser, gaAccount.getOwner());
    }
}
