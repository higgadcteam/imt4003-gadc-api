package no.scootr.gadc.api.security;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Hooks up the resources and the filters.
 */
@Provider
public class SecurityConstraintConfigurator implements DynamicFeature {
    @Inject
    private RestrictionFilterFactory restrictionFilterFactory;

    /**
     * Attempts to find any filters specified for a resource or a method. If a filter exists, register the filter
     * and add it to the context.
     * @param resourceInfo Information about the resource.
     * @param context Context.
     */
    @Override
    public void configure(ResourceInfo resourceInfo, FeatureContext context) {
        final Class<?> clazz = resourceInfo.getResourceClass();
        final Method method = resourceInfo.getResourceMethod();

        final List<Instance<? extends RestrictionFilter>> filters = restrictionFilterFactory.getRestrictionFilters(clazz, method);
        context.register(new SecurityAccessVerifier(filters, restrictionFilterFactory));
    }
}
