package no.scootr.gadc.api.security;

import no.scootr.gadc.api.security.annotations.AccessConstraint;

import javax.annotation.Nullable;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.CDI;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Instantiates filters used to verify access rights to resources.
 */
public class RestrictionFilterFactory {
    private RestrictionFilterFactory() {
    }

    /**
     * Creates a filter (parent filter) that a filter has been annotated with.
     * @param filter Filter to get parent filter for.
     * @return The filters that a filter has been annotated with.
     */
    public List<Instance<? extends RestrictionFilter>> getParentRestrictionFilter(final RestrictionFilter filter) {
        return getFilter(filter.getClass().getAnnotation(AccessConstraint.class));
    }

    /**
     * Creates a filter that a resource or a method is annotated with.
     * @param clazz Class to get filter for.
     * @param method Method to get filter for.
     * @return The filters specified by the resource, unless a filter is specified on the method.
     */
    public List<Instance<? extends RestrictionFilter>> getRestrictionFilters(final Class<?> clazz, final Method method) {
        return getFilter(getAnnotation(clazz, method));
    }

    private static AccessConstraint getAnnotation(final Class<?> clazz, final Method method) {
        if (method.isAnnotationPresent(AccessConstraint.class)) {
            return method.getAnnotation(AccessConstraint.class);
        } else if (clazz.isAnnotationPresent(AccessConstraint.class)) {
            return clazz.getAnnotation(AccessConstraint.class);
        }

        return null;
    }

    private List<Instance<? extends RestrictionFilter>> getFilter(@Nullable final AccessConstraint accessConstraint) {
        if (accessConstraint != null) {
            final List<Instance<? extends RestrictionFilter>> filters = new ArrayList<>(accessConstraint.value().length);

            for (Class<? extends RestrictionFilter> filter : accessConstraint.value()) {
                filters.add(CDI.current().select(filter));
            }

            return filters;
        } else {
            return new ArrayList<>(0);
        }
    }
}
