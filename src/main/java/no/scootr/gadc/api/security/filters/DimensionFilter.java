package no.scootr.gadc.api.security.filters;

import no.scootr.gadc.api.model.Dimensions;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.security.RestrictionFilter;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.services.DimensionsService;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * Filter that checks if the Dimension being accessed by the logged user has the the rights to access it.
 * Created by Brikendi on 11/23/2015.
 */
@AccessConstraint(AuthorizationFilter.class)
public class DimensionFilter implements RestrictionFilter {
    @Inject
    private UserTokenService userTokenService;
    @Inject
    private DimensionsService dimensionsService;

    /**
     * Authorizes the access of the logged user to the Dimension entity.
     * @param containerRequestContext Request context.
     * @return a boolean value which is true when the user is associated with the dimension. False if otherwise.
     */
    @Override
    public boolean authorize(ContainerRequestContext containerRequestContext) {
        final User loggedUser = userTokenService.getUserAuthToken(containerRequestContext.getSecurityContext().getUserPrincipal().getName()).getUser();
        final MultivaluedMap<String, String> maps = containerRequestContext.getUriInfo().getPathParameters();

        final int dimensionID = Integer.valueOf(maps.getFirst("dimensionID"));
        final Dimensions dimensions = dimensionsService.findByID(dimensionID);

        if (dimensions == null) {
            throw ResponseFactory.buildError("Dimension with ID: " + dimensionID + " was not found in the system", Response.Status.NOT_FOUND);
        }

        if (dimensions.getOwner() == null) {
            return false;
        }

        return dimensions.getOwner().getId() == loggedUser.getId();
    }
}
