package no.scootr.gadc.api.security;

import javax.ws.rs.container.ContainerRequestContext;

/**
 * Filter for checking rights towards a resource or a resource method.
 */
public interface RestrictionFilter {

    /**
     * Checks if the request has sufficient rights to access a resource.
     * @param containerRequestContext Request context.
     * @return {@code true} if the call has access to the resource, otherwise {@code false}.
     */
    boolean authorize(ContainerRequestContext containerRequestContext);
}
