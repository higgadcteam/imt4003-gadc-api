package no.scootr.gadc.api.security;

import javax.ws.rs.core.SecurityContext;
import java.security.Principal;

/**
 * Security context wrapping the current auth token that the user uses to access the system through a {@code Principal}.
 */
public class AuthSecurityContext implements SecurityContext {
    private Principal userPrincipal;

    public AuthSecurityContext(final String authToken) {
        this.userPrincipal = () -> authToken;
    }

    @Override
    public Principal getUserPrincipal() {
        return this.userPrincipal;
    }

    @Override
    public boolean isUserInRole(String role) {
        return false;
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public String getAuthenticationScheme() {
        return null;
    }
}
