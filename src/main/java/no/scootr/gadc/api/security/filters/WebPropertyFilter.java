package no.scootr.gadc.api.security.filters;

import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.security.RestrictionFilter;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.services.UserService;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.services.WebPropertyService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * Filter that checks if the web property being accessed by the logged user has the the rights to access it.
 * Created by Brikendi on 11/24/2015.
 */
@AccessConstraint(AuthorizationFilter.class)
public class WebPropertyFilter implements RestrictionFilter {

    @Inject
    private UserTokenService userTokenService;
    @Inject
    private UserService userService;
    @Inject
    private WebPropertyService webPropertyService;


    /**
     * Authorizes the access of the logged user to the web property entity.
     * @param containerRequestContext Request context.
     * @return a boolean value which is true when the user is associated with the google account of the google analytics account of the web property. False if otherwise.
     */
    @Override
    public boolean authorize(ContainerRequestContext containerRequestContext) {
        final User loggedUser = userTokenService.getUserAuthToken(containerRequestContext.getSecurityContext().getUserPrincipal().getName()).getUser();
        final MultivaluedMap<String, String> maps = containerRequestContext.getUriInfo().getPathParameters();

        final String webPropertyID = maps.getFirst("webPropertyID");
        final WebProperty webProperty = webPropertyService.getWebPropertyById(webPropertyID);

        if (webProperty == null) {
            throw ResponseFactory.buildError("Web Property with ID: " + webPropertyID + " was not found in the system", Response.Status.NOT_FOUND);
        }

        if (webProperty.getGaAccount() != null
                && webProperty.getGaAccount().getOwner() != null
                && userService.checkRelationBetweenUserAndGoogleAccount(loggedUser, webProperty.getGaAccount().getOwner())) {
            return true;
        }

        return false;
    }
}
