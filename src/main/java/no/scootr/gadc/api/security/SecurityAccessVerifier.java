package no.scootr.gadc.api.security;

import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.enterprise.inject.Instance;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.logging.Filter;

/**
 * A request filter that will execute all the filters associated with a method or a class on requests.
 */
public class SecurityAccessVerifier implements ContainerRequestFilter {
    private final List<Instance<? extends RestrictionFilter>> restrictionFilter;
    private final RestrictionFilterFactory restrictionFilterFactory;

    public SecurityAccessVerifier(final List<Instance<? extends RestrictionFilter>> restrictionFilter,
                                  final RestrictionFilterFactory restrictionFilterFactory) {
        this.restrictionFilter = restrictionFilter;
        this.restrictionFilterFactory = restrictionFilterFactory;
    }

    /**
     * Validates the call by going through the RestrictionFilters associated with the resource.
     * @param requestContext Request context.
     * @throws IOException Should not happen.
     * @throws WebApplicationException User does not have rights to perform this call.
     */
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        authorizeRequest(requestContext, this.restrictionFilter);
    }

    private void authorizeRequest(final ContainerRequestContext requestContext, final List<Instance<? extends RestrictionFilter>> filters) {
        for (Instance<? extends RestrictionFilter> filter : filters) {

            final List<Instance<? extends RestrictionFilter>> parentFilters = restrictionFilterFactory.getParentRestrictionFilter(filter.get());

            authorizeRequest(requestContext, parentFilters);

            if (!filter.get().authorize(requestContext)) {
                throw ResponseFactory.buildError("403 Forbidden! Seems that you do not have permission to access this resource.", Response.Status.FORBIDDEN);
            }
        }
    }
}
