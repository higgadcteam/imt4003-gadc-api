package no.scootr.gadc.api.security.filters;

import no.scootr.gadc.api.model.Category;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.security.RestrictionFilter;
import no.scootr.gadc.api.security.annotations.AccessConstraint;
import no.scootr.gadc.api.services.CategoryService;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.utillity.ResponseFactory;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * Filter that checks if the category being accessed by the logged user has the the rights to access it.
 * Created by Brikendi on 11/22/2015.
 */
@AccessConstraint(AuthorizationFilter.class)
public class CategoryFilter implements RestrictionFilter {

    @Inject
    private UserTokenService userTokenService;
    @Inject
    private CategoryService categoryService;

    /**
     * Authorizes the access of the logged user to the category entity.
     * @param containerRequestContext Request context.
     * @return a boolean value which is true when the user is associated with the category. False if otherwise.
     */
    @Override
    public boolean authorize(ContainerRequestContext containerRequestContext) {
        final User loggedUser = userTokenService.getUserAuthToken(containerRequestContext.getSecurityContext().getUserPrincipal().getName()).getUser();
        final MultivaluedMap<String, String> maps = containerRequestContext.getUriInfo().getPathParameters();

        final int categoryID = Integer.valueOf(maps.getFirst("categoryID"));
        final Category category = categoryService.getCategoryById(categoryID);

        if (category == null) {
            throw ResponseFactory.buildError("Category with ID: " + categoryID + " was not found in the system", Response.Status.NOT_FOUND);
        }

        if (category.getOwner() == null) {
            return false;
        }

        return category.getOwner().getId() == loggedUser.getId();
    }
}
