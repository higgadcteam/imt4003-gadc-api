package no.scootr.gadc.api.google;

import io.swagger.annotations.ApiParam;
import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.GAAccount;
import no.scootr.gadc.api.model.GoogleAccount;
import no.scootr.gadc.api.services.AccessTokenService;
import no.scootr.gadc.api.services.GoogleAccountService;
import no.scootr.gadc.api.utillity.ApiCall;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import java.io.IOException;

@Stateless
public class GoogleWebPropertyService {
    @Inject
    private GoogleAccountService gaService;
    @Inject
    private AccessTokenService tokenService;
    @Inject
    private GoogleAccountService googleAccountService;

    /**
     * This method will initiate an API Call to Google Analytics Core Reporting API to retrieve all the profiles
     * that are associated with the given web property of a specified Google Analytics Account.
     * @param webPropertyID ID of the web property entity.
     * @param gaAccount Google Analytics account entity which is used to retrieve the google account to check if
     * the authorization token has expired or not.
     * @return A json object containing the list of all the retrieved profiles of the web property.
     * @throws CustomizedMessageException
     * @throws IOException
     */
    public JsonObject apiCallGetViewsOfWebProperty(@ApiParam(value = "Web Property ID", required = true) @PathParam("webPropertyID") String webPropertyID, GAAccount gaAccount) throws CustomizedMessageException {
        final GoogleAccount googleAccount = gaService.getGoogleAccount(gaAccount.getOwner().getId());
        try {
            if ((System.currentTimeMillis() / 1000) > googleAccount.getOAuthToken().getExpirationTime()) {
                tokenService.refreshAccessToken(googleAccount);
            }

            /**
             * Get all the Google account from the google management api by performing a GET Request and providing
             * the Authorization token as a header parameter.
             */
            final ApiCall call = new ApiCall(String.format("https://www.googleapis.com/analytics/v3/management/accounts/%s/webproperties/%s/profiles", gaAccount.getId(), webPropertyID), "GET");
            call.setHeader("Authorization", String.format("Bearer %s", googleAccount.getOAuthToken().getTokenValue()));
            return call.readJsonResponse();
        } catch (IOException ioe) {
            handleIOExceptionOfApiCall(googleAccount, ioe);
        } catch (CustomizedMessageException cme) {
            throw new WebApplicationException(cme);
        }
        return null;
    }

    /**
     * This method will perform the API call for getting the Web Properties from the Google Core Reporting API, and
     * return a JSONObject with the list of all web properties retrieved.
     * @param gaAccount Google Analytics Account.
     * @param googleAnalyticsAccountID ID of the Google Analytics Account.
     * @return The list of web properties associated to the given google analytics account entity formatted as a json object.
     */
    public JsonObject apiCallGetWebProperties(final GAAccount gaAccount, final long googleAnalyticsAccountID) throws CustomizedMessageException {
        final GoogleAccount googleAccount = gaService.getGoogleAccount(gaAccount.getOwner().getId());

        try {
            /**
             * Check if token has expired.
             */
            if ((System.currentTimeMillis() / 1000) > googleAccount.getOAuthToken().getExpirationTime()) {
                tokenService.refreshAccessToken(googleAccount);
            }


            /**
             * Get all the google account from the Google management API by performing a GET Request and providing
             * the Authorization token as a header parameter.
             */
            final ApiCall call = new ApiCall(String.format("https://www.googleapis.com/analytics/v3/management/accounts/%s/webproperties", googleAnalyticsAccountID), "GET");
            call.setHeader("Authorization", String.format("Bearer %s", googleAccount.getOAuthToken().getTokenValue()));
            return call.readJsonResponse();

        } catch (IOException ioe) {
            handleIOExceptionOfApiCall(googleAccount, ioe);
        } catch (CustomizedMessageException cme) {
            throw new WebApplicationException(cme);
        }
        return null;
    }

    /**
     * This method has been refactored to handle an IOException if returned from the Google API.
     * @param googleAccount Google account to be updated by refreshing the access token or disabling the entity.
     * @param ioe The original cause of the call.
     */
    private void handleIOExceptionOfApiCall(GoogleAccount googleAccount, IOException ioe) throws CustomizedMessageException {
        if (ioe.getMessage().contains("HTTP response code: 401")) {
            tokenService.refreshAccessToken(googleAccount);
            throw new CustomizedMessageException("Authorization token had been expired or the scope was wrong. Please re-synchronize this Google Account.");
        } else if (ioe.getMessage().contains("HTTP response code: 403")) {
            googleAccountService.changeGoogleAccountStatus(googleAccount, false);
            throw new CustomizedMessageException("This google account does not have rights to access any google analytics accounts and therefore it will be deactivated for the moment.");
        } else {
            throw new CustomizedMessageException("Google Analytics Service is unavailable!");
        }

    }
}
