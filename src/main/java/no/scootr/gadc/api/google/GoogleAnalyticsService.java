package no.scootr.gadc.api.google;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.GoogleAccount;
import no.scootr.gadc.api.model.Profile;
import no.scootr.gadc.api.services.AccessTokenService;
import no.scootr.gadc.api.services.GoogleAccountService;
import no.scootr.gadc.api.utillity.ApiCall;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.WebApplicationException;
import java.io.IOException;

/**
 * This service is used to make api calls to Google for retrieving the google analytics accounts for a specific
 * google account.
 */
@Stateless
public class GoogleAnalyticsService {
    private static final String DATA_API_RESOURCE_PATH = "https://www.googleapis.com/analytics/v3/data/ga?";

    @Inject
    AccessTokenService tokenService;
    @Inject
    GoogleAccountService googleAccountService;


    /**
     * This method will perform the API call for getting the Google Analytics Accounts from the Google Core Reporting API, and
     * return a JSONObject with the list of all GA accounts retrieved.
     * @param googleAccount Google Account entity that is used to check the authorization token if it has expired or not.
     * @return The json object that contains the security information that will be updated for the google account.
     */
    public JsonObject apiCallGetGAAccounts(final GoogleAccount googleAccount) throws CustomizedMessageException {
        try {

            /**
             * Check if token has expired.
             */
            if ((System.currentTimeMillis() / 1000) > googleAccount.getOAuthToken().getExpirationTime()) {
                tokenService.refreshAccessToken(googleAccount);
            }

            /**
             * Get all the Google accounts from the google management API by performing a GET Request and providing
             * the Authorization token as a header parameter.
             */
            final ApiCall call = new ApiCall("https://www.googleapis.com/analytics/v3/management/accounts", "GET");
            call.setContentType("application/json");
            call.setHeader("Authorization", String.format("Bearer %s", googleAccount.getOAuthToken().getTokenValue()));
            return call.readJsonResponse();
        } catch (IOException ioe) {
            handleIOExceptionOfApiCall(googleAccount, ioe);
        } catch (CustomizedMessageException cme) {
            throw new WebApplicationException(cme);
        }
        return null;
    }

    /**
     * This method has been refactored to handle an IOException if returned from the Google API.
     * @param googleAccount Google account to be updated by refreshing the access token or disabling the entity.
     * @param ioe The original cause of the call.
     */
    private void handleIOExceptionOfApiCall(GoogleAccount googleAccount, IOException ioe) throws CustomizedMessageException {
        if (ioe.getMessage().contains("HTTP response code: 401")) {
            tokenService.refreshAccessToken(googleAccount);
            throw new CustomizedMessageException(ioe, "Authorization token had been expired or the scope was wrong. Please re-synchronize this Google Account.");
        } else if (ioe.getMessage().contains("HTTP response code: 403")) {
            googleAccountService.changeGoogleAccountStatus(googleAccount, false);
            throw new CustomizedMessageException(ioe, "This google account does not have rights to access any google analytics accounts and therefore it will be deactivated for the moment.");
        } else {
            throw new CustomizedMessageException(ioe, "Google Analytics Service is unavailable!");
        }

    }

    public JsonObject getAnalyticsData(final String query, final Profile profile) throws CustomizedMessageException {
        try {
            final GoogleAccount owner = profile.getWebProperty().getGaAccount().getOwner();


            if ((System.currentTimeMillis() / 1000) > owner.getOAuthToken().getExpirationTime()) {
                tokenService.refreshAccessToken(owner);
            }

            final ApiCall call = new ApiCall(DATA_API_RESOURCE_PATH + query, "GET");
            call.setHeader("Authorization", String.format("Bearer %s", owner.getOAuthToken().getTokenValue()));

            return call.readJsonResponse();
        } catch (IOException ex) {
            throw new CustomizedMessageException(ex, ex.getMessage());
        }
    }
}
