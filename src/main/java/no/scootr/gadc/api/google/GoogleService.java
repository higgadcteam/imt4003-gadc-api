package no.scootr.gadc.api.google;

import no.scootr.gadc.api.model.OAuthToken;
import no.scootr.gadc.api.utillity.ApiCall;

import javax.ejb.Stateless;
import javax.json.JsonObject;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * This class is used to get the OAuth2 Authentication token by providing the access token of the google account.
 */
@Stateless
public class GoogleService {
    /**
     * Attributes (Stored in the Environments Variables) that are necessary to get a Authentication token from Google API.
     */
    private static final String CLIENT_SECRET = System.getenv("GA_API_SECRET");
    private static final String CLIENT_ID = System.getenv("GA_API_CLIENT_ID");
    private static final String DEFAULT_ENCODING = "UTF-8";

    /**
     * This method makes a API call to the Google API to get the authentication token, by providing the access token and the
     * attributes defined above as body parameters.
     * @param accessToken Access token that is used to retrieve the authorization bearer from Google API.
     * @return The OAuthToken object that contains the security information for the google account.
     * @throws IOException
     */
    public OAuthToken getOauthToken(final String accessToken, final String redirectionUrl) throws IOException {
        final String body = String.format(
                "code=%s&client_id=%s&client_secret=%s&redirect_uri=%s&grant_type=%s&scope=",
                URLEncoder.encode(accessToken, DEFAULT_ENCODING),
                URLEncoder.encode(CLIENT_ID, DEFAULT_ENCODING),
                URLEncoder.encode(CLIENT_SECRET, DEFAULT_ENCODING),
                URLEncoder.encode(redirectionUrl, DEFAULT_ENCODING),
                URLEncoder.encode("authorization_code", DEFAULT_ENCODING));

        final ApiCall call = new ApiCall("https://www.googleapis.com/oauth2/v3/token", "POST");
        call.setContentType("application/x-www-form-urlencoded");
        call.writeString(body);

        final JsonObject o = call.readJsonResponse();
        return new OAuthToken(o.getString("token_type"),
                o.getString("access_token"),
                o.getString("refresh_token"),
                o.getInt("expires_in"));
    }

    /**
     * Create another ApiCall and retrieve the user information using the access token from the previous API call.
     * Store the information in a Json object.
     *
     * @param accessToken Access token string that is used to retrieve the Google user information.
     * @return The Google Account user information received from the google API as a json object.
     * @throws IOException
     */
    public JsonObject getUserInfo(final String accessToken) throws IOException {
        final ApiCall userInfoCall = new ApiCall("https://www.googleapis.com/oauth2/v2/userinfo", "GET");
        userInfoCall.setHeader("Authorization", String.format("Bearer %s", accessToken));

        return userInfoCall.readJsonResponse();
    }
}
