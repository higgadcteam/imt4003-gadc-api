package no.scootr.gadc.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Profile are properties of a web property which can be specific applied filters, raw etc.
 * Created by Brikendi on 11/24/2015.
 */
@Entity
@XmlRootElement
@ApiModel(value = "Profile", description = "Profile are properties of a web property which can be specific applied filters, raw etc.")
public class Profile {

    @Id
    @XmlElement
    @ApiModelProperty("Unique ID of the profile")
    private long id;

    @XmlElement
    @ApiModelProperty(value = "Name of the profile", example = "Filtered")
    private String profileName;

    @ManyToOne
    @ApiModelProperty("The web property that the profile belongs to")
    private WebProperty webProperty;

    public Profile() {
        
    }

    public Profile(long id, String profileName) {
        this.id = id;
        this.profileName = profileName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    @XmlTransient
    @JsonIgnore
    public WebProperty getWebProperty() {
        return webProperty;
    }

    public void setWebProperty(WebProperty webProperty) {
        this.webProperty = webProperty;
    }
}
