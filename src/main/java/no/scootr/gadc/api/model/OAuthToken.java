package no.scootr.gadc.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Defines an OpenAuth tokenValue used for accessing content from Google.
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class OAuthToken {
    public static final String BEARER = "Bearer";
    public static final String DEFAULT_AUTH_SCHEME = BEARER;

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @XmlTransient
    @JsonIgnore
    private int id;

    @XmlElement
    @ApiModelProperty(value = "The token type", example = "Bearer")
    private String tokenType;

    @XmlTransient
    private String tokenValue;

    @XmlTransient
    private String refreshToken;

    @XmlElement
    @ApiModelProperty("How long the token is valid in seconds")
    private long timeToLive;

    public OAuthToken() {
    }

    public OAuthToken(String tokenType, String token, String refreshToken, long timeToLive) {
        this.tokenType = tokenType;
        this.tokenValue = token;
        this.refreshToken = refreshToken;
        this.timeToLive = timeToLive;
    }

    @XmlTransient
    public String getTokenType() {
        return tokenType;
    }

    @XmlTransient
    public String getTokenValue() {
        return tokenValue;
    }

    @XmlTransient
    public long getTimeToLive() {
        return timeToLive;
    }

    @XmlTransient
    public String getRefreshToken() {
        return refreshToken;
    }

    @XmlTransient
    public int getId() {
        return id;
    }

    @XmlTransient
    public static String getBEARER() {
        return BEARER;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public void setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public void setTimeToLive(long timeToLive) {
        this.timeToLive = timeToLive;
    }
}
