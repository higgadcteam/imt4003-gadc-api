package no.scootr.gadc.api.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Error message is used when throwing Application Specific Exception, by providing the error message and error code
 * to the front end. I is usually send back to the frontend as a Json Response entity.
 */
@XmlRootElement
@ApiModel(value = "Error message", description = "Contains information about an error that occurred for a request.")
public class ErrorMessage {
    @ApiModelProperty("A detailed message describing what went wrong")
    private String errorMessageDescription;

    public ErrorMessage() {
    }

    public ErrorMessage(String errorMsg) {
        this.errorMessageDescription = errorMsg;
    }

    public String getErrorMessageDescription() {
        return errorMessageDescription;
    }

    public void setErrorMessageDescription(String errorMessageDescription) {
        this.errorMessageDescription = errorMessageDescription;
    }
}
