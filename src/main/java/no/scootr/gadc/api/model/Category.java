package no.scootr.gadc.api.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.LinkedList;
import java.util.List;

/**
 * Categories are used to categorize GA-Accounts.
 */
@Entity
@XmlRootElement
@ApiModel(value = "Category", description = "A registered category in the system.")
public class Category {

    @Id
    @GeneratedValue
    @XmlElement
    @ApiModelProperty("Unique ID of the category")
    private int id;

    @Column(unique = true)
    @XmlElement
    @ApiModelProperty("Name of the category")
    private String name;

    @ManyToOne
    @ApiModelProperty("The user that has added this Category")
    private User owner;

    @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    private List<WebProperty> webProperties;

    public Category() {
        this.webProperties = new LinkedList<>();
    }

    public Category(String name) {
        this();
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public List<WebProperty> getWebProperties() {
        return webProperties;
    }

    public void setWebProperties(List<WebProperty> webProperties) {
        this.webProperties = webProperties;
    }

    @XmlTransient
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
