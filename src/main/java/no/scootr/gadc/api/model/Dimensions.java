package no.scootr.gadc.api.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Dimensions are used to specify the dimension of the metrics in order to get the desired data from the
 * core reporting api (A dimension can be BrowserType, OS etc).
 */
@Entity
@XmlRootElement
@ApiModel(value = "Dimension", description = "Dimensions are used to specify the dimension of the metrics in order to " +
        "get the desired data from the core reporting api (A dimension can be BrowserType, OS etc)")
public class Dimensions {

    @Id
    @GeneratedValue
    @XmlElement
    @ApiModelProperty("Unique ID of the dimension")
    private int id;

    @XmlElement
    @ApiModelProperty("Name of the dimension")
    private String dimensionName;

    @XmlElement
    //@Column(unique = true)
    @ApiModelProperty("Value of the dimension")
    private String dimensionValue;

    @XmlElement
    @ApiModelProperty("Status of the dimension, indicating if it is active or not.")
    private boolean status;

    @ManyToOne
    @ApiModelProperty("The user that has added this Dimension")
    private User owner;

    public Dimensions() {
    }

    public Dimensions(String dimensionName, String dimensionValue, boolean status) {
        this.dimensionName = dimensionName;
        this.dimensionValue = dimensionValue;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDimensionName() {
        return dimensionName;
    }

    public void setDimensionName(String dimensionName) {
        this.dimensionName = dimensionName;
    }

    public String getDimensionValue() {
        return dimensionValue;
    }

    public void setDimensionValue(String dimensionValue) {
        this.dimensionValue = dimensionValue;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @XmlTransient
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
