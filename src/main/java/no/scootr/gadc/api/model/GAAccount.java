package no.scootr.gadc.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.LinkedList;
import java.util.List;

/**
 * The top-most entity in the Google Analytics hierarchy.
 * @see <a href="https://developers.google.com/analytics/devguides/config/mgmt/v3/#concepts">What Is The Management API - Conteptual Overview</a>
 * @see <a href="https://support.google.com/analytics/answer/1102152">Example account structures</a>
 */
@Entity
@XmlRootElement
@ApiModel(value = "Google Analytics Account", description = "The top-most entity in the Google Analytics hierarchy")
public class GAAccount {

    @Id
    @ApiModelProperty("Unique ID of the Google Analytics Account")
    private long id;

    @ApiModelProperty("Name of the Google Analytics account")
    private String name;

    @ManyToOne
    @NotNull
    @ApiModelProperty("The Google Account that has rights to the Google Analytics account")
    private GoogleAccount owner;

    @OneToMany(mappedBy = "gaAccount", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    private List<WebProperty> webProperties;

    public GAAccount() {
        this.webProperties = new LinkedList<>();
    }

    public GAAccount(String name, GoogleAccount owner, long id) {
        this();
        this.name = name;
        this.owner = owner;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement
    public GoogleAccount getOwner() {
        return owner;
    }

    public void setOwner(GoogleAccount owner) {
        this.owner = owner;
    }

    @XmlTransient
    @JsonIgnore
    public List<WebProperty> getWebProperties() {
        return webProperties;
    }

    public void setWebProperties(List<WebProperty> webProperties) {
        this.webProperties = webProperties;
    }
}
