package no.scootr.gadc.api.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.LinkedList;
import java.util.List;

/**
 * Tags are used for organizing several GA-Accounts.
 */
@Entity
@XmlRootElement
@ApiModel(value = "Tag", description = "Tags are used for organizing several GA-Accounts.")
public class Tag {

    @Id
    @GeneratedValue
    @XmlElement
    @ApiModelProperty("Unique ID of the tag")
    private int id;


    @Column(unique = true)
    @XmlElement
    @ApiModelProperty(value = "Name of the tag", example = "fashion")
    private String name;

    @ManyToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    private List<WebProperty> webProperties;

    public Tag() {
        this.webProperties = new LinkedList<>();
    }

    @XmlTransient
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @XmlTransient
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public List<WebProperty> getWebProperties() {
        return webProperties;
    }

    public void setWebProperties(List<WebProperty> webProperties) {
        this.webProperties = webProperties;
    }
}
