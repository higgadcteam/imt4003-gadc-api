package no.scootr.gadc.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;
import java.util.UUID;

/**
 * An authorization token that is used for a user to access this API.
 */
@Entity
@ApiModel(value = "User Authorization Token", description = "An authorization token that is used for a user to access this API.")
public class UserAuthToken extends OAuthToken {
    private static final long DEFAULT_TTL = 600000;

    @XmlTransient
    private long created;

    @XmlTransient
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.DETACH)
    private User user;

    public UserAuthToken() {
    }

    /**
     * Creates a new auth token and generates the values needed for it.
     * @param user The user that owns the auth token.
     */
    public UserAuthToken(User user) {
        super(OAuthToken.BEARER, UUID.randomUUID().toString(), UUID.randomUUID().toString(), DEFAULT_TTL);
        this.user = user;
        this.created = System.currentTimeMillis() / 1000L;
    }

    /**
     * Gets the authorization token value that the user will put in the authorization HTTP header.
     * @return A random, unique value for the token that the user will use in the authorization header.
     */
    @Override
    @XmlValue
    @ApiModelProperty(value = "The authorization token value that is put in the Authorization header of requests", example = "A random, unique token.")
    public String getTokenValue() {
        return String.valueOf(getId()) + "-" + super.getTokenValue();
    }

    /**
     * @return The random token that is part of the authorization token.
     */
    @XmlTransient
    @JsonIgnore
    public String getRandomTokenValue() {
        return super.getTokenValue();
    }

    /**
     * @return Gets the refresh token that is used to obtain a new auth token for the user.
     */
    @Override
    @XmlValue
    @ApiModelProperty("Refresh token for getting a new authorization token when the token expires")
    public String getRefreshToken() {
        return String.valueOf(getId()) + "-" + super.getRefreshToken();
    }

    /**
     * @return The user associated with the token.
     */
    @XmlTransient
    public User getUser() {
        return user;
    }

    /**
     * @return True if the token has expires, otherwise false.
     */
    @XmlTransient
    @JsonIgnore
    public boolean isExpired() {
        return System.currentTimeMillis() / 1000L > created + getTimeToLive();
    }
}
