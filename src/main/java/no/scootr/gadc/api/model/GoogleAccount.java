package no.scootr.gadc.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import no.scootr.gadc.api.utillity.lazyloading.LazyLoaderFactory;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

/**
 * The Google account that owns one or several GA Accounts. This is the same account that the user may use to
 * access other Google services such as Gmail, YouTube, etc. It holds a token that is used for authorization for
 * API requests in addition for a flag indicating whether the token is valid (has access to account data).
 */
@Entity
@XmlRootElement
@ApiModel(value = "Google Account",
        description = "The Google account that owns one or several GA Accounts. This is the same account that the user may use to " +
        "access other Google services such as Gmail, YouTube, etc. It holds a token that is used for authorization for " +
        "API requests in addition for a flag indicating whether the token is valid (has access to account data).")
public class GoogleAccount {

    @Id
    @GeneratedValue
    @XmlElement
    @ApiModelProperty("Unique ID of the Google Account")
    private int id;

    @Column(unique = true)
    @XmlElement
    @ApiModelProperty("The email address used to access the Google Account")
    private String email;
    @XmlElement
    @ApiModelProperty("Indicating whether the account is active or not in the system. If it is false, the user has to re-authorize the account")
    private boolean active;

    @OneToOne(cascade = CascadeType.REMOVE)
    private GoogleAccountAuthToken oAuthToken;

    @XmlElement
    @ManyToOne
    @ApiModelProperty("The user that has added this Google Account")
    private User owner;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @XmlTransient
    private List<GAAccount> gaAccounts;

    public GoogleAccount() {
    }

    public GoogleAccount(String email, boolean active, User owner) {
        this.email = email;
        this.active = active;
        this.owner = owner;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @XmlTransient
    @JsonIgnore
    public GoogleAccountAuthToken getOAuthToken() {
        return oAuthToken;
    }

    public void setOAuthToken(GoogleAccountAuthToken oAuthToken) {
        this.oAuthToken = oAuthToken;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


    public User getOwner() {
        return owner;
    }

    public void setOwner(User user) {
        this.owner = user;
    }

    @XmlTransient
    @JsonIgnore
    public List<GAAccount> getGaAccounts() {
        return LazyLoaderFactory.using(this, gaAccounts).assertLoaded(gaAccounts);
    }

    public void setGaAccounts(List<GAAccount> gaAccounts) {
        this.gaAccounts = gaAccounts;
    }
}
