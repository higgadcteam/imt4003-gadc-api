package no.scootr.gadc.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

/**
 * Web properties contains information specific to a tracking code that can be used on one or more pages.
 * @see <a href="https://developers.google.com/analytics/devguides/config/mgmt/v3/#concepts">What Is The Management API - Conteptual Overview</a>
 */
@Entity
@XmlRootElement
@ApiModel(value = "Web Property", description = "Web properties contains information specific to a tracking code that can be used on one or more pages.")
public class WebProperty {

    @Id
    @XmlElement
    @ApiModelProperty("Unique ID of the web property")
    private String id;

    @XmlElement
    @ApiModelProperty("Name of the web property")
    private String name;

    @XmlElement
    @ApiModelProperty(value = "URL of the web property", example = "http://www.example.com")
    private String websiteURL;

    @ManyToOne
    private GAAccount gaAccount;

    @XmlElement
    @ManyToMany(mappedBy = "webProperties", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @Fetch(FetchMode.SELECT)
    @ApiModelProperty(value = "The categories that the web property is categorized under.")
    private List<Category> categories;

    @XmlElement
    @ManyToMany(mappedBy = "webProperties", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @ApiModelProperty(value = "The different tags that the web property has beed tagged with.")
    private List<Tag> tags;

    @XmlElement
    @OneToMany(mappedBy = "webProperty", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @ApiModelProperty(value = "The profiles of the web property.")
    private List<Profile> profiles;

    public WebProperty() {
        this.tags = new ArrayList<>();
    }

    public WebProperty(String id, String name, GAAccount gaAccount) {
        this.id = id;
        this.name = name;
        this.gaAccount = gaAccount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebsiteURL() {
        return websiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        this.websiteURL = websiteURL;
    }

    public GAAccount getGaAccount() {
        return gaAccount;
    }

    public void setGaAccount(GAAccount gaAccount) {
        this.gaAccount = gaAccount;
    }

    @XmlTransient
    @JsonIgnore
    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @XmlTransient
    @JsonIgnore
    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    @XmlTransient
    @JsonIgnore
    public List<Profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<Profile> profiles) {
        this.profiles = profiles;
    }
}
