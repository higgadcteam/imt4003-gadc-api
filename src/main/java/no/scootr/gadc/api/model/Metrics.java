package no.scootr.gadc.api.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Metrics to be used to get data from the Google Analytics Reporting API. Some examples of metrics can be: sessions, bounce rate, newUsers etc.
 */
@Entity
@XmlRootElement
@ApiModel(value = "Metric", description = "Metrics to be used to get data from the Google Analytics Reporting API. Some examples of metrics can be: sessions, bounce rate, newUsers etc.")
public class Metrics {

    @Id
    @GeneratedValue
    @XmlElement
    @ApiModelProperty("Unique ID of the metric")
    private int id;

    @XmlElement
    @ApiModelProperty("Name of the metric")
    private String metricsName;

    @XmlElement
    //@Column(unique = true)
    @ApiModelProperty("Value of the metric")
    private String metricsValue;

    @XmlElement
    @ApiModelProperty("Status indicating if the metric is active or not")
    private boolean status;

    @ManyToOne
    @ApiModelProperty("The user that has added this Metric")
    private User owner;

    public Metrics() {
    }

    public Metrics(String metricsName, String metricsValue, boolean status) {
        this.metricsName = metricsName;
        this.metricsValue = metricsValue;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMetricsName() {
        return metricsName;
    }

    public void setMetricsName(String metricsName) {
        this.metricsName = metricsName;
    }

    public String getMetricsValue() {
        return metricsValue;
    }

    public void setMetricsValue(String metricsValue) {
        this.metricsValue = metricsValue;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @XmlTransient
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
