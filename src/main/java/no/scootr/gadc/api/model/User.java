package no.scootr.gadc.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.LinkedList;
import java.util.List;

/**
 * Representing data about one user in the system.
 */
@Entity
@XmlRootElement
@ApiModel(value = "User", description = "Representing data about a registered user in the system.")
public class User {
    @Id
    @GeneratedValue
    @XmlElement
    @ApiModelProperty("Unique ID of the user")
    private int id;

    @Column(unique = true)
    @XmlElement
    @ApiModelProperty("Email of the user")
    private String email;

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    private List<UserAuthToken> accessTokens;

    @XmlTransient
    @OneToMany(mappedBy = "owner", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    private List<GoogleAccount> accounts;

    @XmlTransient
    @OneToMany(mappedBy = "owner", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    private List<Category> categories;

    @XmlTransient
    @OneToMany(mappedBy = "owner", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    private List<Dimensions> dimensions;

    @XmlTransient
    @OneToMany(mappedBy = "owner", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    private List<Metrics> metriccs;

    public User() {
        this.accounts = new LinkedList<>();
        this.accessTokens = new LinkedList<>();
    }

    public User(String email) {
        this();
        this.email = email;
    }

    @XmlTransient
    @JsonIgnore
    public String getEmail() {
        return email;
    }

    @XmlTransient
    @JsonIgnore
    public List<UserAuthToken> getAccessTokens() {
        return accessTokens;
    }

    public int getId() {
        return id;
    }

    @XmlTransient
    @JsonIgnore
    public List<GoogleAccount> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<GoogleAccount> accounts) {
        this.accounts = accounts;
    }
}
