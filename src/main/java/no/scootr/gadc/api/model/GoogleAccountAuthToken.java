package no.scootr.gadc.api.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Contains token information about a token that is used to access a Google Account.
 */
@Entity
public class GoogleAccountAuthToken extends OAuthToken {
    private long created;

    @OneToOne
    private GoogleAccount googleAccount;

    public GoogleAccountAuthToken(String tokenType, String token, String refreshToken, long timeToLive, long created, GoogleAccount googleAccount) {
        super(tokenType, token, refreshToken, timeToLive);
        this.created = created;
        this.googleAccount = googleAccount;
    }

    public GoogleAccountAuthToken() {
    }

    public GoogleAccount getGoogleAccount() {
        return googleAccount;
    }

    public void setGoogleAccount(GoogleAccount googleAccount) {
        this.googleAccount = googleAccount;
    }

    public long getExpirationTime() {
        return created + getTimeToLive();
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }
}
