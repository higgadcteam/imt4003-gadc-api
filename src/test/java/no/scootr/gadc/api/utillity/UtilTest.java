package no.scootr.gadc.api.utillity;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test case testing methods in the Utility Test.
 */
public class UtilTest {

    @Test
    public void testValidNumber() throws Exception {
        assertTrue(Util.isValidDouble("1"));
        assertTrue(Util.isValidDouble("0.0"));
        assertTrue(Util.isValidDouble("0"));
        assertTrue(Util.isValidDouble("1.3"));
        assertFalse(Util.isValidDouble(""));
        assertFalse(Util.isValidDouble(null));
    }
}