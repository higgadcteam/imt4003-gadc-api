package no.scootr.gadc.api.utillity;

/**
 * Created by marti_000 on 15.11.2015.
 */
public interface GenericTransactionRequest {
    void execute() throws Exception;
}
