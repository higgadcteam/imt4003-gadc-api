package no.scootr.gadc.api.utillity;

/**
 * Created by marti_000 on 15.11.2015.
 */
public interface TransactionRequest<T> {
    T execute() throws Exception;
}
