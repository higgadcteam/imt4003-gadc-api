package no.scootr.gadc.api.utillity;

import org.needle4j.db.transaction.TransactionHelper;
import org.needle4j.db.transaction.VoidRunnable;
import org.needle4j.junit.DatabaseRule;

import javax.persistence.EntityManager;

/**
 * Helper class to make it easier and cleaner to executeAndGet calls that requires transactional support.
 */
public class Transactional<T> {
    private TransactionHelper transactionHelper;
    private T returnObject;

    private Transactional(TransactionHelper transactionHelper) {
        this.transactionHelper = transactionHelper;
    }

    /**
     * Creates a new transactional helper instance.
     * @param databaseRule The database rule used for database communication.
     * @return A new {@code Transactional} object.
     */
    public static Transactional using(final DatabaseRule databaseRule) {
        return new Transactional(databaseRule.getTransactionHelper());
    }

    public static <T> Transactional<T> fetch(final DatabaseRule databaseRule) {
        return new Transactional<>(databaseRule.getTransactionHelper());
    }

    /**
     * Executes the transaction.
     * @param call The call requiring transactional support.
     * @throws Exception
     */
    public T executeAndGet(final TransactionRequest<T> call) throws Exception {
        transactionHelper.executeInTransaction(new VoidRunnable() {
            @Override
            public void doRun(EntityManager entityManager) throws Exception {
                returnObject = call.execute();
            }
        });

        return returnObject;
    }

    public void execute(final GenericTransactionRequest call) throws Exception {
        transactionHelper.executeInTransaction(new VoidRunnable() {
            @Override
            public void doRun(EntityManager entityManager) throws Exception {
                call.execute();
            }
        });
    }
}
