package no.scootr.gadc.api.utillity.json;

import org.junit.Test;

import javax.json.*;

import static org.junit.Assert.*;

/**
 * Test case for merging JSON documents.
 */
public class JsonMergerTest {

    @Test
    public void testMergeJsonArray() throws Exception {
        JsonMerger jsonMerger = new JsonMerger();

        final JsonArray firstArray = Json.createArrayBuilder()
                .add("1.0").add("2").add("3").build();

        final JsonObject o1 = Json.createObjectBuilder()
                .add("foo", firstArray)
                .build();

        final JsonObject o2 = Json.createObjectBuilder()
                .add("foo", firstArray)
                .build();

        final JsonObject mergedObject = jsonMerger.mergeObjects(new JsonObject[]{o1, o2});
        assertEquals(1, mergedObject.size());

        final JsonArray secondArray = Json.createArrayBuilder()
                .add(1).add(2).add(3).build();

        final JsonArray array = jsonMerger.mergeJsonArray(firstArray, secondArray);
        assertEquals(3, array.size());
        assertEquals(JsonValue.ValueType.NUMBER, array.get(0).getValueType());
        assertEquals(2, ((JsonNumber) array.get(0)).intValue());
    }
}