package no.scootr.gadc.api.resources;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.GAAccount;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.model.UserAuthToken;
import no.scootr.gadc.api.requestbeans.GAAccountAndWebPropertyRequest;
import no.scootr.gadc.api.requestbeans.GAAccountRequest;
import no.scootr.gadc.api.services.GAAccountService;
import no.scootr.gadc.api.services.UserTokenService;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.lang.Exception;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * This class contains unit testing for getting GaAccounts, get GaAccount by id, adding a GaAccount,
 * adding a GaAccount with web property, deleting a GaAccount.
 */
public class GAAccountResourceTest {

    private GAAccountResource gaAccountResource;
    private UserAuthToken userAuthToken;
    private User loggedUser;

    @Before
    public void testMock() {
        gaAccountResource = new GAAccountResource();
        gaAccountResource.gaAccountService = mock(GAAccountService.class);
        gaAccountResource.securityContext = mock(SecurityContext.class);
        gaAccountResource.userTokenService = mock(UserTokenService.class);

        userAuthToken = mock(UserAuthToken.class);
        loggedUser = new User();
        loggedUser = mock(User.class);
    }

    @Test
    public void testGetGAAccounts() {
        final Principal principal = mock(Principal.class);
        when(gaAccountResource.securityContext.getUserPrincipal()).thenReturn(principal);
        when(gaAccountResource.userTokenService.getUserAuthToken(gaAccountResource.securityContext.getUserPrincipal().getName())).thenReturn(userAuthToken);
        when(userAuthToken.getUser()).thenReturn(loggedUser);

        final List<GAAccount> gaAccountsList = new ArrayList<GAAccount>();
        gaAccountsList.add(mock(GAAccount.class));

        when(gaAccountResource.gaAccountService.getAllGAAccounts(loggedUser)).thenReturn(gaAccountsList);

        assertEquals(gaAccountsList, gaAccountResource.getAllGAAccountList());


    }

    @Test
    public void testGetGAAccountByID() {
        GAAccount gaAccount = mock(GAAccount.class);
        when(gaAccountResource.gaAccountService.getGAAccountByID(1)).thenReturn(gaAccount);
        Response found = gaAccountResource.getGAAccountByID(1);
        assertEquals(200, found.getStatus());
    }

    @Test(expected = WebApplicationException.class)
    public void testGetGAAccountByWrongId() throws  Exception {
        Response notFound = gaAccountResource.getGAAccountByID(6);
    }

    @Test
    public void testAddGAAccount() throws CustomizedMessageException {
        final GAAccountRequest gaAccountRequest = mock(GAAccountRequest.class);
        final GAAccount addedGaAccount = mock(GAAccount.class);

        when(gaAccountResource.gaAccountService.addGAAccount(gaAccountRequest)).thenReturn(addedGaAccount);

        final Response response = gaAccountResource.addGAAccount(gaAccountRequest);

        assertEquals(addedGaAccount, response.getEntity());
    }

    @Test
    public void testAddGAAccountWithWebProperty() throws CustomizedMessageException {
        final GAAccountAndWebPropertyRequest gaAccountAndWebPropertyRequest = mock(GAAccountAndWebPropertyRequest.class);
        final GAAccount addedGaAccount = mock(GAAccount.class);

        when(gaAccountResource.gaAccountService.addGAAccountWithWPs(gaAccountAndWebPropertyRequest)).thenReturn(addedGaAccount);

        final Response response = gaAccountResource.addGAAccountWithWPs(gaAccountAndWebPropertyRequest);
        assertEquals(addedGaAccount, response.getEntity());
    }

    @Test
    public void testDeleteGAAccount() throws CustomizedMessageException {
        GAAccount gaAccount = mock(GAAccount.class);
        long gaAccountId = 1;

        when(gaAccountResource.gaAccountService.getGAAccountByID(gaAccountId)).thenReturn(gaAccount);
        when(gaAccountResource.gaAccountService.deleteGAAccount(gaAccount)).thenReturn(gaAccount);

        Response response = gaAccountResource.deleteGAAccount(gaAccountId);
        assertEquals(gaAccount, response.getEntity());
    }
}