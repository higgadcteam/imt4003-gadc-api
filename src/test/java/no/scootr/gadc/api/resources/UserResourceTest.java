package no.scootr.gadc.api.resources;

import no.scootr.gadc.api.google.GoogleService;
import no.scootr.gadc.api.model.OAuthToken;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.model.UserAuthToken;
import no.scootr.gadc.api.requestbeans.LoginRequest;
import no.scootr.gadc.api.requestbeans.RefreshTokenRequest;
import no.scootr.gadc.api.services.UserService;
import no.scootr.gadc.api.services.UserTokenService;
import org.junit.Before;
import org.junit.Test;
import java.lang.Exception;

import javax.json.JsonObject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.io.IOException;
import java.security.Principal;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * This class contains unit testing for getting a user info and deleting a user auth token.
 */
public class UserResourceTest {
    private UserResource userResource;

    @Before
    public void testMock() {
        userResource = new UserResource();
        userResource.userService = mock(UserService.class);
        userResource.securityContext = mock(SecurityContext.class);
        userResource.userTokenService = mock(UserTokenService.class);
        userResource.googleService = mock(GoogleService.class);
    }

    @Test
    public void testGetUserInfo() throws Exception {
        when(userResource.userService.getUserById(3)).thenReturn(new User("masha@gmail.com"));
        Response found = userResource.getUserInfo(3);
        assertEquals(200, found.getStatus());
    }

    @Test(expected = WebApplicationException.class)
    public void testGetUserInfoByWrongId() throws  Exception {
        userResource.getUserInfo(1);
    }

    @Test
    public void testDeleteUserAuthToken() {
        final Principal principal = mock(Principal.class);

        final UserAuthToken userAuthToken = mock(UserAuthToken.class);
        when(userResource.securityContext.getUserPrincipal()).thenReturn(principal);
        when(userResource.userTokenService.getUserAuthToken(userResource.securityContext.getUserPrincipal().getName())).thenReturn(userAuthToken);

        assertEquals(Response.Status.OK.getStatusCode(), userResource.invalidateToken().getStatus());

        verify(userResource.userTokenService).removeToken(userAuthToken);
    }

    @Test
    public void testLogin() throws IOException {
        assertNull(System.getenv("GA_API_LOGIN_REDIRECTION_URL"));
        final Principal principal = mock(Principal.class);

        final String loginAuthToken = "secret";
        final String email = "foo@example.com";
        final OAuthToken oAuthToken = mock(OAuthToken.class);
        final LoginRequest loginRequest = mock(LoginRequest.class);

        final User user = mock(User.class);

        when(loginRequest.getToken()).thenReturn(loginAuthToken);

        final UserAuthToken userAuthToken = mock(UserAuthToken.class);

        final String googleOauthTokenValue = "secret2";
        when(oAuthToken.getTokenValue()).thenReturn(googleOauthTokenValue);
        final JsonObject jsonObject = mock(JsonObject.class);

        when(jsonObject.getString("email")).thenReturn(email);
        when(userResource.googleService.getUserInfo(googleOauthTokenValue)).thenReturn(jsonObject);

        when(userResource.securityContext.getUserPrincipal()).thenReturn(principal);
        when(userResource.googleService.getOauthToken(loginAuthToken, null)).thenReturn(oAuthToken);
        when(userResource.userService.getOrRegisterUser(email)).thenReturn(user);
        when(userResource.userTokenService.createToken(user)).thenReturn(userAuthToken);

        final Response response = userResource.login(loginRequest);

        assertEquals(userAuthToken, response.getEntity());
    }

    @Test
    public void testGetUserRefreshToken() {
        final String refreshToken = "secret";
        final RefreshTokenRequest refreshTokenRequest = mock(RefreshTokenRequest.class);
        final User user = mock(User.class);
        final UserAuthToken userAuthToken = mock(UserAuthToken.class);

        when(refreshTokenRequest.getRefreshToken()).thenReturn(refreshToken);
        when(userResource.userTokenService.findRefreshToken(refreshToken)).thenReturn(user);
        when(userResource.userTokenService.createToken(user)).thenReturn(userAuthToken);

        final Response response = userResource.getUserRefreshToken(refreshTokenRequest);
        assertEquals(userAuthToken, response.getEntity());
    }
}