package no.scootr.gadc.api.resources;

import no.scootr.gadc.api.google.GoogleAnalyticsService;
import no.scootr.gadc.api.model.*;
import no.scootr.gadc.api.services.ProfileService;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.utillity.json.JsonMerger;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * This class contains unit testing for Getting the analytics data.
 */
public class DataResourceTest {
    private DataResource dataResource;

    @Before
    public void setupTest() {
        dataResource = new DataResource();
        dataResource.profileService = mock(ProfileService.class);
        dataResource.userTokenService = mock(UserTokenService.class);
        dataResource.googleAnalyticsService = mock(GoogleAnalyticsService.class);
        dataResource.securityContext = mock(SecurityContext.class);
        dataResource.jsonMerger = mock(JsonMerger.class);
    }

    @Test
    public void testGetAnalyticsData() throws java.lang.Exception {
        final UriInfo uriInfo = mock(UriInfo.class);
        final List<String> ids = new ArrayList<>(2);
        final MultivaluedMap<String, String> multivaluedMap = mock(MultivaluedMap.class);
        ids.add("ga:123");
        ids.add("ga:321");

        when(uriInfo.getQueryParameters()).thenReturn(multivaluedMap);

        final Principal principal = mock(Principal.class);

        final UserAuthToken userAuthToken = mock(UserAuthToken.class);
        when(dataResource.securityContext.getUserPrincipal()).thenReturn(principal);
        when(dataResource.userTokenService.getUserAuthToken(dataResource.securityContext.getUserPrincipal().getName())).thenReturn(userAuthToken);

        final User user = mock(User.class);

        when(userAuthToken.getUser()).thenReturn(user);

        final Profile firstProfile = mockProfile();
        final Profile secondProfile = mockProfile();
        when(dataResource.profileService.getProfileByID(123)).thenReturn(firstProfile);
        when(dataResource.profileService.getProfileByID(321)).thenReturn(secondProfile);

        when(firstProfile.getWebProperty().getGaAccount().getOwner().getOwner().getId()).thenReturn(123);
        when(secondProfile.getWebProperty().getGaAccount().getOwner().getOwner().getId()).thenReturn(123);

        assertNotNull(firstProfile.getWebProperty().getGaAccount().getOwner().getOwner().getId());
        when(user.getId()).thenReturn(123);

        final Set<String> keys = new HashSet<>();
        keys.add("foo");

        final List<String> values = new ArrayList<>(1);
        values.add("bar");
        when(multivaluedMap.keySet()).thenReturn(keys);

        when(multivaluedMap.get("foo")).thenReturn(values);

        dataResource.getAnalyticsData(uriInfo, ids);

        verify(dataResource.googleAnalyticsService).getAnalyticsData(anyString(), eq(firstProfile));
        verify(dataResource.googleAnalyticsService).getAnalyticsData(anyString(), eq(secondProfile));

        verify(dataResource.jsonMerger).mergeObjects(anyObject());
    }

    private Profile mockProfile() {
        final Profile profile = mock(Profile.class);
        when(profile.getWebProperty()).thenReturn(mock(WebProperty.class));
        when(profile.getWebProperty().getGaAccount()).thenReturn(mock(GAAccount.class));
        when(profile.getWebProperty().getGaAccount().getOwner()).thenReturn(mock(GoogleAccount.class));
        when(profile.getWebProperty().getGaAccount().getOwner().getOwner()).thenReturn(mock(User.class));

        return profile;
    }
}