package no.scootr.gadc.api.resources;
import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.Category;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.model.UserAuthToken;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.requestbeans.CategoryRequest;
import no.scootr.gadc.api.services.CategoryService;
import no.scootr.gadc.api.services.UserTokenService;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.security.Principal;
import java.util.*;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * This class contains unit testing for Get all categories, get category by ID, get web properties by category,
 * adding a category, modify a category and delete a category.
 */

public class CategoryResourceTest {
    private CategoryResource categoryResource;

    private UserAuthToken userAuthToken;
    private User loggedUser;

    @Before
    public void testMock() {
        categoryResource = new CategoryResource();
        categoryResource.categoryService = mock(CategoryService.class);
        categoryResource.securityContext = mock(SecurityContext.class);
        categoryResource.userTokenService = mock(UserTokenService.class);
        categoryResource.securityContext = mock(SecurityContext.class);

        userAuthToken = mock(UserAuthToken.class);
        loggedUser = new User();
        loggedUser = mock(User.class);

    }

    @Test
    public void testGetAllCategoriesList() {
        final Principal principal = mock(Principal.class);

        when(categoryResource.securityContext.getUserPrincipal()).thenReturn(principal);
        when(categoryResource.userTokenService.getUserAuthToken(categoryResource.securityContext.getUserPrincipal().getName())).thenReturn(userAuthToken);
        when(userAuthToken.getUser()).thenReturn(loggedUser);

        final List<Category> categoryList = new ArrayList<Category>();
        categoryList.add(mock(Category.class));

        when(categoryResource.categoryService.getCategories(loggedUser)).thenReturn(categoryList);

        assertEquals(categoryList, categoryResource.getAllCategoriesList());
    }

    @Test
    public void testGetCategoryById() {
        //simulate Category ID = 1 in the DB
        when(categoryResource.categoryService.getCategoryById(1)).thenReturn(new Category("games"));
       //test response when ID exists
        javax.ws.rs.core.Response found = categoryResource.getCategoryInfo(1);
        assertEquals(200, found.getStatus());
    }

    @Test(expected = WebApplicationException.class)
    public void testGetCategoryByWrongId() throws  Exception {
        javax.ws.rs.core.Response notFound = categoryResource.getCategoryInfo(999);
    }

    @Test
    public void testGetWebPropertiesByCategory() {
        final int categoryId = 2;
        final Category category = mock(Category.class);
        when(categoryResource.categoryService.getCategoryById(categoryId)).thenReturn(category);
        final WebProperty webProperty = mock(WebProperty.class);
        final List<WebProperty> webProperties = new ArrayList<>();
        webProperties.add(webProperty);

        when(category.getWebProperties()).thenReturn(webProperties);

        final Response response = categoryResource.getWebPropertiesByCategory(categoryId);

        assertEquals(webProperties, response.getEntity());
    }

    @Test
    public void testAddCategory() throws CustomizedMessageException {
        categoryResource.categoryService = mock(CategoryService.class);

        final CategoryRequest categoryRequest = mock(CategoryRequest.class);
        final String categoryName = "foobar";

        when(categoryRequest.getName()).thenReturn(categoryName);

        final Principal principal = mock(Principal.class);

        when(categoryResource.securityContext.getUserPrincipal()).thenReturn(principal);
        when(categoryResource.userTokenService.getUserAuthToken(categoryResource.securityContext.getUserPrincipal().getName())).thenReturn(userAuthToken);
        when(userAuthToken.getUser()).thenReturn(loggedUser);

        when(categoryResource.categoryService.getCategory(categoryRequest.getName())).thenReturn(null);

        categoryResource.addCategory(categoryRequest);


        final Response response = categoryResource.addCategory(categoryRequest);

        final Category addedCategory = (Category) response.getEntity();

        assertEquals(addedCategory.getOwner(), loggedUser);
        assertEquals(categoryName, addedCategory.getName());
    }

    @Test
    public void testModifyCategory() {
        final int categoryId = 1;
        final Category category = mock(Category.class);
        final String categoryName = "some category name";

        final Category storedCategory = mock(Category.class);

        when(storedCategory.getName()).thenReturn(categoryName);

        when(categoryResource.categoryService.getCategoryById(categoryId)).thenReturn(storedCategory);
        when(categoryResource.categoryService.update(storedCategory)).thenReturn(storedCategory);

        Response response = categoryResource.modifyCategory(categoryId, category);
        Category returnedCategory = (Category) response.getEntity();

        assertEquals(storedCategory, returnedCategory);
        assertEquals(categoryName, returnedCategory.getName());
    }

    @Test
    public void testDeleteCategory() throws CustomizedMessageException {
        Category category = mock(Category.class);
        int categoryId = anyInt();
        when(categoryResource.categoryService.getCategoryById(categoryId)).thenReturn(category);
        when(categoryResource.categoryService.deleteCategory(category)).thenReturn(category);

        Response response = categoryResource.deleteCategory(categoryId);
        assertEquals(category, response.getEntity());
    }
}