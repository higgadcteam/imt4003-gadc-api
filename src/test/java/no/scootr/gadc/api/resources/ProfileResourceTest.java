package no.scootr.gadc.api.resources;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.Profile;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.services.ProfileService;
import no.scootr.gadc.api.services.WebPropertyService;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;
import java.lang.Exception;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * This class contains unit testing for getting profiles of web property, getting a profile by ID, delete a profile.
 */
public class ProfileResourceTest {
    private ProfileResource profileResource;

    @Before
    public void testMock() {
        profileResource = new ProfileResource();
        profileResource.profileService = mock(ProfileService.class);
        profileResource.webPropertyService = mock(WebPropertyService.class);
    }

    @Test
    public void testGetProfilesOfWebProperty() {
        final String webPropertyId = "sss";
        final WebProperty webProperty = mock(WebProperty.class);
        when(profileResource.webPropertyService.getWebPropertyById(webPropertyId)).thenReturn(webProperty);
        Profile profile = mock(Profile.class);
        final List<Profile> profiles = new ArrayList<>();
        profiles.add(profile);
        when(webProperty.getProfiles()).thenReturn(profiles);
        final Response response = profileResource.getProfilesOfWebProperty(webPropertyId);
        assertEquals(profiles, response.getEntity());

    }

    @Test
    public void testGetProfileByID() {
        when(profileResource.profileService.getProfileByID(1)).thenReturn(new Profile());
        Response found = profileResource.getProfileByID(1);
        assertEquals(200, found.getStatus());
    }

    @Test(expected = WebApplicationException.class)
    public void testFindProfileByWrongID() throws  Exception {
        profileResource.getProfileByID(9);
    }

    @Test
    public void testDeleteProfile() throws CustomizedMessageException {
        Profile profile = mock(Profile.class);
        int profileId = anyInt();

        when(profileResource.profileService.getProfileByID(profileId)).thenReturn(profile);
        when(profileResource.profileService.deleteProfile(profile)).thenReturn(profile);

        Response response = profileResource.getProfileByID(profileId);
        assertEquals(profile, response.getEntity());
    }
}