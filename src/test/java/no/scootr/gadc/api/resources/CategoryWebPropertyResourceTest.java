package no.scootr.gadc.api.resources;

import no.scootr.gadc.api.model.Category;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.model.UserAuthToken;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.requestbeans.CategorizeWebPropertyRequest;
import no.scootr.gadc.api.services.CategoryService;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.services.WebPropertyService;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.security.Principal;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


/**
 * This class contains unit testing for Categorize web property and Delete category from web property.
 */
public class CategoryWebPropertyResourceTest {
    private CategoryWebPropertyResource categoryWebPropertyResource;

    @Before
    public void testMock() {
        categoryWebPropertyResource = new CategoryWebPropertyResource();
        categoryWebPropertyResource.categoryService = mock(CategoryService.class);
        categoryWebPropertyResource.wpService = mock(WebPropertyService.class);
        categoryWebPropertyResource.userTokenService = mock(UserTokenService.class);
        categoryWebPropertyResource.securityContext = mock(SecurityContext.class);
    }

    @Test
    public void testCategorizeWebProperty() {
        final String webPropertyId = "webPropertyId";
        final int categoryId = 1;
        final int userId = 2;
        final CategorizeWebPropertyRequest request = mock(CategorizeWebPropertyRequest.class);

        final WebProperty webProperty = mock(WebProperty.class);
        final Category category = mock(Category.class);
        final User loggedUser = mock(User.class);

        final UserAuthToken userAuthToken = mock(UserAuthToken.class);
        final Principal principal = mock(Principal.class);

        when(categoryWebPropertyResource.wpService.getWebPropertyById(webPropertyId)).thenReturn(webProperty);
        when(categoryWebPropertyResource.categoryService.getCategoryById(categoryId)).thenReturn(category);

        when(request.getCategoryID()).thenReturn(categoryId);
        when(loggedUser.getId()).thenReturn(userId);

        when(categoryWebPropertyResource.securityContext.getUserPrincipal()).thenReturn(principal);
        when(categoryWebPropertyResource.userTokenService.getUserAuthToken(categoryWebPropertyResource.securityContext.getUserPrincipal().getName())).thenReturn(userAuthToken);
        when(userAuthToken.getUser()).thenReturn(loggedUser);

        when(category.getOwner()).thenReturn(loggedUser);

        when(categoryWebPropertyResource.categoryService.containsWebProperty(category, webProperty)).thenReturn(true);
        when(categoryWebPropertyResource.wpService.containsCategory(category, webProperty)).thenReturn(true);

        final Response response = categoryWebPropertyResource.categorizeWebProperty(webPropertyId, request);
        assertEquals(webProperty, response.getEntity());
    }

    @Test
    public void testDeleteCategoryFromWebProperty() throws java.lang.Exception {
        final WebProperty webProperty = mock(WebProperty.class);
        final Category category = mock(Category.class);

        final int categoryId = 1;
        final String webPropertyId = "123";

        when(categoryWebPropertyResource.wpService.getWebPropertyById(webPropertyId)).thenReturn(webProperty);
        when(categoryWebPropertyResource.categoryService.getCategoryById(categoryId)).thenReturn(category);

        when(categoryWebPropertyResource.categoryService.containsWebProperty(category, webProperty)).thenReturn(true);
        when(categoryWebPropertyResource.wpService.containsCategory(category, webProperty)).thenReturn(true);

        final Response response = categoryWebPropertyResource.deleteCategoryFromWebProperty(webPropertyId, categoryId);

        assertEquals(webProperty, response.getEntity());

        verify(categoryWebPropertyResource.categoryService, times(1)).deleteWebPropertyFromCategory(category, webProperty);
        verify(categoryWebPropertyResource.categoryService, times(1)).edit(category);

        verify(categoryWebPropertyResource.wpService, times(1)).deleteCategoryFromWebProperty(category, webProperty);
        verify(categoryWebPropertyResource.wpService, times(1)).edit(webProperty);
    }
}