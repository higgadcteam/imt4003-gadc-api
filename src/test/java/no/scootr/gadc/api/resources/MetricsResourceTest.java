package no.scootr.gadc.api.resources;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.Metrics;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.model.UserAuthToken;
import no.scootr.gadc.api.requestbeans.MetricsRequest;
import no.scootr.gadc.api.services.MetricsService;
import no.scootr.gadc.api.services.UserTokenService;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.lang.Exception;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * This class contains unit testing for getting all available metrics, get a single metric, delete metric and
 * add new metric.
 */
public class MetricsResourceTest {
    private MetricsResource metricsResource;
    private UserAuthToken userAuthToken;
    private User loggedUser;

    @Before
    public void testMock() {
        metricsResource = new MetricsResource();
        metricsResource.metricsService = mock(MetricsService.class);
        metricsResource.securityContext = mock(SecurityContext.class);
        metricsResource.userTokenService = mock(UserTokenService.class);

        userAuthToken = mock(UserAuthToken.class);
        loggedUser = new User();
        loggedUser = mock(User.class);

    }

    @Test
    public void testGetAllAvailableMetrics() {
        final Principal principal = mock(Principal.class);
        when(metricsResource.securityContext.getUserPrincipal()).thenReturn(principal);
        when(metricsResource.userTokenService.getUserAuthToken(metricsResource.securityContext.getUserPrincipal().getName())).thenReturn(userAuthToken);
        when(userAuthToken.getUser()).thenReturn(loggedUser);
        final List<Metrics> metricsList = new ArrayList<Metrics>();
        metricsList.add(mock(Metrics.class));
        when(metricsResource.metricsService.findAllUserMetrics(loggedUser)).thenReturn(metricsList);
        assertEquals(metricsList, metricsResource.getAllAvailableMetrics());
    }

    @Test
    public void testGetSingleMetrics() throws Exception {
        when(metricsResource.metricsService.findByID(4)).thenReturn(new Metrics("asd", "dgg", true));
        Response found = metricsResource.getSingleMetrics(4);
        assertEquals(200, found.getStatus());
    }

    @Test(expected = WebApplicationException.class)
    public void testGetUserInfoByWrongId() throws  Exception {
        metricsResource.getSingleMetrics(4);
    }

    @Test
    public void testDeleteMetric() throws CustomizedMessageException {
        Metrics metric = mock(Metrics.class);
        int metricId = anyInt();

        when(metricsResource.metricsService.findByID(metricId)).thenReturn(metric);
        when(metricsResource.metricsService.deleteMetrics(metric)).thenReturn(metric);

        Response response = metricsResource.deleteMetric(metricId);
        assertEquals(metric, response.getEntity());
    }

    @Test
    public void testAddNewMetrics() throws CustomizedMessageException {
        final String metricName = "test";
        final String metricValue = "testValue";
        final boolean status = true;
        final MetricsRequest metricsRequest = mock(MetricsRequest.class);
        when(metricsRequest.getMetricsValue()).thenReturn(metricValue);
        when(metricsRequest.getMetricsName()).thenReturn(metricName);
        when(metricsRequest.getStatus()).thenReturn(status);

        final Principal principal = mock(Principal.class);
        when(metricsResource.securityContext.getUserPrincipal()).thenReturn(principal);
        when(metricsResource.userTokenService.getUserAuthToken(metricsResource.securityContext.getUserPrincipal().getName())).thenReturn(userAuthToken);
        when(userAuthToken.getUser()).thenReturn(loggedUser);

        final Metrics metrics = mock(Metrics.class);
        when(metrics.getOwner()).thenReturn(loggedUser);
        when(metricsResource.metricsService.findByMetricsValueAndOwner(metricsRequest.getMetricsValue().toLowerCase(), loggedUser)).thenReturn(metrics);

        Response response = metricsResource.addNewMetrics(metricsRequest);

        assertEquals(metrics, response.getEntity());
        verify(metricsResource.metricsService).edit(anyObject());
    }
}