package no.scootr.gadc.api.resources;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.Dimensions;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.model.UserAuthToken;
import no.scootr.gadc.api.requestbeans.DimensionsRequest;
import no.scootr.gadc.api.services.DimensionsService;
import no.scootr.gadc.api.services.UserTokenService;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * This class contains unit testing for getting all available dimensions, get single dimension, delete a dimension and
 * create dimension.
 */

public class DimensionResourceTest {
    private DimensionResource dimensionResource;
    private UserAuthToken userAuthToken;
    private User loggedUser;

    @Before
    public void testMock() {
        dimensionResource = new DimensionResource();
        dimensionResource.dimensionsService = mock(DimensionsService.class);
        dimensionResource.securityContext = mock(SecurityContext.class);
        dimensionResource.userTokenService = mock(UserTokenService.class);
        userAuthToken = mock(UserAuthToken.class);
        loggedUser = new User();
        loggedUser = mock(User.class);
    }


    @Test
    public void testGetAllAvailableDimensions() {
        final Principal principal = mock(Principal.class);
        when(dimensionResource.securityContext.getUserPrincipal()).thenReturn(principal);
        when(dimensionResource.userTokenService.getUserAuthToken(dimensionResource.securityContext.getUserPrincipal().getName())).thenReturn(userAuthToken);
        when(userAuthToken.getUser()).thenReturn(loggedUser);

        final List<Dimensions> dimensionsList = new ArrayList<Dimensions>();
        dimensionsList.add(mock(Dimensions.class));

        when(dimensionResource.dimensionsService.findAllUserDimension(loggedUser)).thenReturn(dimensionsList);
        assertEquals(dimensionsList, dimensionResource.getAllAvailableDimensions());
    }

    @Test
        public void testGetSingleDimension() {
            when(dimensionResource.dimensionsService.findByID(1)).thenReturn(new Dimensions("qwe", "asd", true));
            Response found = dimensionResource.getSingleDimension(1);
            assertEquals(200, found.getStatus());
        }

    @Test(expected = WebApplicationException.class)
    public void testGetSingleDimensionByWrongID() throws  java.lang.Exception {
        Response notFound = dimensionResource.getSingleDimension(999);
    }

    @Test
    public void testCreateDimension() {
        final String dimensionName = "foo";
        final String dimensionValue = "bar";
        final int userId = 1;

        when(loggedUser.getId()).thenReturn(userId);
        final DimensionsRequest dimensionsRequest = mock(DimensionsRequest.class);
        when(dimensionsRequest.getDimensionName()).thenReturn(dimensionName);
        when(dimensionsRequest.getDimensionValue()).thenReturn(dimensionValue);

        final Principal principal = mock(Principal.class);

        when(dimensionResource.securityContext.getUserPrincipal()).thenReturn(principal);
        when(dimensionResource.userTokenService.getUserAuthToken(dimensionResource.securityContext.getUserPrincipal().getName())).thenReturn(userAuthToken);
        when(userAuthToken.getUser()).thenReturn(loggedUser);

        final Dimensions dimensions = new Dimensions();
        dimensions.setOwner(loggedUser);
        when(dimensionResource.dimensionsService.findByDimensionValueAndOwner(dimensionValue, loggedUser)).thenReturn(dimensions);

        final Response response = dimensionResource.addNewDimension(dimensionsRequest);
        assertEquals(dimensions, response.getEntity());
        assertEquals(dimensions.getDimensionName(), dimensionName);
        assertEquals(dimensions.getDimensionValue(), dimensionValue);
    }

    @Test
    public void testDeleteDimension() throws CustomizedMessageException {
        Dimensions dimension = mock(Dimensions.class);
        int dimensionId = anyInt();

        when(dimensionResource.dimensionsService.findByID(dimensionId)).thenReturn(dimension);
        when(dimensionResource.dimensionsService.deleteDimension(dimension)).thenReturn(dimension);

        Response response = dimensionResource.deleteDimension(dimensionId);
        assertEquals(dimension, response.getEntity());
    }

}