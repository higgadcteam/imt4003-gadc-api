package no.scootr.gadc.api.resources;

import no.scootr.gadc.api.model.Tag;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.requestbeans.TagRequest;
import no.scootr.gadc.api.services.TagService;
import no.scootr.gadc.api.services.WebPropertyService;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;
import java.lang.Exception;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * This class contains unit testing for getting all tags, get web properties by tag and find tag by id.
 */
public class TagResourceTest {
    private TagResource tagResource;
    private WebPropertyResource webPropertyResource;

    @Before
    public void testMock() {
        tagResource = new TagResource();
        tagResource.tagService = mock(TagService.class);
        webPropertyResource = new WebPropertyResource();
        webPropertyResource.wpService = mock(WebPropertyService.class);
    }

    @Test
    public void testGetAllTags() {
        final List<Tag> tagsList = new ArrayList<Tag>();
        tagsList.add(mock(Tag.class));
        when(tagResource.tagService.findAll()).thenReturn(tagsList);
        assertEquals(tagsList, tagResource.getAllTags());
    }

    @Test
    public void testGetWebPropertiesByTag() {
        final String tagName = "sport";
        final Tag tag = mock(Tag.class);

        when(tagResource.tagService.getTagByName(tagName)).thenReturn(tag);

        final WebProperty webProperty = mock(WebProperty.class);
        final List<WebProperty> webProperties = new ArrayList<>();
        webProperties.add(webProperty);

        when(tag.getWebProperties()).thenReturn(webProperties);

        List<WebProperty> returnWebProperties = tagResource.getWebPropertiesByTag(tagName);

        assertEquals(webProperties, returnWebProperties);
    }

    @Test
    public void testFindTagByID() {
        when(tagResource.tagService.getTagByID(1)).thenReturn(new Tag());
        Response found = tagResource.findTagByID(1);
        assertEquals(200, found.getStatus());
    }

    @Test(expected = WebApplicationException.class)
    public void testFindTagByWrongID() throws  Exception {
        tagResource.findTagByID(12345);
    }

    @Test
    public void testRegisterTag() {
        final TagRequest tagRequest = mock(TagRequest.class);

        final String tagName = "foo";

        when(tagResource.tagService.getTagByName(tagName)).thenReturn(null);
        when(tagRequest.getTagName()).thenReturn(tagName);

        final Response response = tagResource.registerTag(tagRequest);
        final Tag storedTag = (Tag) response.getEntity();

        assertEquals(tagName, storedTag.getName());
    }
}