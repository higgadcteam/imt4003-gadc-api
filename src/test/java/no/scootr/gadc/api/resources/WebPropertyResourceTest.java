package no.scootr.gadc.api.resources;
import no.scootr.gadc.api.model.*;
import no.scootr.gadc.api.services.UserTokenService;
import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.GAAccount;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.model.UserAuthToken;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.requestbeans.WebPropertyRequest;
import no.scootr.gadc.api.services.GAAccountService;
import no.scootr.gadc.api.services.WebPropertyService;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.ArrayList;
import java.util.List;
import java.lang.Exception;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * This class contains unit testing for getting all web properties, delete a web property, get a web property for account,
 * add a web property.
 */
public class WebPropertyResourceTest {
    private WebPropertyResource webPropertyResource;

    private UserAuthToken userAuthToken;
    private User loggedUser;

    @Before
    public void testMock() {
        webPropertyResource = new WebPropertyResource();
        webPropertyResource.wpService = mock(WebPropertyService.class);
        webPropertyResource.userTokenService = mock(UserTokenService.class);
        webPropertyResource.securityContext = mock(SecurityContext.class);
        webPropertyResource.gaaService = mock(GAAccountService.class);

        userAuthToken = mock(UserAuthToken.class);
        loggedUser = new User();
        loggedUser = mock(User.class);
    }

    @Test
    public void testGetWebProperties() {
        final Principal principal = mock(Principal.class);

        when(webPropertyResource.securityContext.getUserPrincipal()).thenReturn(principal);
        when(webPropertyResource.userTokenService.getUserAuthToken(webPropertyResource.securityContext.getUserPrincipal().getName())).thenReturn(userAuthToken);
        when(userAuthToken.getUser()).thenReturn(loggedUser);

        final List<GoogleAccount> googleAccountList = new ArrayList<>();
        when(loggedUser.getAccounts()).thenReturn(googleAccountList);

        final GoogleAccount googleAccount = mock(GoogleAccount.class);
        googleAccountList.add(googleAccount);

        final List<GAAccount> gaAccountsList = new ArrayList<>();
        final GAAccount gaAccount = mock(GAAccount.class);
        gaAccountsList.add(gaAccount);
        when(googleAccount.getGaAccounts()).thenReturn(gaAccountsList);

        final List<WebProperty> webPropertiesList = new ArrayList<>();
        final WebProperty webProperty = mock(WebProperty.class);
        webPropertiesList.add(webProperty);
        when(gaAccount.getWebProperties()).thenReturn(webPropertiesList);


        final List<WebProperty> returnedWebProperties = webPropertyResource.getWebProperties();

        assertEquals(1, returnedWebProperties.size());
        assertEquals(webProperty, returnedWebProperties.get(0));
        //verify(webPropertiesList.addAll(gaAccountsList.get(0).getWebProperties()));
        webPropertyResource.gaaService = mock(GAAccountService.class);
        webPropertyResource.wpService = mock(WebPropertyService.class);
    }

    @Test
    public void testDeleteWebProperty() throws Exception {
        WebProperty webProperty = mock(WebProperty.class);
        String webPropertyId = anyString();

        when(webPropertyResource.wpService.getWebPropertyById(webPropertyId)).thenReturn(webProperty);
        when(webPropertyResource.wpService.deleteWebProperty(webProperty)).thenReturn(webProperty);

        Response response = webPropertyResource.deleteWebProperty(webPropertyId);
        assertEquals(webProperty, response.getEntity());
    }

    @Test
    public void testGetWebPropertyForAccount() {
        final int googleAccountID = 1;
        GAAccount gaAccount = mock(GAAccount.class);
        when(webPropertyResource.gaaService.findGAAccount(googleAccountID)).thenReturn(gaAccount);

        final List<WebProperty> webPropertyList = new ArrayList<>();

        when(gaAccount.getWebProperties()).thenReturn(webPropertyList);

        assertEquals(gaAccount.getWebProperties(), webPropertyList);

    }

    @Test
    public void testAddWebProperty() throws CustomizedMessageException {
        final long accountID = 1;
        final String webPropertyID = "123WWU";
        final String webPropertyName = "Gjovik";
        final WebPropertyRequest webPropertyRequest = mock(WebPropertyRequest.class);
        when(webPropertyRequest.getId()).thenReturn(webPropertyID);
        when(webPropertyRequest.getName()).thenReturn(webPropertyName);
        GAAccount gaAccount = mock(GAAccount.class);

        when(webPropertyResource.gaaService.findGAAccount(accountID)).thenReturn(gaAccount);
        WebProperty webProperty = mock(WebProperty.class);

        when(webPropertyResource.wpService.addWebProperty(webPropertyRequest, gaAccount)).thenReturn(webProperty);

        Response response = webPropertyResource.addWebProperty(accountID, webPropertyRequest);

        assertEquals(webProperty, response.getEntity());
    }
}