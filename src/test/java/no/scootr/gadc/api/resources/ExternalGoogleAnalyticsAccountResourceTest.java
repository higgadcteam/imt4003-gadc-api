package no.scootr.gadc.api.resources;

import no.scootr.gadc.api.google.GoogleAnalyticsService;
import no.scootr.gadc.api.model.GAAccount;
import no.scootr.gadc.api.model.GoogleAccount;
import no.scootr.gadc.api.services.GAAccountService;
import no.scootr.gadc.api.services.GoogleAccountService;
import org.junit.Before;
import org.junit.Test;

import javax.json.*;
import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * This class contains unit testing for getting GaAccounts and updating a GaAccount.
 */
public class ExternalGoogleAnalyticsAccountResourceTest {
    private ExternalGoogleAnalyticsAccountResource resource;

    @Before
    public void setupTest() {
        resource = new ExternalGoogleAnalyticsAccountResource();
        resource.googleAccountService = mock(GoogleAccountService.class);
        resource.gaAccountService = mock(GAAccountService.class);
        resource.googleAnalyticsService = mock(GoogleAnalyticsService.class);
    }

    @Test
    public void testGetGAAccounts() throws java.lang.Exception {
        final int googleAccountId = 1;
        final GoogleAccount googleAccount = mock(GoogleAccount.class);
        final JsonObject jsonObject = mock(JsonObject.class);
        final JsonArray jsonArray = mock(JsonArray.class);

        when(resource.googleAccountService.getGoogleAccount(googleAccountId)).thenReturn(googleAccount);
        when(resource.googleAnalyticsService.apiCallGetGAAccounts(googleAccount)).thenReturn(jsonObject);
        when(jsonObject.getJsonArray(ExternalGoogleAnalyticsAccountResource.ITEMS_FIELD)).thenReturn(jsonArray);

        final Response response = resource.getGAAccounts(googleAccountId);

        assertEquals(jsonArray, response.getEntity());
    }

    @Test
    public void testUpdateGoogleAnalyticsAccount() throws java.lang.Exception {
        final int googleAccountId = 1;
        final GoogleAccount googleAccount = mock(GoogleAccount.class);
        final List<GAAccount> gaAccountList = new ArrayList<>();
        final GAAccount gaAccount = mock(GAAccount.class);
        gaAccountList.add(gaAccount);

        final JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
        objectBuilder.add("id", "1");
        objectBuilder.add("name", "some name");

        final JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        arrayBuilder.add(objectBuilder);

        final JsonObjectBuilder mainObjectBuilder = Json.createObjectBuilder();
        mainObjectBuilder.add(ExternalGoogleAnalyticsAccountResource.ITEMS_FIELD, arrayBuilder);

        final JsonObject builtJsonObject = mainObjectBuilder.build();

        when(resource.googleAccountService.getGoogleAccount(googleAccountId)).thenReturn(googleAccount);
        when(resource.googleAnalyticsService.apiCallGetGAAccounts(googleAccount)).thenReturn(builtJsonObject);
        when(resource.gaAccountService.getAccountsByOwner(googleAccount)).thenReturn(gaAccountList);
        when(gaAccount.getId()).thenReturn(5L);

        final Response response = resource.updateGoogleAnalyticsAccount(googleAccountId);
        List responseList = (List) response.getEntity();

        assertEquals(1, responseList.size());

        verify(resource.gaAccountService).deleteGAAccount(gaAccount);
        verify(resource.gaAccountService).addOrUpdateGAAccount(anyObject());
    }
}