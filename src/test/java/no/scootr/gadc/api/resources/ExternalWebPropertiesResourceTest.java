package no.scootr.gadc.api.resources;

import no.scootr.gadc.api.google.GoogleWebPropertyService;
import no.scootr.gadc.api.model.GAAccount;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.services.GAAccountService;
import no.scootr.gadc.api.services.ProfileService;
import no.scootr.gadc.api.services.WebPropertyService;
import org.junit.Before;
import org.junit.Test;

import javax.json.*;
import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;
import java.lang.Exception;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * This class contains unit testing for getting profiles of GaAccount, update web properties and getting
 * web properties views.
 */
public class ExternalWebPropertiesResourceTest {
    private ExternalWebPropertiesResource externalWebPropertiesResource;

    @Before
    public void setupTest() {
        externalWebPropertiesResource = new ExternalWebPropertiesResource();
        externalWebPropertiesResource.gaAccountService = mock(GAAccountService.class);
        externalWebPropertiesResource.webPropertyService = mock(WebPropertyService.class);
        externalWebPropertiesResource.profileService = mock(ProfileService.class);
        externalWebPropertiesResource.googleWebPropertyService = mock(GoogleWebPropertyService.class);
    }

    @Test
    public void testGetProfilesOfGAAccount() throws Exception {
        final long gaaId = 1;
        final GAAccount gaAccount = mock(GAAccount.class);
        final JsonObject jsonObject = mock(JsonObject.class);
        final JsonArray jsonArray = mock(JsonArray.class);

        when(externalWebPropertiesResource.gaAccountService.getGAAccountByID(gaaId)).thenReturn(gaAccount);
        when(externalWebPropertiesResource.googleWebPropertyService.apiCallGetWebProperties(gaAccount, gaaId)).thenReturn(jsonObject);
        when(jsonObject.getJsonArray(ExternalWebPropertiesResource.ITEMS_FIELD)).thenReturn(jsonArray);

        final Response response = externalWebPropertiesResource.getProfilesOfGAAccount(gaaId);
        assertEquals(jsonArray, response.getEntity());
    }

    @Test
    public void testUpdateWebProperties() throws Exception {
        final String webPropertyId = "123";
        final long gaAccountId = 1;
        final List<WebProperty> webPropertiesDb = new ArrayList<>(1);
        final WebProperty persistentWebProperty = mock(WebProperty.class);
        when(persistentWebProperty.getId()).thenReturn(webPropertyId);
        webPropertiesDb.add(persistentWebProperty);

        final JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
        objectBuilder.add("id", "1");
        objectBuilder.add("name", "some name");
        objectBuilder.add("websiteUrl", "google.com");

        final JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        arrayBuilder.add(objectBuilder);

        final JsonObjectBuilder mainObjectBuilder = Json.createObjectBuilder();
        mainObjectBuilder.add(ExternalWebPropertiesResource.ITEMS_FIELD, arrayBuilder);

        final JsonObject builtJsonObject = mainObjectBuilder.build();

        final GAAccount gaAccount = mock(GAAccount.class);

        when(externalWebPropertiesResource.gaAccountService.getGAAccountByID(gaAccountId)).thenReturn(gaAccount);
        when(externalWebPropertiesResource.googleWebPropertyService.apiCallGetWebProperties(gaAccount, gaAccountId)).thenReturn(builtJsonObject);
        //when(externalWebPropertiesResource.webPropertyService.getWebPropertyById(webPropertyId)).thenReturn(persistentWebProperty);

        when(externalWebPropertiesResource.webPropertyService.getWebProperties(gaAccountId)).thenReturn(webPropertiesDb);

        final Response response = externalWebPropertiesResource.updateWebProperties(gaAccountId);


        List list = (List) response.getEntity();

        assertEquals(1, list.size());
        verify(externalWebPropertiesResource.webPropertyService).deleteWebProperty(anyObject());
    }

    @Test
    public void testGetWebPropertyViews() throws Exception {
        final long gaaId = 2;
        final String webPropertyId = "456";

        final GAAccount gaAccount = mock(GAAccount.class);
        final WebProperty webProperty = mock(WebProperty.class);
        when(externalWebPropertiesResource.gaAccountService.getGAAccountByID(gaaId)).thenReturn(gaAccount);

        final JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
        objectBuilder.add("id", "1");
        objectBuilder.add("name", "some name");

        final JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        arrayBuilder.add(objectBuilder);

        final JsonObjectBuilder mainObjectBuilder = Json.createObjectBuilder();
        mainObjectBuilder.add(ExternalWebPropertiesResource.ITEMS_FIELD, arrayBuilder);

        final JsonObject builtJsonObject = mainObjectBuilder.build();

        when(externalWebPropertiesResource.webPropertyService.getWebPropertyById(webPropertyId)).thenReturn(webProperty);
        when(externalWebPropertiesResource.googleWebPropertyService.apiCallGetViewsOfWebProperty(webPropertyId, gaAccount)).thenReturn(builtJsonObject);
        when(externalWebPropertiesResource.profileService.getProfileByID(1)).thenReturn(null);

        final Response response = externalWebPropertiesResource.getWebPropertyViews(gaaId, webPropertyId);

        assertNotNull(response);
        verify(externalWebPropertiesResource.profileService).addProfile(anyObject());
        final JsonArray jsonArray = (JsonArray) response.getEntity();

        assertEquals(1, jsonArray.size());
    }
}