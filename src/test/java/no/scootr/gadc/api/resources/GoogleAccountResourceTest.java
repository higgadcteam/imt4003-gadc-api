package no.scootr.gadc.api.resources;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.google.GoogleService;
import no.scootr.gadc.api.model.*;
import no.scootr.gadc.api.requestbeans.GoogleAccountRequest;
import no.scootr.gadc.api.requestbeans.LoginRequest;
import no.scootr.gadc.api.services.GoogleAccountService;
import no.scootr.gadc.api.services.UserTokenService;
import org.junit.Before;
import org.junit.Test;

import javax.json.JsonObject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.io.IOException;
import java.nio.file.attribute.UserPrincipal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.lang.Exception;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * This class contains unit testing for getting all Google Accounts, getting all active Google Accounts,
 * adding a Google Account, get Gooogle Account with ID, delete a google account and test register account with token.
 */
public class GoogleAccountResourceTest {
    private GoogleAccountResource googleAccountResource;
    private UserAuthToken userAuthToken;
    private User loggedUser;

    @Before
    public void testMock() {
        googleAccountResource = new GoogleAccountResource();
        googleAccountResource.gAccountService = mock(GoogleAccountService.class);
        googleAccountResource.securityContext = mock(SecurityContext.class);
        googleAccountResource.userTokenService = mock(UserTokenService.class);
        googleAccountResource.googleAnalyticsService = mock(GoogleService.class);

        userAuthToken = mock(UserAuthToken.class);
        loggedUser = new User();
        loggedUser = mock(User.class);
    }

    @Test
    public void testGetAllGAccountList() {
        final Principal principal = mock(Principal.class);
        when(googleAccountResource.securityContext.getUserPrincipal()).thenReturn(principal);
        when(googleAccountResource.userTokenService.getUserAuthToken(googleAccountResource.securityContext.getUserPrincipal().getName())).thenReturn(userAuthToken);
        when(userAuthToken.getUser()).thenReturn(loggedUser);

        final List<GoogleAccount> googleAccountList = new ArrayList<GoogleAccount>();
        googleAccountList.add(mock(GoogleAccount.class));

        when(googleAccountResource.gAccountService.getAllGoogleAccounts(loggedUser)).thenReturn(googleAccountList);

        assertEquals(googleAccountList, googleAccountResource.getAllGAccountList());

    }

    @Test
    public void testGetActiveGoogleAccounts() {
        final Principal principal = mock(Principal.class);
        when(googleAccountResource.securityContext.getUserPrincipal()).thenReturn(principal);
        when(googleAccountResource.userTokenService.getUserAuthToken(googleAccountResource.securityContext.getUserPrincipal().getName())).thenReturn(userAuthToken);
        when(userAuthToken.getUser()).thenReturn(loggedUser);


        final List<GoogleAccount> activeGoogleAccountList = new ArrayList<GoogleAccount>();
        activeGoogleAccountList.add(mock(GoogleAccount.class));

        when(googleAccountResource.gAccountService.getActiveGoogleAccounts(loggedUser)).thenReturn(activeGoogleAccountList);

        assertEquals( activeGoogleAccountList, googleAccountResource.getActiveGAccounts());
    }

    @Test
    public void testAddGoogleAccount() throws CustomizedMessageException {
        final GoogleAccountRequest googleAccountRequest = mock(GoogleAccountRequest.class);
        final GoogleAccount addedGoogleAccount = mock(GoogleAccount.class);

        when(googleAccountResource.gAccountService.addGoogleAccount(googleAccountRequest)).thenReturn(addedGoogleAccount);

        final Response response = googleAccountResource.addNewGoogleAccount(googleAccountRequest);

        assertNotNull(response);
        assertEquals(addedGoogleAccount, response.getEntity());
    }

    @Test
    public void testGetGoogleAccount() {
        GoogleAccount googleAccount = mock(GoogleAccount.class);
        when(googleAccountResource.gAccountService.getGoogleAccount(1)).thenReturn(googleAccount);
        Response found = googleAccountResource.getGoogleAccount(1);
        assertEquals(200, found.getStatus());
    }

    @Test(expected = WebApplicationException.class)
    public void testGetGAAccountByWrongId() throws  Exception {
        Response notFound = googleAccountResource.getGoogleAccount(6);
    }


    @Test
    public void testDeleteGoogleAccount() throws Exception {
        GoogleAccount googleAccount = mock(GoogleAccount.class);
        int googleAccountId = anyInt();

        when(googleAccountResource.gAccountService.getGoogleAccount(googleAccountId)).thenReturn(googleAccount);
        when(googleAccountResource.gAccountService.deleteGoogleAccount(googleAccount)).thenReturn(googleAccount);

        Response response = googleAccountResource.deleteGoogleAccount(googleAccountId);
        assertEquals(googleAccount, response.getEntity());
    }

    @Test
    public void testRegisterAccountWithToken() throws IOException, CustomizedMessageException {
        final LoginRequest loginRequest = mock(LoginRequest.class);
        final OAuthToken oAuthToken = mock(OAuthToken.class);
        final JsonObject jsonObject = mock(JsonObject.class);
        final User user = mock(User.class);
        final String tokenValue = "123";
        final String oAuthTokenValue = "456";
        final String sampleEmail = "user@example.com";
        final UserAuthToken userAuthToken = mock(UserAuthToken.class);


        final SecurityContext securityContext = mock(SecurityContext.class);
        final UserPrincipal userPrincipal = mock(UserPrincipal.class);
        when(securityContext.getUserPrincipal()).thenReturn(userPrincipal);

        when(userPrincipal.getName()).thenReturn(tokenValue);
        googleAccountResource.securityContext = securityContext;

        when(jsonObject.getString("email")).thenReturn(sampleEmail);

        when(oAuthToken.getTokenValue()).thenReturn(oAuthTokenValue);
        when(loginRequest.getToken()).thenReturn(tokenValue);
        when(googleAccountResource.googleAnalyticsService.getOauthToken(tokenValue, null)).thenReturn(oAuthToken);
        when(googleAccountResource.googleAnalyticsService.getUserInfo(oAuthToken.getTokenValue())).thenReturn(jsonObject);
        when(googleAccountResource.userTokenService.getUserAuthToken(tokenValue)).thenReturn(userAuthToken);
        when(userAuthToken.getUser()).thenReturn(user);

        googleAccountResource.registerAccountWithToken(loginRequest);

        verify(googleAccountResource.gAccountService).addGoogleAccount(anyObject());

    }
}