package no.scootr.gadc.api.resources;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.Tag;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.requestbeans.TagRequest;
import no.scootr.gadc.api.services.TagService;
import no.scootr.gadc.api.services.WebPropertyService;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.lang.Exception;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * This class contains unit testing for deleting a tag from a web property and tag a web property.
 */
public class TagWebPropertyResourceTest {
    private TagWebPropertyResource tagWebPropertyResource;

    @Before
    public void testMock() {
        tagWebPropertyResource = new TagWebPropertyResource();
        tagWebPropertyResource.tagService = mock(TagService.class);
        tagWebPropertyResource.wpService = mock(WebPropertyService.class);
    }

    @Test
    public void testDeleteTagFromWebProperty() throws Exception {
        final WebProperty webProperty = mock(WebProperty.class);
        final Tag tag = mock(Tag.class);

        final int tagId = 1;
        final String webPropertyId = "123";

        when(tagWebPropertyResource.wpService.getWebPropertyById(webPropertyId)).thenReturn(webProperty);
        when(tagWebPropertyResource.tagService.getTagByID(tagId)).thenReturn(tag);

        when(tagWebPropertyResource.tagService.containsWebProperty(tag, webProperty)).thenReturn(true);
        when(tagWebPropertyResource.wpService.containsTag(tag, webProperty)).thenReturn(true);

        final Response response = tagWebPropertyResource.deleteTagFromWebProperty(webPropertyId, tagId);

        assertEquals(webProperty, response.getEntity());

        verify(tagWebPropertyResource.tagService, times(1)).deleteWebPropertyFromTag(tag, webProperty);
        verify(tagWebPropertyResource.tagService, times(1)).edit(tag);

        verify(tagWebPropertyResource.wpService, times(1)).deleteTagFromWebProperty(tag, webProperty);
        verify(tagWebPropertyResource.wpService, times(1)).edit(webProperty);
    }

    @Test
    public void testTagWebProperty() throws CustomizedMessageException {
        final String webPropertyId = "webPropertyId";
        final TagRequest tagRequest = mock(TagRequest.class);
        tagRequest.setTagName("123123");
        final WebProperty webProperty = new WebProperty();

        when(tagWebPropertyResource.wpService.getWebPropertyById(webPropertyId)).thenReturn(webProperty);
        when(tagWebPropertyResource.tagService.containsWebProperty(anyObject(), anyObject())).thenReturn(false);
        when(tagWebPropertyResource.wpService.containsTag(anyObject(), anyObject())).thenReturn(false);

        final Response response = tagWebPropertyResource.tagWebProperty(webPropertyId, tagRequest);

        assertEquals(webProperty, response.getEntity());

        verify(tagWebPropertyResource.tagService).edit(anyObject());
        verify(tagWebPropertyResource.wpService).edit(webProperty);
    }
}