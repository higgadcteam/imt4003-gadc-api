package no.scootr.gadc.api.persistence;

import no.scootr.gadc.api.persistence.model.Foo;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Martin on 09.11.2015.
 */
@Stateless
public class FooService {
    @PersistenceContext
    private EntityManager em;

    public Foo persistEntity(final Foo entity) {
        em.persist(entity);
        return entity;
    }

    public Foo getFooById(final int id) {
        return em.find(Foo.class, id);
    }
}