package no.scootr.gadc.api.persistence;

import no.scootr.gadc.api.utillity.Transactional;
import no.scootr.gadc.api.persistence.model.Foo;
import org.junit.Rule;
import org.junit.Test;
import org.needle4j.annotation.ObjectUnderTest;
import org.needle4j.junit.DatabaseRule;
import org.needle4j.junit.NeedleRule;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Integration test for testing the persistence API is being properly mocked for a service.
 */
public class PersistenceTest {
    @Rule
    public DatabaseRule databaseRule = new DatabaseRule();

    @Rule
    public NeedleRule needleRule = new NeedleRule(databaseRule);

    @ObjectUnderTest
    public FooService fooService;

    @Test
    public void testPersistence() throws java.lang.Exception {
        final Foo foo = new Foo("foobar");

        Transactional.using(databaseRule).executeAndGet(() -> fooService.persistEntity(foo));

        assertTrue(foo.getId() > 0);

        assertNotNull(fooService.getFooById(foo.getId()));
    }
}
