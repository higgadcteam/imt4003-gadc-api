package no.scootr.gadc.api.persistence.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Martin on 09.11.2015.
 */
@Entity
public class Foo {
    @Id
    @GeneratedValue
    private int id;

    private String value;

    public Foo() {
    }

    public Foo(String value) {
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public String getValue() {
        return value;
    }
}
