package no.scootr.gadc.api.services;

import no.scootr.gadc.api.model.GAAccount;
import no.scootr.gadc.api.model.GoogleAccount;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.requestbeans.GAAccountRequest;
import no.scootr.gadc.api.requestbeans.GoogleAccountRequest;
import no.scootr.gadc.api.utillity.Transactional;
import org.junit.Rule;
import org.junit.Test;
import org.needle4j.annotation.ObjectUnderTest;
import org.needle4j.junit.DatabaseRule;
import org.needle4j.junit.NeedleRule;
import java.lang.Exception;

import static org.junit.Assert.*;

/**
 * This class contains unit testing for getting accounts by owner.
 */
public class GAAccountServiceTest {

    @Rule
    public DatabaseRule databaseRule = new DatabaseRule();

    @Rule
    public NeedleRule needleRule = new NeedleRule(databaseRule);

    @ObjectUnderTest
    public UserService userService;

    @ObjectUnderTest
    public GoogleAccountService googleAccountService;

    @ObjectUnderTest
    public GAAccountService gaAccountService;

    @Test
    public void testGetAccountsByOwner() throws Exception {

        final GoogleAccountRequest googleAccountRequest = new GoogleAccountRequest();
        googleAccountRequest.setEmail("qwerty@i.ua");
        googleAccountRequest.setUserId(456);

        final GAAccountRequest gaAccountRequest = new GAAccountRequest();
        gaAccountRequest.setId(456);
        gaAccountRequest.setName("qwerty");
        gaAccountRequest.setOwner(456);

        final User registeredUser = (User) Transactional.fetch(databaseRule)
                .executeAndGet(() -> userService.getOrRegisterUser("qwerty@i.ua"));
        googleAccountRequest.setUserId(registeredUser.getId());

        final GoogleAccount registeredAccount = (GoogleAccount) Transactional.fetch(databaseRule)
                .executeAndGet(() -> googleAccountService.addGoogleAccount(googleAccountRequest));
        gaAccountRequest.setOwner(registeredAccount.getId());

        final GAAccount registeredGaAccount = (GAAccount) Transactional.fetch(databaseRule)
                .executeAndGet(() -> gaAccountService.addGAAccount(gaAccountRequest));

        assertNotNull(gaAccountService.getAccountsByOwner(registeredGaAccount.getOwner()));
        assertEquals(gaAccountRequest.getId(), registeredGaAccount.getId());
        assertNotNull(gaAccountService.getGAAccountByID(registeredGaAccount.getId()));
    }
}