package no.scootr.gadc.api.services;

import no.scootr.gadc.api.model.GoogleAccount;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.requestbeans.GoogleAccountRequest;
import no.scootr.gadc.api.utillity.Transactional;
import org.junit.Rule;
import org.junit.Test;
import org.needle4j.annotation.ObjectUnderTest;
import org.needle4j.junit.DatabaseRule;
import org.needle4j.junit.NeedleRule;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * This class contains unit testing for registring a user, getting a user by id.
 */
public class UserServiceTest {

    @Rule
    public DatabaseRule databaseRule = new DatabaseRule();

    @Rule
    public NeedleRule needleRule = new NeedleRule(databaseRule);

    @ObjectUnderTest
    public UserService userService;

    @ObjectUnderTest
    public GoogleAccount googleAccount;

    public void testRegisterUser() {
        final User user = userService.getOrRegisterUser("foo@bar.com");
        assertNotNull(user);
        assertNotNull(userService.getUserById(user.getId()));
        assertNotNull(userService.getOrRegisterUser("foo@bar.com"));
    }

    @Test
    public void testGetUserById() throws java.lang.Exception {
        assertNull(userService.getUserById(-1));

        final GoogleAccountRequest googleAccountRequest = new GoogleAccountRequest();
        final User registeredUser = (User) Transactional.fetch(databaseRule)
                .executeAndGet(() -> userService.getOrRegisterUser("drt@i.ua"));

        googleAccountRequest.setUserId(registeredUser.getId());

        assertNotNull(userService.getUserById(registeredUser.getId()));
    }

}