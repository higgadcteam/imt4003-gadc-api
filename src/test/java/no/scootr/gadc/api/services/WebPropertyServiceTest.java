package no.scootr.gadc.api.services;

import no.scootr.gadc.api.exceptions.registrationexceptions.CustomizedMessageException;
import no.scootr.gadc.api.model.*;
import no.scootr.gadc.api.requestbeans.GAAccountRequest;
import no.scootr.gadc.api.requestbeans.GoogleAccountRequest;
import no.scootr.gadc.api.requestbeans.WebPropertyRequest;
import no.scootr.gadc.api.utillity.Transactional;
import org.junit.Rule;
import org.junit.Test;
import org.needle4j.annotation.ObjectUnderTest;
import org.needle4j.junit.DatabaseRule;
import org.needle4j.junit.NeedleRule;

import static org.junit.Assert.*;

/**
 * This class contains unit testing for getting the web property by ID and adding a web property.
 */
public class WebPropertyServiceTest {

    @Rule
    public DatabaseRule databaseRule = new DatabaseRule();

    @Rule
    public NeedleRule needleRule = new NeedleRule(databaseRule);

    @ObjectUnderTest
    public WebPropertyService webPropertyService;

    @ObjectUnderTest
    public GAAccountService gaAccountService;

    @ObjectUnderTest
    public GoogleAccountService googleAccountService;

    @ObjectUnderTest
    public UserService userService;



    @Test
    public void testGetWebPropertyById() throws java.lang.Exception {
        assertNull(webPropertyService.getWebPropertyById("-1"));

        final WebPropertyRequest webPropertyRequest = new WebPropertyRequest();
        webPropertyRequest.setId("1");

        final GoogleAccountRequest googleAccountRequest = new GoogleAccountRequest();
        googleAccountRequest.setEmail("drt@i.ua");
        googleAccountRequest.setUserId(123);

        final GAAccountRequest gaAccountRequest = new GAAccountRequest();
        gaAccountRequest.setId(1234);
        gaAccountRequest.setName("masha");

        final User registeredUser = (User) Transactional.fetch(databaseRule)
                .executeAndGet(() -> userService.getOrRegisterUser("drt@i.ua"));

        googleAccountRequest.setUserId(registeredUser.getId());

        final GoogleAccount registeredAccount = (GoogleAccount) Transactional.fetch(databaseRule)
                .executeAndGet(() -> googleAccountService.addGoogleAccount(googleAccountRequest));
        gaAccountRequest.setOwner(registeredAccount.getId());

        final GAAccount registeredGaAccount = (GAAccount) Transactional.fetch(databaseRule)
                .executeAndGet(() -> gaAccountService.addGAAccount(gaAccountRequest));

        final WebProperty webProperty = (WebProperty) Transactional.fetch(databaseRule)
                .executeAndGet(() -> webPropertyService.addWebProperty(webPropertyRequest, registeredGaAccount));

        assertNotNull(webPropertyService.getWebPropertyById(webProperty.getId()));
    }

    @Test(expected = javax.persistence.PersistenceException.class)
    public void testAddWebProperty() throws CustomizedMessageException {
        final WebPropertyRequest wprequest = new WebPropertyRequest();
        final GAAccount gaAccount = new GAAccount();
        wprequest.setName(null);
        webPropertyService.addWebProperty(wprequest, gaAccount);
    }

    @Test(expected = Exception.class)
    public void testAddWebPropertyIdIsNull() throws CustomizedMessageException {
        final WebPropertyRequest wprequest = new WebPropertyRequest();
        webPropertyService.addWebProperty(wprequest, null);
    }

}