package no.scootr.gadc.api.services;

import no.scootr.gadc.api.model.Tag;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.utillity.Transactional;
import org.junit.Rule;
import org.junit.Test;
import org.needle4j.annotation.ObjectUnderTest;
import org.needle4j.junit.DatabaseRule;
import org.needle4j.junit.NeedleRule;

import java.lang.Exception;

import static org.junit.Assert.*;

/**
 * This class contains unit testing for adding a tag, getting tag by name.
 */
public class TagServiceTest {
    @Rule
    public DatabaseRule databaseRule = new DatabaseRule();

    @Rule
    public NeedleRule needleRule = new NeedleRule(databaseRule);

    @ObjectUnderTest
    public TagService tagService;

    @Test
    public void testGetTagByID() {

        assertNull(tagService.getTagByID(-1));
    }

    @Test
    public void testAddTag() throws Exception {
        final Tag tag = new Tag();
        tag.setName("sport");

        Transactional.using(databaseRule).execute(() -> tagService.addTag(tag));
        assertEquals(tagService.getTagByID(tag.getId()).getId(), tag.getId());
    }

    @Test
    public void testContainsWebProperty() {
        final Tag tag = new Tag();
        final WebProperty webProperty = new WebProperty();

        assertFalse(tagService.containsWebProperty(tag, webProperty));
    }

    @Test
    public void testGetTagByName() throws java.lang.Exception {
        final Tag tag = new Tag();
        tag.setName("masha");
        Transactional.using(databaseRule).execute(() -> tagService.addTag(tag));
        assertNotNull(tagService.getTagByName(tag.getName()));
    }
}
