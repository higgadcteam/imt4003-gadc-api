package no.scootr.gadc.api.services;

import no.scootr.gadc.api.model.Dimensions;
import no.scootr.gadc.api.utillity.Transactional;
import org.junit.Rule;
import org.junit.Test;
import org.needle4j.annotation.ObjectUnderTest;
import org.needle4j.junit.DatabaseRule;
import org.needle4j.junit.NeedleRule;
import java.lang.Exception;

import static org.junit.Assert.*;

/**
 * This class contains unit testing for adding a dimension, find by id and deleting a dimension.
 */
public class DimensionsServiceTest {

    @Rule
    public DatabaseRule databaseRule = new DatabaseRule();

    @Rule
    public NeedleRule needleRule = new NeedleRule(databaseRule);

    @ObjectUnderTest
    public DimensionsService dimensionsService;

    @Test
    public void testAdd() throws Exception {
        final Dimensions dimensions = new Dimensions();
        dimensions.setDimensionName("dimension");
        dimensions.setDimensionValue("3");
        Transactional.using(databaseRule).execute(() -> dimensionsService.add(dimensions));
        assertEquals(dimensionsService.findByID(dimensions.getId()).getId(), dimensions.getId());
    }


    @Test
    public void testFindByID() throws java.lang.Exception {
        assertNull(dimensionsService.findByID(-1));
    }

    @Test
    public void testFindByDimensionValue() throws java.lang.Exception {
        final Dimensions dimensions = new Dimensions();
        dimensions.setDimensionValue("dimension");
        Transactional.using(databaseRule).execute(() -> dimensionsService.add(dimensions));
        assertNotNull(dimensionsService.findByDimensionValue(dimensions.getDimensionValue()));
    }

    @Test
    public void testDeleteDimension() throws java.lang.Exception {
        final Dimensions dimensions = new Dimensions();
        dimensions.setDimensionValue("1");
        Transactional.using(databaseRule).execute(() -> dimensionsService.add(dimensions));
        Transactional.using(databaseRule).execute(() -> dimensionsService.deleteDimension(dimensions));
        assertNull(dimensionsService.findByDimensionValue(dimensions.getDimensionValue()));
    }
}