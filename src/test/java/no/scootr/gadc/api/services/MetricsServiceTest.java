package no.scootr.gadc.api.services;

import no.scootr.gadc.api.model.Metrics;
import no.scootr.gadc.api.utillity.Transactional;
import org.junit.Rule;
import org.junit.Test;
import org.needle4j.annotation.ObjectUnderTest;
import org.needle4j.junit.DatabaseRule;
import org.needle4j.junit.NeedleRule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * This class contains unit testing for adding a metric and find the metric by id.
 */
public class MetricsServiceTest {
    @Rule
    public DatabaseRule databaseRule = new DatabaseRule();

    @Rule
    public NeedleRule needleRule = new NeedleRule(databaseRule);

    @ObjectUnderTest
    public MetricsService metricsService;

    @Test
    public void testAdd() throws java.lang.Exception {
        final Metrics metrics = new Metrics();
        metrics.setMetricsName("metric");
        metrics.setMetricsValue("1");
        Transactional.using(databaseRule).execute(() -> metricsService.add(metrics));
        assertEquals(metricsService.findByID(metrics.getId()).getId(), metrics.getId());
    }

    @Test
    public void testFindByID() throws Exception {
        assertNull(metricsService.findByID(-1));
    }
}


