package no.scootr.gadc.api.services;

import no.scootr.gadc.api.model.GoogleAccount;
import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.requestbeans.GoogleAccountRequest;
import no.scootr.gadc.api.utillity.Transactional;
import org.junit.Rule;
import org.junit.Test;
import org.needle4j.annotation.ObjectUnderTest;
import org.needle4j.junit.DatabaseRule;
import org.needle4j.junit.NeedleRule;

import static org.junit.Assert.*;

/**
 * This class contains unit testing for getting google account by email.
 */
public class GoogleAccountServiceTest {

    @Rule
    public DatabaseRule databaseRule = new DatabaseRule();

    @Rule
    public NeedleRule needleRule = new NeedleRule(databaseRule);

    @ObjectUnderTest
    public GoogleAccountService googleAccountService;

    @ObjectUnderTest
    public UserService userService;

    @Test
    public void testGetGoogleAccountByEmail() throws java.lang.Exception {
        final GoogleAccountRequest googleAccountRequest = new GoogleAccountRequest();
        googleAccountRequest.setEmail("mariya@i.ua");
        googleAccountRequest.setUserId(123);

        final User registeredUser = (User) Transactional.fetch(databaseRule)
                .executeAndGet(() -> userService.getOrRegisterUser("mariya@i.ua"));
        googleAccountRequest.setUserId(registeredUser.getId());

        final GoogleAccount registeredAccount = (GoogleAccount) Transactional.fetch(databaseRule)
                .executeAndGet(() -> googleAccountService.addGoogleAccount(googleAccountRequest));
        assertNotNull(googleAccountService.getGoogleAccount(registeredAccount.getEmail()));
    }

}