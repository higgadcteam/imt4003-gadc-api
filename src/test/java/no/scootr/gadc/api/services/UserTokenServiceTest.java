package no.scootr.gadc.api.services;

import no.scootr.gadc.api.model.User;
import no.scootr.gadc.api.model.UserAuthToken;
import no.scootr.gadc.api.utillity.Transactional;
import org.junit.Rule;
import org.junit.Test;
import org.needle4j.annotation.ObjectUnderTest;
import org.needle4j.junit.DatabaseRule;
import org.needle4j.junit.NeedleRule;

import static org.junit.Assert.*;

/**
 * This class contains unit testing for removing a token.
 */
public class UserTokenServiceTest {

    @Rule
    public DatabaseRule databaseRule = new DatabaseRule();

    @Rule
    public NeedleRule needleRule = new NeedleRule(databaseRule);

    @ObjectUnderTest
    public UserTokenService userTokenService;

    @ObjectUnderTest
    public UserService userService;

   @Test
    public void testRemoveToken() throws java.lang.Exception {
        final User user = userService.getOrRegisterUser("foo@bar.com");
        final UserAuthToken token = new UserAuthToken(user);
        Transactional.using(databaseRule).execute(() -> userTokenService.createToken(user));
        Transactional.using(databaseRule).execute(() -> userTokenService.removeToken(token));
        assertNull(userTokenService.getUserAuthToken("Bearer " + token.getTokenValue()));
    }

}