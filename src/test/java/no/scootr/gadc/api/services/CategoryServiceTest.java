package no.scootr.gadc.api.services;

import no.scootr.gadc.api.model.Category;
import no.scootr.gadc.api.model.WebProperty;
import no.scootr.gadc.api.utillity.Transactional;
import org.junit.Rule;
import org.junit.Test;
import org.needle4j.annotation.ObjectUnderTest;
import org.needle4j.junit.DatabaseRule;
import org.needle4j.junit.NeedleRule;

import static org.junit.Assert.*;

/**
 * This class contains unit testing for getting the category by ID, adding a category, getting the category
 * by name and deleting a category.
 */

public class CategoryServiceTest {
    @Rule
    public DatabaseRule databaseRule = new DatabaseRule();

    @Rule
    public NeedleRule needleRule = new NeedleRule(databaseRule);

    @ObjectUnderTest
    public CategoryService categoryService;

    @ObjectUnderTest
    public WebPropertyService webPropertyService;

    @Test
    public void testGetCategoryById() {
        assertNull(categoryService.getCategoryById(-1));
    }

    @Test
    public void testAddCategory() throws java.lang.Exception {
        final Category category = new Category();
        category.setName("games");
        Transactional.using(databaseRule).execute(() -> categoryService.add(category));
        assertEquals(categoryService.getCategoryById(category.getId()).getId(), category.getId());
    }

    @Test
    public void testContainsWebProperty() {
        final Category category = new Category();
        final WebProperty webProperty = new WebProperty();
        assertFalse(categoryService.containsWebProperty(category, webProperty));
    }

    @Test
    public void testGetCategoryByName() throws java.lang.Exception {
        final Category category = new Category();
        category.setName("masha");
        Transactional.using(databaseRule).execute(() -> categoryService.add(category));
        assertNotNull(categoryService.getCategory(category.getName()));
    }

    @Test
    public void testDeleteCategory() throws java.lang.Exception {
        final Category category = new Category();
        category.setName("sport");
        Transactional.using(databaseRule).execute(() -> categoryService.add(category));
        Transactional.using(databaseRule).execute(() -> categoryService.deleteCategory(category));
        assertNull(categoryService.getCategory(category.getName()));
    }

}

